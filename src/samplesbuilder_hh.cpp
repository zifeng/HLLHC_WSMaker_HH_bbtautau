#include <set>
#include <unordered_map>
#include <utility>
#include <iostream>
#include <map>

#include <TString.h>

#include "WSMaker/configuration.hpp"
#include "WSMaker/containerhelpers.hpp"
#include "WSMaker/sample.hpp"
#include "samplesbuilder_hh.hpp"

void SamplesBuilder_HH::declareSamples() {
  TString massPoint = m_config.getValue("MassPoint", "1000");
  TString type = m_config.getValue("Type", "RSG");
  int injection = m_config.getValue("DoInjection", 0);
  TString massInjection = TString::Itoa(injection, 10);
  bool VBF = m_config.getValue("VBF", false);
  bool ggFVBF = m_config.getValue("ggFVBF", false);


  // data or pseudo-data should always be there
  addData();

  // signal
  if (type == "RSGc2") {
    addSample(Sample("Ghhbbtautau" + massPoint + "c20", SType::Sig, kViolet));
    addSample(Sample("RS_G_hh_bbtt_hh_c20_M" + massPoint, SType::Sig, kViolet));
  } else if (type == "RSGc1") {
    addSample(Sample("Ghhbbtautau" + massPoint + "c10", SType::Sig, kViolet));
    addSample(Sample("RS_G_hh_bbtt_hh_c10_M" + massPoint, SType::Sig, kViolet));  } else if (type == "RSG") {
    addSample(Sample("Ghhbbtautau" + massPoint + "c10", SType::Sig, kViolet));
    addSample(Sample("RS_G_hh_bbtt_hh_c10_M" + massPoint, SType::Sig, kViolet));
  } else if (type == "2HDM") {
    addSample(Sample("Hhhbbtautau"+massPoint, SType::Sig, kViolet));
    addSample(Sample("X"+massPoint, SType::Sig, kViolet));
  } else if (type.Contains("SM")) {
    if (!ggFVBF){//not fitting ggF and VBF together (default if not specified)
      std::cout<<"Type: "<<type<<std::endl;
      if (type == "SM") {
	if (VBF){
	  TString sample = "hhttbbVBFSM";//VBF SM signal
	  sample.ReplaceAll("SMVBF", "");
	  addSample(Sample(sample, SType::Sig, kViolet));
	}else {
	  addSample(Sample("hhttbb", SType::Sig, kViolet));//ggF SM signal
	  //addSample(Sample("hhttbbFTapprox", SType::Sig, kViolet));
	}

      } else if (type == "SMRW") {
	addSample(Sample("hhttbbRW", SType::Sig, kViolet));
      } else if (VBF && type.Contains("c2v")){
	std::cout<<"VBF and c2v "<<"Type: "<<type<<std::endl;
	if (type.Contains("c2v01")){
	std::cout<<"VBF and c2v01 "<<"Type: "<<type<<std::endl;
	  TString sample = "hhttbbVBFSM"; //VBF SM signal for c2v=0
	  sample.ReplaceAll("SMVBF", "");
	  addSample(Sample(sample, SType::Sig, kViolet));
	} else {
	  TString sample = "hhttbbVBF" + type; //VBF c2v signals
	  sample.ReplaceAll("SMVBF", "");
	  addSample(Sample(sample, SType::Sig, kViolet));
	}
	 
      } else if (VBF && type.Contains("l")){
	if (type.Contains("l01")){
	  TString sample = "hhttbbVBFSM"; //VBF SM for l=1
	  sample.ReplaceAll("SMVBF", "");
	  addSample(Sample(sample, SType::Sig, kViolet));
	} else {
	  TString sample = "hhttbbVBF" + type; //VBF klambda signals
	  sample.ReplaceAll("SMVBF", "");
	  addSample(Sample(sample, SType::Sig, kViolet));
	} 
      } else {
	if (type.Contains("l01")){
	  TString sample = "hhttbb";
	  sample.ReplaceAll("SM", "");
	  addSample(Sample(sample, SType::Sig, kViolet));
	} else {
	TString sample = "hhttbb" + type;// klambda ggF signals
	sample.ReplaceAll("SM", "");
	addSample(Sample(sample, SType::Sig, kViolet));
	} 
      }
    } else if (ggFVBF){
      if (type =="SMggFVBF"){
	addSample(Sample("hhttbb", SType::Sig, kViolet));
	addSample(Sample("hhttbbVBFSM", SType::Sig, kViolet));
      } else if (type.Contains("c2v")){
	TString sampleggF = "hhttbb";
	if (type.Contains("c2v01")){
	    TString sampleVBF = "hhttbbVBF";
	    sampleVBF.ReplaceAll("SMggFVBF", "");
	    addSample(Sample(sampleVBF, SType::Sig, kViolet));
	  } else {
	    TString sampleVBF = "hhttbbVBF" + type;
	    sampleVBF.ReplaceAll("SMggFVBF", "");
	    addSample(Sample(sampleVBF, SType::Sig, kViolet));
	  }
	sampleggF.ReplaceAll("SMggFVBF", "");
	addSample(Sample(sampleggF, SType::Sig, kViolet));

      } else {
	if (type.Contains("l01")){
	    TString sampleggF = "hhttbb";
		TString sampleVBF = "hhttbbVBF";
		sampleggF.ReplaceAll("SMggFVBF", "");
		sampleVBF.ReplaceAll("SMggFVBF", "");
		addSample(Sample(sampleggF, SType::Sig, kViolet));
		addSample(Sample(sampleVBF, SType::Sig, kViolet));
	      } else {
		TString sampleggF = "hhttbb" + type;
		TString sampleVBF = "hhttbbVBF" + type;
		sampleggF.ReplaceAll("SMggFVBF", "");
		sampleVBF.ReplaceAll("SMggFVBF", "");
		addSample(Sample(sampleggF, SType::Sig, kViolet));
		addSample(Sample(sampleVBF, SType::Sig, kViolet));
	}
      }
    }
  } else if (type == "ZZ"){
    addSample(Sample("ZZllqq", SType::Sig, kViolet));
  } else if (type == "LQ3") {
    addSample(Sample("LQ3btaubtau"+massPoint, SType::Sig, kViolet));
  }


  if(injection>0 && massInjection != massPoint) {
    std::cout << "INJECTION " << massInjection << std::endl;

    if (type == "RSGc2") {
      addSample(Sample("Ghhbbtautau" + massInjection + "c20", SType::None, kViolet));
    } else if (type == "RSGc1") {
      addSample(Sample("Ghhbbtautau" + massInjection + "c10", SType::None, kViolet));
    } else if (type == "RSG") {
      addSample(Sample("Ghhbbtautau" + massInjection + "c10", SType::None, kViolet));
    }else if (type == "2HDM") {
      addSample(Sample("Hhhbbtautau"+massInjection, SType::None, kViolet));
      addSample(Sample("InjX"+massInjection, SType::None, kViolet));
    } else if (type == "SM") {
      addSample(Sample("hhttbb", SType::None, kViolet));
    } else if (type == "LQ3") {
      addSample(Sample("LQ3btaubtau"+massInjection, SType::None, kViolet));
    }
  }

  // TODO add spectator samples

  // SM Higgs
  addBkgs( { {"WlvH125", kRed}, {"ggZvvH125", kRed}, {"qqZvvH125", kRed},
           {"ggZllH125", kRed}, {"qqZllH125", kRed} } );
  
  
  addBkgs( { {"VH", kRed } });//to be removed when lephad fixes inputs
  addBkgs( { {"ttH", kRed } });
  addBkgs( { {"ggZHbb", kRed }});
  addBkgs( { {"qqZHbb", kRed }});
  addBkgs( { {"ggZHtautau", kRed }});
  addBkgs( { {"qqZHtautau" , kRed} });
  addBkgs( { {"WHbb", kRed }});
  addBkgs( { {"WHtautau" , kRed} });
  addBkgs( { {"ggFHtautau" , kRed} });
  addBkgs( { {"VBFHtautau", kRed} });
  

  addBkgs( { {"ttZ", kGreen-2} });
  addBkgs( { {"ttW", kGreen-2} });


  // Z->tautau + jets
  addBkgs( { {"Zttl", kAzure - 9} } );
  addBkgs( { {"Zttcl", kAzure - 4}, {"Zttcc", kAzure - 2}, {"Zttbl", kAzure + 2}, {"Zttbc", kAzure + 3}, {"Zttbb", kAzure + 4} } );

  // W->taunu + jets
  addBkgs( { {"Wtt", kGreen} } );
  addBkgs( { {"Wttl", kGreen} } );
  addBkgs( { {"Wttcl", kGreen}, {"Wttcc", kGreen}, {"Wttbl", kGreen}, {"Wttbc", kGreen}, {"Wttbb", kGreen} } );

 // Z->tautau + jets
  addBkgs( { {"ZttMGl", kAzure - 9} } );
  addBkgs( { {"ZttMGcl", kAzure - 4}, {"ZttMGcc", kAzure - 2}, {"ZttMGbl", kAzure + 2}, {"ZttMGbc", kAzure + 3}, {"ZttMGbb", kAzure + 4} } );

  // W->taunu + jets
  addBkgs( { {"WttMG", kGreen} } );
  addBkgs( { {"WttMGl", kGreen} } );
  addBkgs( { {"WttMGcl", kGreen}, {"WttMGcc", kGreen}, {"WttMGbl", kGreen}, {"WttMGbc", kGreen}, {"WttMGbb", kGreen} } );

  // Sherpa version of ee/mumu in base

  // W+jets
  addBkgs( { {"W", kGreen - 9} } );
  addBkgs( { {"WMG", kGreen - 9} } );
  addBkgs( { {"WMGl", kGreen - 9} } );
  addBkgs( { {"WMGcl", kGreen - 6}, {"WMGcc", kGreen - 3}, {"WMGbl", kGreen + 2}, {"WMGbc", kGreen + 3}, {"WMGbb", kGreen + 4} } );

  // Z+jets
  addBkgs( { {"ZMGl", kAzure - 9} } );
  addBkgs( { {"ZMGcl", kAzure - 4}, {"ZMGcc", kAzure - 2}, {"ZMGbl", kAzure + 2}, {"ZMGbc", kAzure + 3}, {"ZMGbb", kAzure + 4} } );


  // diboson
  if (type == "ZZ"){
    addBkgs( { {"WZ", kGray}, {"ZZvvqq", kGray + 1}, {"WW", kGray + 3} } );
  } else{
    addBkgs( { {"WZ", kGray}, {"ZZ", kGray + 1}, {"WW", kGray + 3} } );
  }

  // DY
  addBkgs( { {"DY", kOrange}, {"DYtt", kOrange + 1} } );

  // SM h
  addBkgs( { {"ZllH125", kOrange} } );

  // ttbar, stop, sherpa Zll & Wlnu ... 
  // W+jets
  addBkgs( { {"Wl", kGreen - 9} } );
  addBkgs( { {"Wcl", kGreen - 6}, {"Wcc", kGreen - 3}, {"Wbl", kGreen + 2}, {"Wbc", kGreen + 3}, {"Wbb", kGreen + 4} } );

  // Z+jets
  addBkgs( { {"Zl", kAzure - 9} } );
  addBkgs( { {"Zcl", kAzure - 4}, {"Zcc", kAzure - 2}, {"Zbl", kAzure + 2}, {"Zbc", kAzure + 3}, {"Zbb", kAzure + 4} } );

  // Top and single-top
  addBkgs( { {"ttbar", kOrange} } );
  addBkgs( { {"stopt", kYellow - 7}, {"stops", kYellow - 5}, {"stopWt", kOrange + 3} } );

  //addBkgs( { {"ttbarFake", kOrange - 6} } );

  //hadhad ttbarFakes from MC for now
  //addSample(Sample("ttbarTF", SType::Bkg, kOrange -6));
  //addSample(Sample("ttbarFT", SType::Bkg, kOrange -6));
  //addSample(Sample("ttbarFF", SType::Bkg, kOrange -6));
  //

  //hadhad ttbarFakes from data FRs
  //addSample(Sample("ttbarFRTF", SType::DataDriven, kOrange -6));
  //addSample(Sample("ttbarFRFT", SType::DataDriven, kOrange -6));
  //addSample(Sample("ttbarFRFF", SType::DataDriven, kOrange -6));

  //hadHad ttbar fakes from data SFs
  addSample(Sample("ttbarSFTF", SType::DataDriven, kOrange -6));
  addSample(Sample("ttbarSFFT", SType::DataDriven, kOrange -6));
  addSample(Sample("ttbarSFFF", SType::DataDriven, kOrange -6));

  //addSample(Sample("stoptTFFR", SType::DataDriven, kYellow - 7));
  //addSample(Sample("stoptFTFR", SType::DataDriven, kYellow - 7));
  //addSample(Sample("stoptFFFR", SType::DataDriven, kYellow - 7));

  //addSample(Sample("stopsTFFR", SType::DataDriven, kYellow - 5));
  //addSample(Sample("stopsFTFR", SType::DataDriven, kYellow - 5));
  //addSample(Sample("stopsFFFR", SType::DataDriven, kYellow - 5));

  //addSample(Sample("stopWtTFFR", SType::DataDriven, kYellow + 3));
  //addSample(Sample("stopWtFTFR", SType::DataDriven, kYellow + 3));
  //addSample(Sample("stopWtFFFR", SType::DataDriven, kYellow + 3));

  //lephad Fakes from MC for now
  /*
  addSample(Sample("ttbarFake", SType::Bkg, kPink+1));

  addSample(Sample("stopsFake", SType::Bkg, kPink+1));
  addSample(Sample("stoptFake", SType::Bkg, kPink+1));
  addSample(Sample("stopWtFake", SType::Bkg, kPink+1));

  addSample(Sample("ZttbbFake", SType::Bkg, kPink+1));
  addSample(Sample("ZttbcFake", SType::Bkg, kPink+1));
  addSample(Sample("ZttccFake", SType::Bkg, kPink+1));

  addSample(Sample("ZttblFake", SType::Bkg, kPink+1));
  addSample(Sample("ZttclFake", SType::Bkg, kPink+1));
  addSample(Sample("ZttlFake", SType::Bkg, kPink+1));

  addSample(Sample("ZbbFake", SType::Bkg, kPink+1));
  addSample(Sample("ZbcFake", SType::Bkg, kPink+1));
  addSample(Sample("ZccFake", SType::Bkg, kPink+1));

  addSample(Sample("ZblFake", SType::Bkg, kPink+1));
  addSample(Sample("ZclFake", SType::Bkg, kPink+1));
  addSample(Sample("ZlFake", SType::Bkg, kPink+1));

  addSample(Sample("WWFake", SType::Bkg, kPink+1));
  addSample(Sample("ZZFake", SType::Bkg, kPink+1));
  addSample(Sample("WZFake", SType::Bkg, kPink+1));

  addSample(Sample("WFake", SType::Bkg, kPink+1));
  addSample(Sample("WttFake", SType::Bkg, kPink+1));

  addSample(Sample("ttHFake", SType::Bkg, kPink+1));
  addSample(Sample("VHFake", SType::Bkg, kPink+1));
  //
  */

  //LepHad combined and HadHad multi-jet fakes
  addSample(Sample("Fake", SType::DataDriven, kPink + 1));
  //addSample(Sample("QCD", SType::DataDriven, kPink + 1));

}

void SamplesBuilder_HH::declareKeywords() {
  // use standard keywords
  m_keywords = {
    {"Diboson", {"WZ", "ZZ", "WW"}},
    {"VHVV", {"WZ", "ZZ", "WW"}},
    {"Zjets", {"Zcl", "Zcc", "Zbl", "Zbb", "Zl", "Zbc"}},
    {"Zc", {"Zcl", "Zcc"}},
    {"Zb", {"Zbl", "Zbb", "Zbc"}},
    {"Zhf", {"Zbl", "Zbb", "Zbc", "Zcc"}},
    {"ZbORc", {"Zbl", "Zbb", "Zbc", "Zcl", "Zcc"}},
    {"Wjets", {"Wcl", "Wcc", "Wbl", "Wbb", "Wl", "Wbc"}},
    {"Wb", {"Wbl", "Wbb", "Wbc"}},
    {"Whf", {"Wcc", "Wbl", "Wbb", "Wbc"}},
    {"WbbORcc", {"Wcc", "Wbb"}},
    {"WbcORbl", {"Wbl", "Wbc"}},
    {"Stop", {"stops", "stopt", "stopWt"}},
    {"Top", {"ttbar", "stops", "stopt", "stopWt"}}
  };

  for (const auto& spair : m_samples) {
    const Sample& s = spair.second;

 // Higgs samples deserve shorthands too
    if(s.name().Contains("hhbbtautau")) {
      m_keywords["hh"].insert(s.name());
    }

    // Higgs samples deserve shorthands too
    if(s.name().Contains("ZvvH") && !s.name().Contains("MIL")) {
      m_keywords["ZvvH"].insert(s.name());
      m_keywords["Higgs"].insert(s.name());
      m_keywords["VHVV"].insert(s.name());
    }
    if(s.name().BeginsWith("WlvH")) {
      m_keywords["WlvH"].insert(s.name());
      m_keywords["Higgs"].insert(s.name());
      m_keywords["VHVV"].insert(s.name());
    }
    if(s.name().Contains("ZllH")) {
      m_keywords["ZllH"].insert(s.name());
      m_keywords["Higgs"].insert(s.name());
      m_keywords["VHVV"].insert(s.name());
    }
    if(s.name().BeginsWith("qq") || s.name().BeginsWith("Wlv")) {
      m_keywords["qqVH"].insert(s.name());
    }
    if(s.name().BeginsWith("qqZ")) {
      m_keywords["qqZH"].insert(s.name());
    }
    if(s.name().BeginsWith("ggZ")) {
      m_keywords["ggZH"].insert(s.name());
    }
  }



  // and add a few more

    //hadhad ttbar fakes from MC for now
    m_keywords["ttbarFakeMC"].insert({"ttbarFT", "ttbarTF", "ttbarFF"});

    //lephad fakes from MC for now
    // m_keywords["LHFakeMC"].insert({"ttbarFake", "stopsFake", "stoptFake", "stopWtFake", "ZttbbFake", "ZttbcFake", "ZttccFake", "ZttblFake","ZttclFake", "ZttlFake","ZbbFake","ZbcFake","ZccFake","ZblFake""ZclFake","ZlFake","WWFake","ZZFake","WZFake""WFake","WttFake","ttHFake","VHFake"});

    // MG aliases
    m_keywords["Zl"].insert("ZMGl"); 
    m_keywords["Zcl"].insert("ZMGcl"); 
    m_keywords["Zcc"].insert("ZMGcc"); 
    m_keywords["Zbl"].insert("ZMGbl"); 
    m_keywords["Zbc"].insert("ZMGbc"); 
    m_keywords["Zbb"].insert("ZMGbb"); 

    m_keywords["Zttl"].insert("ZttMGl"); 
    m_keywords["Zttcl"].insert("ZttMGcl"); 
    m_keywords["Zttcc"].insert("ZttMGcc"); 
    m_keywords["Zttbl"].insert("ZttMGbl"); 
    m_keywords["Zttbc"].insert("ZttMGbc"); 
    m_keywords["Zttbb"].insert("ZttMGbb"); 

    m_keywords["Wl"].insert("WMGl"); 
    m_keywords["Wcl"].insert("WMGcl"); 
    m_keywords["Wcc"].insert("WMGcc"); 
    m_keywords["Wbl"].insert("WMGbl"); 
    m_keywords["Wbc"].insert("WMGbc"); 
    m_keywords["Wbb"].insert("WMGbb"); 

    m_keywords["Wttl"].insert("WttMGl"); 
    m_keywords["Wttcl"].insert("WttMGcl"); 
    m_keywords["Wttcc"].insert("WttMGcc"); 
    m_keywords["Wttbl"].insert("WttMGbl"); 
    m_keywords["Wttbc"].insert("WttMGbc"); 
    m_keywords["Wttbb"].insert("WttMGbb"); 

    // Aliases for Zee/mumu or Ztautau (e.g. to apply same XS error to both)
    m_keywords["ZlOrZttl"].insert({"Zl", "Zttl", "ZMGl", "ZttMGl"});
    m_keywords["ZclOrZttcl"].insert({"Zcl", "Zttcl", "ZMGcl", "ZttMGcl"});
    m_keywords["ZccOrZttcc"].insert({"Zcc", "Zttcc","ZMGcc", "ZttMGcc"});
    m_keywords["ZblOrZttbl"].insert({"Zbl", "Zttbl","ZMGbl", "ZttMGbl"});
    m_keywords["ZbcOrZttbc"].insert({"Zbc", "Zttbc","ZMGbc", "ZttMGbc"});
    m_keywords["ZbbOrZttbb"].insert({"Zbb", "Zttbb","ZMGbb", "ZttMGbb"});

    // Aliases for Wenu/munu or Wtaunu (e.g. to apply same XS error to both)
    m_keywords["WlOrWttl"].insert({"Wl", "Wttl", "WMGl", "WttMGl"});
    m_keywords["WclOrWttcl"].insert({"Wcl", "Wttcl", "WMGcl", "WttMGcl"});
    m_keywords["WccOrWttcc"].insert({"Wcc", "Wttcc","WMGcc", "WttMGcc"});
    m_keywords["WblOrWttbl"].insert({"Wbl", "Wttbl","WMGbl", "WttMGbl"});
    m_keywords["WbcOrWttbc"].insert({"Wbc", "Wttbc","WMGbc", "WttMGbc"});
    m_keywords["WbbOrWttbb"].insert({"Wbb", "Wttbb","WMGbb", "WttMGbb"});

}

void SamplesBuilder_HH::declareSamplesToMerge() {
  declareMerging(Sample("VH125", SType::Bkg, kRed), {"Higgs"});
  declareMerging(Sample("diboson", SType::Bkg, kGray), {"Diboson"});
  declareMerging(Sample("stop", SType::Bkg, kYellow - 7), {"Stop"});
  declareMerging(Sample("Wjets", SType::Bkg, kGreen + 4), {"Wbb", "Wbc", "Wbl", "Wcc", "Wcl", "Wl"}); //, "W", "WMG"});
  declareMerging(Sample("Wttjets", SType::Bkg, kGreen + 4), {"Wttbb", "Wttbc", "Wttbl", "Wttcc", "Wttcl", "Wttl"}); //, "Wtt", "WMGtt"});
  declareMerging(Sample("Zhf", SType::Bkg, kAzure + 4), {"Zbb", "Zbc", "Zcc"});
  declareMerging(Sample("Ztthf", SType::Bkg, kAzure + 4), {"Zttbb", "Zttbc", "Zttcc"});
  declareMerging(Sample("Zlf", SType::Bkg, kAzure + 4), {"Zl", "Zcl", "Zbl"});
  declareMerging(Sample("Zttlf", SType::Bkg, kAzure + 4), {"Zttl", "Zttcl", "Zttbl"});

  //HadHad ttbar fakes from Data SFs

  declareMerging(Sample("ttbarFake", SType::DataDriven, kOrange - 6), {"ttbarSFFT", "ttbarSFTF", "ttbarSFFF"});

  //HadHad ttbar fakes from data FRs
  //declareMerging(Sample("ttbarFake", SType::DataDriven, kOrange - 6), {"ttbarFRFT", "ttbarFRTF", "ttbarFRFF"});

  //hadhad ttbar fakes from MC for now
  //declareMerging(Sample("ttbarFakeMC", SType::Bkg, kOrange - 6), {"ttbarFT", "ttbarTF", "ttbarFF"});

  //lephad fakes from MC for now
  //declareMerging(Sample("LHFakeMC", SType::Bkg, kPink+1), {"ttbarFake", "stopsFake", "stoptFake", "stopWtFake", "ZttbbFake", "ZttbcFake", "ZttccFake", "ZttblFake","ZttclFake", "ZttlFake","ZbbFake","ZbcFake","ZccFake","ZblFake", "ZclFake","ZlFake","WWFake","ZZFake","WZFake", "WFake","WttFake","ttHFake","VHFake"});

}



