#include "systematiclistsbuilder_hh.hpp"

#include <algorithm>
#include <map>
#include <unordered_map>
#include <vector>
#include <iostream>

#include <TString.h>

#include "WSMaker/configuration.hpp"
#include "WSMaker/containerhelpers.hpp"
#include "WSMaker/properties.hpp"
#include "WSMaker/sample.hpp"
#include "WSMaker/systematic.hpp"
#include "WSMaker/finder.hpp"

void SystematicListsBuilder_HH::fillHistoSystsRenaming() {

  // Renaming for Zll CR
  //m_renameHistoSysts.emplace("SysJET_SR1_JET_GroupedNP_1", "SysJET_GroupedNP_1");
  //m_renameHistoSysts.emplace("SysJET_SR1_JET_GroupedNP_2", "SysJET_GroupedNP_2");
  //m_renameHistoSysts.emplace("SysJET_SR1_JET_GroupedNP_3", "SysJET_GroupedNP_3");

  //Renaming HadHad ttbar shape uncertainties
  //m_renameHistoSysts.emplace("SysTTBAR_ACC_PS", "SysTHEO_ACC_PS_ttbar_HadHadSHAPE");

  //m_renameHistoSysts.emplace("SysTTBAR_ACC_GEN", "SysTHEO_ACC_ME_ttbar_HADHADSHAPE");

  //m_renameHistoSysts.emplace("SysTTBAR_ACC_ISR_PARAM", "SysTHEO_ACC_ISR_ttbar_HADHADSHAPE");
  // m_renameHistoSysts.emplace("SysTTBAR_ACC_FSR", "SysTHEO_ACC_FSR_ttbar_HADHADSHAPE");

}

void SystematicListsBuilder_HH::listAllUserSystematics(bool useFltNorms){
  using P = Property;

  //normFact("All", {{"MC", "DataDriven"}}, (10.0/3.2), 0, 10, true);
  std::vector<TString> sig_decorr = m_config.getStringList("DecorrPOI");

  TString type = m_config.getValue("Type", "RSG");
  TString massPoint = m_config.getValue("MassPoint", "1000");

  // for injection at an arbitrary mass setup in the config file
  int injection     = m_config.getValue("DoInjection", 0);
  TString massInjection = TString::Itoa(injection, 10);
  bool SMXSecLimit = m_config.getValue("SMXSecLimit", false);


  //std::map<int, double> xsecs2HDM = {{300, 0.00881197}, {400, 0.00269636}, {500, 0.00188319}};  // Median limit mus (combined-mass training)
  //std::map<int, double> xsecsRSG = {{300, 5.48578}, {400, 0.791713}, {500, 0.434995}};  // Median limit mus (combined-mass training)

  std::map<int, double> xsecs2HDM ={{300, 0.787063}, {400, 0.15832}, {500, 0.0530007}, {900, 0.0186458}, {1000, 0.0178296}, {1100, 0.0201121}};// hadhad median limit mu 
  std::map<int, double> xsecsRSG ={{300, 2.37155}, {400, 0.248326}, {500, 0.263015}};//hadhad median limit mu 


  double xsec_injected = 1;
  if (type.Contains("RSG")) xsec_injected = xsecsRSG.find(injection) != xsecsRSG.end() ? xsecsRSG[injection] : 1;
  if (type.Contains("2HDM")) xsec_injected = xsecs2HDM.find(injection) != xsecs2HDM.end() ? xsecs2HDM[injection] : 1;
  //if (type.Contains("SM")) xsec_injected = 1;
    if(type.Contains("SM")) xsec_injected = 4;//hadhad expected limit
      
  if(injection>0 && massInjection != massPoint) { 
   normFact("muInjection", {"None"}, xsec_injected, -10, 10, true); // this one is constant
  }

  /* 
  if (type.Contains("SM") || type.Contains("ZZ") || (type.Contains("RSG") && (massPoint.Contains("260"))) || (type.Contains("LQ") && (massPoint.Contains("300") || massPoint.Contains("1400") || massPoint.Contains("1300") || massPoint.Contains("1500")))) {
    normFact("SigScale", {"Sig"}, 1000, 0, 2000, true);  // to improve fit convergence
    if(massInjection == massPoint){
      xsec_injected /= 1000;
    }
  }
  */

  //Scale signal if needed
  //if (type.Contains("SM")){
  //  normFact("SigScale", {"Sig"}, 0.612, 0, 2000, true);
  //}

  // list them all !
  if(sig_decorr.size() != 0) { // TODO or just extend decorrSysForCategories for the POIs ?
    std::vector<Property> decorrelations;
    for(auto& d : sig_decorr) {
      decorrelations.push_back(Properties::props_from_names.at(d));
    }
    addPOI("SigXsecOverSM", {"Sig", {}, std::move(decorrelations)}, 1, -40, 40);
  } else if ( injection>0 || type == "SM") {
    addPOI("SigXsecOverSM", {"Sig"}, 1, -40, 40);
    //addPOI("SigXsecOverSM", {"Sig"}, xsec_injected, -40, 40);
  } else {
    addPOI("SigXsecOverSM", {"Sig"}, 1, -40, 40);
  }

  //return;  // HACK to allow scaling of lumi only for stat limits


  // ttbar ...

  // Cross-section
  bool fltTop = ((CategoryIndex::hasMatching(P::nTag==1) || CategoryIndex::hasMatching(P::nTag==2)) 
		 && CategoryIndex::hasMatching(P::spec=="TauLH")); //not floating ttbar for hadhad alone (this is true when there is lephad included)

  //fltTop = false; // Need this for hadhad alone.  TODO: make steerable so true for lephad alone (no L0 in regtrk)


  bool CombDecorrRatios = true;//include ratio uncertainties on ttbar for HadHad SR and ZCR relative to LH SR 

  if(useFltNorms && fltTop && CombDecorrRatios){//this happens in LepHad and HadHad+LepHad
    // //ttbar extrapolation unc. from LepHad SLT to HadHad SR calculated by Chris in 10/2020
    // normSys("SysTHEO_ACC_TTBAR_ME", 1+0.035, 1-0.035, {"ttbar", {{P::spec,"TauHH"}}});
    // normSys("SysTHEO_ACC_TTBAR_PS", 1+0.047, 1-0.047, {"ttbar", {{P::spec,"TauHH"}}});
    // normSys("SysTHEO_ACC_TTBAR_ISR", 1-0.0052, 1+0.0052, {"ttbar", {{P::spec,"TauHH"}}}); //remove as too small and very asymmetric?
    // normSys("SysTHEO_ACC_TTBAR_FSR", 1+0.0051, 1-0.035, {"ttbar", {{P::spec,"TauHH"}}}); //remove as very asymmetric?
    // normSys("SysTHEO_ACC_TTBAR_PDFalphas", 1-0.0065, 1+0.0065, {"ttbar", {{P::spec,"TauHH"}}}); //remove as too small?

    //All ttbar norms combined in 1 NP for HadHad extrapolation
    //normSys("THEO_ACC_NORM_ttbar_HadHadSR", 1-0.068, 1+0.059 , {"ttbar", {{P::spec,"TauHH"}}});//remove this and add value from ZCR to HadHad SR for ttbar

    //normSys("THEO_ACC_NORM_ttbarFake_HadHadSR", 1-0.068, 1+0.059 , {{{"ttbarSFTF", "ttbarSFFT", "ttbarSFFF"}}, {{P::spec,"TauHH"}}});//remove this and add value from ZCR to HadHad SR for ttbar

    //ttbar extrapolation from ZCR to HadHad SR
    //normSys("THEO_ACC_NORM_ttbar_HadHadSR", 1-0.062, 1+0.047, {"ttbar", {{P::spec,"TauHH"}}});

    //normSys("THEO_ACC_NORM_ttbarFake_HadHadSR", 1-0.062, 1+0.047, {{{"ttbarSFTF", "ttbarSFFT", "ttbarSFFF"}}, {{P::spec,"TauHH"}}});

    //applying separate extrapolation norms with same name as shapes to be correlated between norm and shape (extrapolation norm from CR to SR)

    // normSys("SysTTBAR_ACC_PS", 1-0.022, 1+0.022, {"ttbar", {{P::spec,"TauHH"}}});
    //  normSys("SysTTBAR_ACC_GEN", 1+0.037, 1-0.037, {"ttbar", {{P::spec,"TauHH"}}});
    //  normSys("SysTTBAR_ACC_ISR_PARAM", 1-0.0002, 1-0.003, {"ttbar", {{P::spec,"TauHH"}}});
    //  normSys("SysTTBAR_ACC_FSR", 1+0.019, 1-0.045, {"ttbar", {{P::spec,"TauHH"}}});
    //  normSys("SysTTBAR_PDFalphas", 1-0.0011, 1+0.0011, {"ttbar", {{P::spec,"TauHH"}}});

    //new names for new ttbar uncertainties from Chris in 04/2021

    normSys("SysTHEO_ACC_TTBAR_ME", 1+0.019, 1-0.019, {"ttbar", {{P::spec,"TauHH"}}});
    normSys("SysTHEO_ACC_TTBAR_PS_HadHad", 1-0.011, 1+0.011, {"ttbar", {{P::spec,"TauHH"}}});
    normSys("SysTHEO_ACC_TTBAR_ISR", 1+0.002, 1-0.002, {"ttbar", {{P::spec,"TauHH"}}});
    normSys("SysTHEO_ACC_TTBAR_FSR", 1+0.010, 1-0.023, {"ttbar", {{P::spec,"TauHH"}}});
    normSys("SysTHEO_ACC_TTBAR_PDFalphas", 1-0.0006, 1+0.0006, {"ttbar", {{P::spec,"TauHH"}}});


    // //ttbar extrapolation unc. from LepHad SLT to LepHad LTT SR calculated by Chirs in 10/2020
    // normSys("SysTHEO_ACC_TTBAR_ME", 1-0.012, 1+0.012, {"ttbar", {{P::LTT,1}}});
    // normSys("SysTHEO_ACC_TTBAR_PS", 1-0.015, 1+0.015, {"ttbar", {{P::LTT,1}}});
    // normSys("SysTHEO_ACC_TTBAR_ISR", 1-0.022, 1+0.022, {"ttbar", {{P::LTT,1}}}); //remove as asymmetric?
    // normSys("SysTHEO_ACC_TTBAR_FSR", 1+0.022, 1-0.022, {"ttbar", {{P::LTT,1}}}); //symmetrised here as up and down were on same side, remove?
    // normSys("SysTHEO_ACC_TTBAR_PDFalphas", 1-0.0014, 1+0.0014, {"ttbar", {{P::LTT,1}}}); //remove as too small?

    //All ttbar norms combined in 1 NP for Lephad LTT extrapolation
    //normSys("THEO_ACC_NORM_ttbar_LepHadLTTSR", 1-0.02, 1+0.028, {"ttbar", {{P::LTT,1}}});//remove this and add value from ZCR to LepHad LTT for ttbar

    //ttbar extrapolation from ZCR to LepHad LTT SR
    //normSys("THEO_ACC_NORM_ttbar_LepHadLTTSR", 1-0.094, 1+0.09, {"ttbar", {{P::LTT,1}}}); 

    //applying separate extrapolation norms with same name as shapes to be correlated between norms and shapes
    normSys("SysTHEO_ACC_TTBAR_ME", 1-0.005, 1+0.005 , {"ttbar", {{P::LTT,1}}});
    normSys("SysTHEO_ACC_TTBAR_PS_LTT", 1-0.044, 1+0.044, {"ttbar", {{P::LTT,1}}});
    normSys("SysTHEO_ACC_TTBAR_ISR", 1-0.007, 1+0.007, {"ttbar", {{P::LTT,1}}});
    normSys("SysTHEO_ACC_TTBAR_FSR", 1+0.0048, 1-0.016, {"ttbar", {{P::LTT,1}}});
    normSys("SysTHEO_ACC_TTBAR_PDFalphas", 1-0.0037, 1+0.0037, {"ttbar", {{P::LTT,1}}});

    //ttbar extrapolation from ZCR to SLT SR
    //    normSys("THEO_ACC_NORM_ttbar_LepHadSLTSR", 1-0.073, 1+0.074, {"ttbar", {{P::LTT,0}}});
    //applying separate extrapolation norms with same name as shapes to be correlated between norms and shapes
    normSys("SysTHEO_ACC_TTBAR_ME", 1+0.0013, 1-0.0013 , {"ttbar", {{P::LTT,0}}});
    normSys("SysTHEO_ACC_TTBAR_PS_SLT",1-0.036, 1+0.036 , {"ttbar", {{P::LTT,0}}});
    normSys("SysTHEO_ACC_TTBAR_ISR",1+0.0041, 1-0.0041 , {"ttbar", {{P::LTT,0}}});
    normSys("SysTHEO_ACC_TTBAR_FSR", 1+0.007, 1-0.0049 , {"ttbar", {{P::LTT,0}}});
    normSys("SysTHEO_ACC_TTBAR_PDFalphas", 1-0.003, 1+0.003 , {"ttbar", {{P::LTT,0}}});

    //ttbar extrapolation unc. from LepHad SLT to Z+HF CR calculated by Chirs in 10/2020 
    //normSys("THEO_ACC_ME_ttbar_ZCR", 0.0026, {"ttbar", {{P::nLep, 2}}}); //remove as too small?
    //normSys("THEO_ACC_PS_ttbar_ZCR", 0.067, {"ttbar", {{P::nLep, 2}}});
    //normSys("THEO_ACC_ISR_ttbar_ZCR", 1-0.0005, 1+0.0082, {"ttbar", {{P::nLep, 2}}}); //remove as too small and asymmetric?
    //normSys("THEO_ACC_FSR_ttbar_ZCR", 1-0.014, 1+0.0098, {"ttbar", {{P::nLep, 2}}});
    //normSys("THEO_ACC_PDFalphas_ttbar_ZCR", 0.0059, {"ttbar", {{P::nLep, 2}}});
    //All ttbar norms combined in 1 NP for Z+HF CR extrapolation
    //normSys("THEO_ACC_NORM_ttbar_ZCR", 0.069, {"ttbar", {{P::nLep, 2}}});//remove this as ttbar from CR now

    //floating ttbar norm
    //normFact("ttbar", {"ttbar"});
    normFact("ttbar", {{"ttbar", "ttbarSFTF", "ttbarSFFT", "ttbarSFFF"}}, 1, 0, 2, true);//HadHad ttbar fakes have to float together with true ttbar because of how the SFs were derived

  } else if(useFltNorms && fltTop) {
    normFact("ttbar", {"ttbar"}); 

  } else {//this happens in HadHadOnly

    //try floating ttbar also in HadHad only fit and applying extrapolation from CR to SR
    normFact("ttbar", {{"ttbar", "ttbarSFTF", "ttbarSFFT", "ttbarSFFF"}});
    
    //normSys("THEO_ACC_NORM_ttbar_HadHadSR", 1-0.062, 1+0.047 , {"ttbar", {{P::spec,"TauHH"}}});

    //normSys("THEO_ACC_NORM_ttbarFake_HadHadSR", 1-0.062, 1+0.047 , {{{"ttbarSFTF", "ttbarSFFT", "ttbarSFFF"}}, {{P::spec,"TauHH"}}});

    //  normSys("SysTTBAR_ACC_PS", 1-0.022, 1+0.022, {"ttbar", {{P::spec,"TauHH"}}});
    //  normSys("SysTTBAR_ACC_GEN", 1+0.037, 1-0.037, {"ttbar", {{P::spec,"TauHH"}}});
    //  normSys("SysTTBAR_ACC_ISR_PARAM", 1-0.0002, 1-0.003, {"ttbar", {{P::spec,"TauHH"}}});
    //  normSys("SysTTBAR_ACC_FSR", 1+0.019, 1-0.045, {"ttbar", {{P::spec,"TauHH"}}});
    //  normSys("SysTTBAR_PDFalphas", 1-0.0011, 1+0.0011, {"ttbar", {{P::spec,"TauHH"}}});

    //new names for new ttbar uncertainties from Chris in 04/2021

    normSys("SysTHEO_ACC_TTBAR_ME", 1+0.037, 1-0.037, {"ttbar", {{P::spec,"TauHH"}}});
    normSys("SysTHEO_ACC_TTBAR_PS_HadHad", 1-0.022, 1+0.022, {"ttbar", {{P::spec,"TauHH"}}});
    normSys("SysTHEO_ACC_TTBAR_ISR", 1+0.003, 1-0.003, {"ttbar", {{P::spec,"TauHH"}}});
    normSys("SysTHEO_ACC_TTBAR_FSR", 1+0.019, 1-0.045, {"ttbar", {{P::spec,"TauHH"}}});
    normSys("SysTHEO_ACC_TTBAR_PDFalphas", 1-0.0011, 1+0.0011, {"ttbar", {{P::spec,"TauHH"}}});


    /*
    //ttbar xsec
    normSys("THEO_XS_ttbar", 0.06, {"ttbar"});

    //ttbar acc. uncert. in HadHad SR calculated by Chirs in 10/2020 (taken from shape+norm hists)

    //normSys("THEO_ACC_ME_ttbar", 0.081, {"ttbar", {{P::spec,"TauHH"}}});
    //normSys("THEO_ACC_PS_ttbar", 0.073, {"ttbar", {{P::spec,"TauHH"}}});
    //normSys("THEO_ACC_FSR_ttbar", 1-0.081, 1+0.045, {"ttbar", {{P::spec,"TauHH"}}});
    //All ttbar norms combined in 1 NP (HadHad standalone norm uncertainty)
    normSys("THEO_ACC_NORM_ttbar", 1-0.14, 1+0.12, {"ttbar", {{P::spec,"TauHH"}}});

    //ttbar acc  uncert. in Z+HF CR calculated by Yanlin in 09/2020

    //normSys("THEO_ACC_ME_ttbar", 0.047, {"ttbar", { {P::nLep, 2}}});
    //normSys("THEO_ACC_PS_ttbar", 0.094, {"ttbar", {{P::nLep, 2}}});
    //normSys("THEO_ACC_ISR_ttbar", 0.12, {"ttbar", {{P::nLep, 2}}});
    //normSys("THEO_ACC_FSR_ttbar", 1-0.038, 1+0.025, {"ttbar", {{P::nLep, 2}}});
    //normSys("THEO_ACC_PDFalphas_ttbar", 0.029, {"ttbar", {{P::nLep, 2}}});
    //All ttbar norms combined in 1 NP (ZHF CR standalone norm uncertainty)
    normSys("THEO_ACC_NORM_ttbar", 0.16, {"ttbar", {{P::nLep, 2}}}); 
    */
  }

  // single top .... 

  // cross-sections from Top MC Twiki: https://twiki.cern.ch/twiki/bin/view/LHCPhysics/SingleTopRefXsec
  normSys("THEO_XS_Stop", 0.019, {"stops"});
  normSys("THEO_XS_Stop", 0.021, {"stopt"});
  normSys("THEO_XS_Stop", 0.027, {"stopWt"});

  //stop acceptance from VHbb 
  normSys("THEO_ACC_Stopst", 0.1, {"stops"});
  normSys("THEO_ACC_Stopst", 0.1, {"stopt"});

  // normSys("THEO_ACC_Stop", 0.55, {"stopWt", {{P::spec,"TauLH"}}});//to be updated with numbers from Jordan
  // lephad LTT
  normSys("SysTHEO_ACC_StopWt_ME", 1-0.077, 1+0.077 , {"stopWt", {{P::LTT,1}}});
  normSys("SysTHEO_ACC_StopWt_PS", 1-0.047, 1+0.047, {"stopWt", {{P::LTT,1}}});
  normSys("SysTHEO_ACC_StopWt_TopInterference", 1+0.057, 1-0.057, {"stopWt", {{P::LTT,1}}});
  normSys("SysTHEO_ACC_StopWt_FSR", 1-0.035, 1+0.018, {"stopWt", {{P::LTT,1}}});
  normSys("SysTHEO_ACC_StopWt_ISR", 1-0.023, 1+0.031, {"stopWt", {{P::LTT,1}}});
  normSys("SysTHEO_ACC_StopWt_PDF", 1-0.016, 1+0.016, {"stopWt", {{P::LTT,1}}}); // missing for lephad, use hadhad number
  // lephad SLT
  normSys("SysTHEO_ACC_StopWt_ME", 1-0.011, 1+0.011 , {"stopWt", {{P::LTT,0}}});
  normSys("SysTHEO_ACC_StopWt_PS", 1+0.039, 1-0.039, {"stopWt", {{P::LTT,0}}});
  normSys("SysTHEO_ACC_StopWt_TopInterference", 1+0.039, 1-0.039, {"stopWt", {{P::LTT,0}}});
  normSys("SysTHEO_ACC_StopWt_FSR", 1-0.027, 1+0.022, {"stopWt", {{P::LTT,0}}});
  normSys("SysTHEO_ACC_StopWt_ISR", 1-0.024, 1+0.032, {"stopWt", {{P::LTT,0}}});
  normSys("SysTHEO_ACC_StopWt_PDF", 1-0.016, 1+0.016, {"stopWt", {{P::LTT,0}}}); // missing for lephad, use hadhad number
  // normSys("THEO_ACC_Stop", 0.33, {"stopWt", {{P::spec,"TauHH"}}});//calculated by Bowen in 03/2021
  // hadhad
  normSys("SysTHEO_ACC_StopWt_ME", 1+0.025, 1-0.025, {"stopWt", {{P::spec,"TauHH"}}});
  normSys("SysTHEO_ACC_StopWt_PS", 1-0.08, 1+0.08, {"stopWt", {{P::spec,"TauHH"}}});
  normSys("SysTHEO_ACC_StopWt_TopInterference", 1+0.14, 1-0.14, {"stopWt", {{P::spec,"TauHH"}}});  
  normSys("SysTHEO_ACC_StopWt_FSR", 1-0.025, 1+0.033, {"stopWt", {{P::spec,"TauHH"}}});
  normSys("SysTHEO_ACC_StopWt_ISR", 1-0.047, 1+0.044, {"stopWt", {{P::spec,"TauHH"}}});  
  normSys("SysTHEO_ACC_StopWt_PDF", 1-0.016, 1+0.016, {"stopWt", {{P::spec,"TauHH"}}});//Calculated by Bowen in 03/2021

  // W+jets ...

  // Cross-section
  normSys("THEO_XS_V", 0.025, {{"Wjets", "Wttjets", "MG", "WttMG", "W", "Wtt"}});
 
  //W+jets acceptance from VHbb
  normSys("THEO_ACC_W", 0.185, {{{"Wjets", "Wttjets", "MG", "WttMG", "W", "Wtt"}}, {{P::spec,"TauLH"}}});//acceptance uncertainty taken from VHbb
  normSys("THEO_ACC_W", 0.25, {{{"Wjets", "Wttjets", "MG", "WttMG", "W", "Wtt"}}, {{P::spec,"TauHH"}}});//larger for hadhad to take into account fakes
 

  // Z+jets ....

  // This boolean is not used
  // bool floatHFOnlyForZ = m_config.getValue("FloatHFOnlyForZ", false);

  //xsec on Z+lf
  normSys("THEO_XS_V", 0.025, {{"Zttl", "Zttcl", "Zttbl", "Zl", "Zcl", "Zbl"}});  

  //acceptance on Z+lf from VHbb
  normSys("THEO_ACC_Zlf", 0.115, {{"Zl", "Zcl", "Zbl"}});   //acceptance uncertainty taken from VHbb
  normSys("THEO_ACC_Zlf", 0.115, {{"Zttl", "Zttcl", "Zttbl"}}); 
   

  // floating Zbb+Zbc+Zcc
  if(useFltNorms && CategoryIndex::hasMatching(P::nTag==2) && CategoryIndex::hasMatching(P::nLep==2)) { 

    //floating Z+HF normalisation if Z+HF CR is present
    normFact("Zhf", {{"Zbb" , "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}}, 1, 0, 2, true);
    // normFact("Zbb", {{"ZbbOrZttbb", "ZbcOrZttbc", "ZccOrZttcc"}});

    if(CombDecorrRatios) {
      //Z+HF extrapolation from ZCR to HadHad SR calculated by Chris in 10/2020      
      //normSys("THEO_ACC_SCALE_Zhf_HadHadSR", 1-0.096, 1+0.12, {{{"Zbb", "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}}, {{P::spec,"TauHH"}}});
      //normSys("THEO_ACC_CKKW_Zhf_HadHadSR", 0.053, {{{"Zbb", "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}}, {{P::spec,"TauHH"}}});
      //normSys("THEO_ACC_QSF_Zhf_HadHadSR", 0.06, {{{"Zbb", "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}}, {{P::spec,"TauHH"}}});
      //normSys("THEO_ACC_PDFalphas_Zhf_HadHadSR", 0.0077, {{{"Zbb", "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}}, {{P::spec,"TauHH"}}});
      //All norms on Z+HF combined in 1 NP for HadHad SR
      
      //normSys("THEO_ACC_NORM_Zhf_HadHadSR", 1-0.13, 1+0.14, {{{"Zbb", "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}}, {{P::spec,"TauHH"}}});

      //splitting Z+hf norms in different sources and correlate MG one to shape
      normSys("SysTHEO_ACC_Zhf_SCALE", 1-0.048, 1+0.06, {{{"Zbb", "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}}, {{P::spec,"TauHH"}}});
      normSys("SysTHEO_ACC_Zhf_CKKW", 1+0.027, 1-0.027, {{{"Zbb", "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}}, {{P::spec,"TauHH"}}});
      normSys("SysTHEO_ACC_Zhf_QSF", 1+0.03, 1-0.03, {{{"Zbb", "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}}, {{P::spec,"TauHH"}}});
      normSys("SysTHEO_ACC_Zhf_PDFChoice", 1-0.0049, 1+0.0049, {{{"Zbb", "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}}, {{P::spec,"TauHH"}}});
      normSys("SysTHEO_ACC_Zhf_PDFalphas", 1-0.0039, 1+0.0039, {{{"Zbb", "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}}, {{P::spec,"TauHH"}}});
      normSys("SysTHEO_ACC_Zhf_GENERATOR", 1-0.035, 1+0.035, {{{"Zbb", "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}}, {{P::spec,"TauHH"}}});


      //Z+HF extrapolation from ZCR to LepHad SLT SR calculated by Chris in 10/2020
      //normSys("THEO_ACC_SCALE_Zhf_LepHadSLTSR", 1-0.029, 1+0.053, {{{"Zbb" , "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}}, {{P::LTT, 0}}});
      //normSys("THEO_ACC_CKKW_Zhf_LepHadSLTSR", 0.07, {{{"Zbb" , "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}}, {{P::LTT, 0}}});
      //normSys("THEO_ACC_QSF_Zhf_LepHadSLTSR", 0.016, {{{"Zbb" , "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}}, {{P::LTT, 0}}});
      //normSys("THEO_ACC_PDFalphas_Zhf_LepHadSLTSR", 0.0026, {{{"Zbb" , "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}}, {{P::LTT, 0}}});
      //All norms on Z+HF combined in 1 NP for LepHad SLT SR
      //normSys("THEO_ACC_NORM_Zhf_LepHadSLTSR", 1-0.078, 1+0.09, {{{"Zbb" , "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}}, {{P::LTT, 0}}});

      //Apply separate extrapolation norms with same name as shapes to correalte between norms and shapes
      normSys("SysTHEO_ACC_Zhf_SCALE", 1-0.015, 1+0.027, {{{"Zbb", "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}}, {{P::LTT,0}}});//scale
      normSys("SysTHEO_ACC_Zhf_CKKW",1+0.035, 1-0.035 , {{{"Zbb", "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}}, {{P::LTT,0}}});
      normSys("SysTHEO_ACC_Zhf_QSF", 1-0.008, 1+0.008, {{{"Zbb", "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}}, {{P::LTT,0}}});
      normSys("SysTHEO_ACC_Zhf_PDFChoice", 1-0.0013, 1+ 0.0013, {{{"Zbb", "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}}, {{P::LTT,0}}});
      normSys("SysTHEO_ACC_Zhf_PDFalphas", 1-0.0049, 1+0.0049, {{{"Zbb", "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}}, {{P::LTT,0}}}); 
      normSys("SysTHEO_ACC_Zhf_GENERATOR", 1+0.011, 1-0.011, {{{"Zbb", "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}}, {{P::LTT,0}}});


      //Z+HF extrapolation from ZCR to LepHad LTT SR calculated by Chirs in 10/2020 
      //normSys("THEO_ACC_SCALE_Zhf_LepHadLTTSR", 1-0.054, 1+0.085, {{{"Zbb" , "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}}, {{P::LTT, 1}}});
      //normSys("THEO_ACC_CKKW_Zhf_LepHadSLTTSR", 0.071, {{{"Zbb" , "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}}, {{P::LTT, 1}}});
      //normSys("THEO_ACC_QSF_Zhf_LepHadLTTSR", 0.016, {{{"Zbb" , "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}}, {{P::LTT, 1}}});
      //normSys("THEO_ACC_PDFalphas_Zhf_LepHadLTTSR", 0.011, {{{"Zbb" , "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}}, {{P::LTT, 1}}});
      //All norms on Z+HF combined in 1 NP for LepHad LTT SR
      //normSys("THEO_ACC_NORM_Zhf_LepHadLTTSR", 1-0.091, 1+0.11, {{{"Zbb" , "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}}, {{P::LTT, 1}}});

      //Apply separate extrapolation norms with same name as shapes to correlate between norms and shapes
      normSys("SysTHEO_ACC_Zhf_SCALE",1-0.027, 1+0.043 , {{{"Zbb", "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}}, {{P::LTT,1}}});
      normSys("SysTHEO_ACC_Zhf_CKKW", 1+0.036, 1-0.036, {{{"Zbb", "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}}, {{P::LTT,1}}});
      normSys("SysTHEO_ACC_Zhf_QSF", 1-0.008 , 1+0.008, {{{"Zbb", "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}}, {{P::LTT,1}}});
      normSys("SysTHEO_ACC_Zhf_PDFChoice", 1-0.0017 , 1+0.0017 , {{{"Zbb", "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}}, {{P::LTT,1}}});
      normSys("SysTHEO_ACC_Zhf_PDFalphas", 1-0.006, 1+0.006, {{{"Zbb", "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}}, {{P::LTT,1}}});
      normSys("SysTHEO_ACC_Zhf_GENERATOR", 1+0.05, 1-0.05, {{{"Zbb", "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}}, {{P::LTT,1}}});

    }
  } else {//if not Z+HF control region do not float Z+HF, scale it by 1.3 and assing dummy uncertainty of 10% BUT DO NOT USE THIS!!!
    normFact("ZhfScale", {{"Zbb" , "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}}, 1.35, 1, 2, true);
    normSys("THEO_ACC_Zhf", 0.1, {{"Zbb", "Zttbb", "Zbc", "Zttbc", "Zcc", "Zttcc"}});
  }  

 

  // Diboson ...
  
  // Cross section from A/H->tautau and H+->taunu
  //sampleNormSys("diboson", 0.06);
    if (type == "ZZ") {
      sampleNormSys("WZ", 0.06);  
      sampleNormSys("WW", 0.06);  
      sampleNormSys("ZZvvqq", 0.06);  
      sampleNormSys("hhttbb", 0.50);        
    } else {
      //sampleNormSys("Diboson", 0.06);  // Note capital Diboson here
      normSys("THEO_XS_Diboson", 0.03, {"Diboson"});
      normSys("THEO_ACC_Diboson", 0.10, {"ZZ"});//acceptance from VHbb https://cds.cern.ch/record/2714885/files/ATLAS-CONF-2020-006.pdf
      normSys("THEO_ACC_Diboson", 0.13, {"WZ"});
      normSys("THEO_ACC_Diboson", 0.125, {"WW"});
    }


  // SM Higgs ...

  //add Higgs normalisation systematic of 28% when running resonance fits, based on 13 TeV evidence
  //normSys("SysHiggsNorm", 0.28, {{"Higgs", "ZllH125", "VH"}});
  //normSys("SysttHNorm", 0.30, {"ttH"});


  //Full Run2 lumi unc
  normSys("ATLAS_LUMI_Run2", 0.01, {"MC", {{P::year, 2015}}});

  //HH SM Signal xsec uncertainties from LHCHHWG
  //these should only be included for the limit on mu, not for the limit on the cross section
  if (!SMXSecLimit){
    //old recommendations
    //normSys("THEO_XS_SCALE_ggFSMHH", 1-0.05, 1+0.022, {"hhttbb"});
    //normSys("THEO_XS_PDF_ggFSMHH", 0.021, {"hhttbb"});
    //normSys("THEO_XS_alphas_ggFSMHH", 0.021, {"hhttbb"});
    //normSys("THEO_XS_Mtop_ggFSMHH", 0.026, {"hhttbb"});

    //new ggF xsec uncertainties recommendations
    normSys("THEO_XS_SCALEMTop_ggFSMHH", 1-0.115, 1+0.03 , {"hhttbb"});
    normSys("THEO_XS_PDFalphas_ggFSMHH", 0.015 , {"hhttbb"});

    normSys("THEO_XS_SCALE_VBFSMHH", 1-0.0002, 1+0.0002, {"hhttbbVBFSM"});
    normSys("THEO_XS_PDFalphas_VBFSMHH", 0.011, {"hhttbbVBFSM"}); 
  }

  //Higgs decay BR uncertainties from https://twiki.cern.ch/twiki/bin/view/LHCPhysics/CERNYellowReportPageBR
  //normFact("BRScale_Hbb", {{"Sig", "ZHbb", "WHbb"}}, 1.0);
  normSys("THEO_BR_Hbb", 1-0.007, 1+0.006, {{"Sig", "ggZHbb","qqZHbb", "WHbb"}});
  //normFact("BRScale_Htautau", {{"Sig", "ZHtautau", "WHtautau", "ggFHtautau", "VBFHtautau"}}, 1.0);
  normSys("THEO_BR_Htautau", 1-0.01, 1+0.011, {{"Sig", "ggZHtautau","qqZHtautau", "WHtautau", "ggFHtautau", "VBFHtautau"}});

  //Single-Higgs xsec uncertainties from https://twiki.cern.ch/twiki/bin/view/LHCPhysics/CERNYellowReportPageAt13TeV
  normSys("THEO_XS_SCALE_ttH", 1-0.046, 1+0.029, {"ttH"});
  normSys("THEO_XS_PDFalphas_ttH", 0.018, {"ttH"});

  normSys("THEO_XS_SCALE_ggFH", 0.020, {"ggFHtautau"});
  normSys("THEO_XS_PDFalphas_ggFH", 0.016, {"ggFHtautau"});
  normSys("THEO_ACC_HF_ggFH", 0.495, {"ggFHtautau"});

  normSys("THEO_XS_SCALE_qqZH", 1-0.003, 1+0.003, {{"qqZHbb", "qqZHtautau"}});
  normSys("THEO_XS_PDFalphas_qqZH", 0.010, {{"qqZHbb", "qqZHtautau"}});

  normSys("THEO_XS_SCALE_ggZH", 1-0.095, 1+0.125, {{"ggZHbb", "ggZHtautau"}});
  normSys("THEO_XS_PDFalphas_ggZH", 0.012, {{"ggZHbb", "ggZHtautau"}});

  normSys("THEO_XS_SCALE_WH", 1-0.004, 1+0.003, {{"WHbb", "WHtautau"}});
  normSys("THEO_XS_PDFalphas_WH", 0.010, {{"WHbb", "WHtautau"}});
  normSys("THEO_ACC_HF_WH", 0.495, {"WHtautau"});

  normSys("THEO_XS_SCALE_VBFH", 1-0.002, 1+0.002, {"VBFHtautau"});
  normSys("THEO_XS_PDFalphas_VBFH", 0.011, {"VBFHtautau"});
  normSys("THEO_ACC_HF_VBFH", 0.495, {"VBFHtautau"});

  //singleH acceptance HadHad
  normSys("THEO_ACC_SCALE_ttH", 0.005, {"ttH", {{P::spec,"TauHH"}}});//calculated by Alessandra in 07/2020
  normSys("THEO_ACC_FSR_ttH", 1-0.03, 1+0.019, {"ttH", {{P::spec,"TauHH"}}});//calculated by Alessandra in 07/2020
  normSys("THEO_ACC_PS_ttH", 0.006, {"ttH", {{P::spec,"TauHH"}}});//calculated by Alessandra in 10/2020
  normSys("THEO_ACC_GEN_ttH", 0.017, {"ttH", {{P::spec,"TauHH"}}});//calculated by Alessandra in 10/2020
  normSys("THEO_ACC_SCALE_ZHbb", 0.009, {{"qqZHbb", "ggZHbb"}, {{P::spec,"TauHH"}}});//calculated by Alessandra in 07/2020
  normSys("THEO_ACC_PS_ZHbb", 0.06, {{"qqZHbb", "ggZHbb"}, {{P::spec,"TauHH"}}});//calculated by Alessandra in 10/2020 
  normSys("THEO_ACC_PDFalphas_ZHtautau", 0.005, {{"qqZHtautau", "ggZHtautau"}, {{P::spec,"TauHH"}}});//calculated by Alessandra in 07/2020
  normSys("THEO_ACC_SCALE_ZHtautau", 0.013, {{"qqZHtautau", "ggZHtautau"}, {{P::spec,"TauHH"}}});//calculated by Alessandra in 07/2020
  normSys("THEO_ACC_PS_ZHtautau", 0.028, {{"qqZHtautau", "ggZHtautau"}, {{P::spec,"TauHH"}}});//calculated by Alessandra in 03/2021

  //singleH acceptance LepHad SLT 
  normSys("THEO_ACC_FSR_ttH", 1-0.026, 1+0.016, {"ttH", {{P::LTT, 0}}});//calculated by Jem in 08/2020
  normSys("THEO_ACC_PS_ttH", 0.007, {"ttH", {{P::LTT, 0}}});
  normSys("THEO_ACC_SCALE_ZHbb", 0.015, {{"qqZHbb", "ggZHbb"}, {{P::LTT, 0}}});//calculated by Jem in 08/2020
  normSys("THEO_ACC_PS_ZHbb", 0.055, {{"qqZHbb", "ggZHbb"}, {{P::LTT, 0}}});//calculated by Jem in 08/2020
  normSys("THEO_ACC_SCALE_ZHtautau", 0.011, {{"qqZHtautau", "ggZHtautau"}, {{P::LTT, 0}}});//calculated by Jem in 08/2020

  //singleH acceptance LepHad LTT
  normSys("THEO_ACC_FSR_ttH", 1-0.075, 1+0.028, {"ttH", {{P::LTT, 1}}});//calculated by Jem in 11/2020
  normSys("THEO_ACC_ISR_ttH", 0.005, {"ttH", {{P::LTT, 1}}});//calculated by Jem in 11/2020
  normSys("THEO_ACC_PS_ttH", 0.034, {"ttH", {{P::LTT, 1}}});
  normSys("THEO_ACC_GEN_ttH", 0.010, {"ttH", {{P::LTT, 1}}});//calculated by Jem in 11/2020
  normSys("THEO_ACC_SCALE_ZHbb", 0.013, {{"qqZHbb", "ggZHbb"}, {{P::LTT, 1}}});//calculated by Jem in 11/2020
  normSys("THEO_ACC_PS_ZHbb", 0.019, {{"qqZHbb", "ggZHbb"}, {{P::LTT, 1}}});//calculated by Jem in 11/2020
  normSys("THEO_ACC_PDFalphas_ZHtautau", 0.006, {{"qqZHtautau", "ggZHtautau"}, {{P::LTT, 1}}});//calculated by Jem in 11/2020
  normSys("THEO_ACC_SCALE_ZHtautau", 0.014, {{"qqZHtautau", "ggZHtautau"}, {{P::LTT, 1}}});//calculated by Jem in 11/2020



  if(type.Contains("SM")){//acceptance unc. on Non Res SM signal
   normSys("THEO_ACC_PS_ggFSMHH", 0.022, {"hhttbb", {{P::spec,"TauHH"}}});//calculated by Bowen in 04/2021
   normSys("THEO_ACC_SCALE_ggFSMHH", 0.007, {"hhttbb", {{P::spec,"TauHH"}}});//calculated in 03/2021
   normSys("THEO_ACC_PS_ggFSMHH", 0.038, {"hhttbb", {{P::LTT,0}}});//calculated by Jem in 05/2021
   normSys("THEO_ACC_PS_ggFSMHH", 0.038, {"hhttbb", {{P::LTT,1}}});//calculated by Jem in 05/2021 
   normSys("THEO_ACC_SCALE_ggFSMHH", 0.006, {"hhttbb", {{P::LTT, 0}}});//calculated by Jem in 05/2021
   normSys("THEO_ACC_SCALE_ggFSMHH", 0.005, {"hhttbb", {{P::LTT,1}}});//calculated by Jem in 05/2021

   normSys("THEO_ACC_PDFalphas_VBFSMHH", 0.005, {"hhttbbVBFSM", {{P::spec,"TauHH"}}});//calculated by Chris in 06/2021
   normSys("THEO_ACC_PS_VBFSMHH", 0.015, {"hhttbbVBFSM", {{P::spec,"TauHH"}}});//calculated by Chris in 06/2021
   //scales missing here because added as shapes
   
   normSys("THEO_ACC_PS_VBFSMHH", 0.032, {"hhttbbVBFSM", {{P::LTT,0}}});//calculated by Jem in 06/2021
   normSys("THEO_ACC_PS_VBFSMHH", 0.011, {"hhttbbVBFSM", {{P::LTT,1}}});//calculated by Jem in 06/2021
   normSys("THEO_ACC_SCALE_VBFSMHH", 0.005, {"hhttbbVBFSM", {{P::spec,"TauLH"}}});//calculated by Jem in 06/2021
   //PDFalphas missing as negligible

  }

 if(type.Contains("2HDM")){//acceptance unc. on Resonant signal (from PS as rest is negligible)

    normSys("THEO_ACC_PS_ggFHH", 0.05, {"Sig", {{P::spec,"TauHH"}}});//calculated by Alessandra in 05/2020 
    normSys("THEO_ACC_PS_ggFHH", 0.03, {"Sig", {{P::LTT,0}}}); //calculated by Nicholas in 06/2021
    normSys("THEO_ACC_PS_ggFHH", 0.035, {"Sig", {{P::LTT,1}}}); //calculated by Nicholas in 06/2021

    if (!(massPoint.Contains("1100") || massPoint.Contains("1200") || massPoint.Contains("1400") || massPoint.Contains("1600"))){
      normSys("THEO_ACC_AF2_ggFHH", 0.032, {"Sig", {{P::spec,"TauHH"}}});//calculated by Chris in 06/2021
      normSys("THEO_ACC_AF2_ggFHH", 0.015, {"Sig", {{P::LTT,1}}});//calculated by Jordan in 06/2021
    }
  }

 //HadHad multijet FFs stat uncertainty
 normSys("FF_Stat_HadHadSR", 0.014,  {"Fake", {{P::spec,"TauHH"}}});
  
}


void SystematicListsBuilder_HH::listAllHistoSystematics() {
  using T = SysConfig::Treat;
  using S = SysConfig::Smooth;
  using P = Property;
  // these flags are not used
  // bool yes(true);     bool no(false); // correlate years ?

  SysConfig noSmoothConfig { T::shape, S::noSmooth};
  SysConfig smoothConfig { T::shape, S::smoothRebinMonotonic};
  SysConfig skipConfig { T::skip, S::noSmooth };
  SysConfig ShapeOnlySmoothConfig {T::shapeonly, S::smoothRebinMonotonic};
  SysConfig ShapeOnlynoSmoothConfig {T::shapeonly, S::noSmooth};
  SysConfig smoothSymmConfig {T::shape, S::smoothRebinMonotonic, SysConfig::Symmetrise::symmetriseOneSided};
  SysConfig smoothSymmAverageConfig {T::shape, S::smoothRebinMonotonic, SysConfig::Symmetrise::symmetriseAverage};
  SysConfig noSmoothSymmConfig { T::shape, S::noSmooth, SysConfig::Symmetrise::symmetriseOneSided};
  SysConfig ShapeOnlySmoothSymmConfig {T::shapeonly, S::smoothRebinMonotonic, SysConfig::Symmetrise::symmetriseOneSided};
  SysConfig ShapeOnlySmoothSymmAverageConfig {T::shapeonly, S::smoothRebinMonotonic, SysConfig::Symmetrise::symmetriseAverage};
  SysConfig ShapeOnlynoSmoothSymmConfig {T::shapeonly, S::noSmooth, SysConfig::Symmetrise::symmetriseOneSided};
  SysConfig ShapeOnlynoSmoothSymmAverageConfig {T::shapeonly, S::noSmooth, SysConfig::Symmetrise::symmetriseAverage};
  SysConfig noSLTSmoothConfig = { T::shape, S::smoothRebinMonotonic, SysConfig::Symmetrise::noSym, std::vector<TString>(), {{P::spec,"TauHH"}, {P::LTT, 1}}};
  SysConfig noSLTnoSmoothConfig = { T::shape, S::noSmooth, SysConfig::Symmetrise::noSym, std::vector<TString>(), {{P::spec,"TauHH"}, {P::LTT, 1}}};
  SysConfig ShapeOnlySmoothLepHadOnlySymmConfig {T::shapeonly, Smoothing_HH::SmoothLepHadOnly, SysConfig::Symmetrise::symmetriseOneSided, std::vector<TString>()};

  //CP uncertainties

  //Weight systematics

  m_histoSysts.insert({ "SysPRW_DATASF", noSmoothConfig});
  m_histoSysts.insert({ "SysJET_JvtEfficiency", noSmoothConfig});
  m_histoSysts.insert({ "SysJET_fJvtEfficiency", skipConfig});//we don't use fwd jets

  //btagging
  m_histoSysts.insert({ "SysFT_EFF_Eigen_B_0", noSmoothConfig}); //we used smoothConfing in the past as it seemed to make things more stable, to be double checked
  m_histoSysts.insert({ "SysFT_EFF_Eigen_B_1", noSmoothConfig});
  m_histoSysts.insert({ "SysFT_EFF_Eigen_B_2", noSmoothConfig});
  m_histoSysts.insert({ "SysFT_EFF_Eigen_C_0", noSmoothConfig});
  m_histoSysts.insert({ "SysFT_EFF_Eigen_C_1", noSmoothConfig});
  m_histoSysts.insert({ "SysFT_EFF_Eigen_C_2", noSmoothConfig});
  m_histoSysts.insert({ "SysFT_EFF_Eigen_C_3", noSmoothConfig});
  m_histoSysts.insert({ "SysFT_EFF_Eigen_Light_0", noSmoothConfig});
  m_histoSysts.insert({ "SysFT_EFF_Eigen_Light_1", noSmoothConfig});
  m_histoSysts.insert({ "SysFT_EFF_Eigen_Light_2", noSmoothConfig});
  m_histoSysts.insert({ "SysFT_EFF_Eigen_Light_3", noSmoothConfig});
  m_histoSysts.insert({ "SysFT_EFF_extrapolation", noSmoothConfig});
  m_histoSysts.insert({ "SysFT_EFF_extrapolation_from_charm", noSmoothConfig});
 
  //taus
  //Weight systs
  m_histoSysts.insert({ "SysTAUS_TRUEELECTRON_EFF_ELEBDT_STAT", skipConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEELECTRON_EFF_ELEBDT_SYST", noSmoothConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL", noSmoothConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_RECO_TOTAL", noSmoothConfig});

  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025", skipConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530", skipConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040", skipConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40", skipConfig}); 

  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025", skipConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530", skipConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040", skipConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40", skipConfig}); 

  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_RNNID_HIGHPT", noSmoothConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_RNNID_SYST", noSmoothConfig});

  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA161718", noSLTnoSmoothConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA1718", noSLTnoSmoothConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016", noSLTnoSmoothConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018", noSLTnoSmoothConfig}); 
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2018AFTTS1", noSLTnoSmoothConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_TRIGGER_STATMC161718", noSLTnoSmoothConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_TRIGGER_STATMC1718", noSLTnoSmoothConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016", noSLTnoSmoothConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018", noSLTnoSmoothConfig}); 
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2018AFTTS1", noSLTnoSmoothConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_TRIGGER_SYST161718", noSLTnoSmoothConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_TRIGGER_SYST1718", noSLTnoSmoothConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016", noSLTnoSmoothConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018", noSLTnoSmoothConfig}); 
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_TRIGGER_SYST2018AFTTS1", noSLTnoSmoothConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU161718", noSLTnoSmoothConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU1718", noSLTnoSmoothConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2016", noSLTnoSmoothConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018", noSLTnoSmoothConfig}); 
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_EFF_TRIGGER_SYSTMU2018AFTTS1", noSLTnoSmoothConfig});

  //4-vector systs
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_SME_TES_INSITUEXP", smoothConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_SME_TES_INSITUFIT", smoothConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_SME_TES_MODEL_CLOSURE", smoothConfig});
  m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_SME_TES_PHYSICSLIST", smoothConfig});

  //MET
  m_histoSysts.insert({ "SysMET_SoftTrk_Scale" , smoothConfig});
  m_histoSysts.insert({ "SysMET_SoftTrk_ResoPerp" , smoothSymmConfig});//one-sided and symmetrised
  m_histoSysts.insert({ "SysMET_SoftTrk_ResoPara" , smoothSymmConfig});//one-sided and symmetrised 
 
  //Jet
  m_histoSysts.insert({ "SysJET_BJES_Response", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_EffectiveNP_Detector1", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_EffectiveNP_Detector2", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_EffectiveNP_Mixed1", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_EffectiveNP_Mixed2", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_EffectiveNP_Mixed3", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_EffectiveNP_Modelling1", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_EffectiveNP_Modelling2", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_EffectiveNP_Modelling3", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_EffectiveNP_Modelling4", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_EffectiveNP_Statistical1", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_EffectiveNP_Statistical2", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_EffectiveNP_Statistical3", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_EffectiveNP_Statistical4", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_EffectiveNP_Statistical5", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_EffectiveNP_Statistical6", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_EtaIntercalibration_Modelling", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_EtaIntercalibration_NonClosure_2018data", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_EtaIntercalibration_NonClosure_highE", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_EtaIntercalibration_NonClosure_negEta", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_EtaIntercalibration_NonClosure_posEta", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_EtaIntercalibration_TotalStat", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_Flavor_Composition", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_Flavor_Response", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_Pileup_OffsetMu", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_Pileup_OffsetNPV", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_Pileup_PtTerm", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_Pileup_RhoTopology", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_PunchThrough_AFII", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_PunchThrough_MC16", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_RelativeNonClosure_AFII", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_SingleParticle_HighPt", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_RelativeNonClosure_MC16", smoothSymmAverageConfig});//included in ZCR but not in SRs, TO BE DOUBLE CHECKED!!!

  m_histoSysts.insert({ "SysJET_JER_DataVsMC_AFII", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_JER_DataVsMC_MC16", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_JER_EffectiveNP_1", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_JER_EffectiveNP_2", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_JER_EffectiveNP_3", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_JER_EffectiveNP_4", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_JER_EffectiveNP_5", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_JER_EffectiveNP_6", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_JER_EffectiveNP_7", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_JER_EffectiveNP_8", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_JER_EffectiveNP_9", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_JER_EffectiveNP_10", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_JER_EffectiveNP_11", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysJET_JER_EffectiveNP_12restTerm", smoothSymmAverageConfig});

  //skip the PD and MC single components of JER as should NOT be used
  m_histoSysts.insert({ "SysJET_JERPD_DataVsMC_AFII", skipConfig});
  m_histoSysts.insert({ "SysJET_JERPD_DataVsMC_MC16", skipConfig});
  m_histoSysts.insert({ "SysJET_JERPD_EffectiveNP_1", skipConfig});
  m_histoSysts.insert({ "SysJET_JERPD_EffectiveNP_2", skipConfig});
  m_histoSysts.insert({ "SysJET_JERPD_EffectiveNP_3", skipConfig});
  m_histoSysts.insert({ "SysJET_JERPD_EffectiveNP_4", skipConfig});
  m_histoSysts.insert({ "SysJET_JERPD_EffectiveNP_5", skipConfig});
  m_histoSysts.insert({ "SysJET_JERPD_EffectiveNP_6", skipConfig});
  m_histoSysts.insert({ "SysJET_JERPD_EffectiveNP_7", skipConfig});
  m_histoSysts.insert({ "SysJET_JERPD_EffectiveNP_8", skipConfig});
  m_histoSysts.insert({ "SysJET_JERPD_EffectiveNP_9", skipConfig});
  m_histoSysts.insert({ "SysJET_JERPD_EffectiveNP_10", skipConfig});
  m_histoSysts.insert({ "SysJET_JERPD_EffectiveNP_11", skipConfig});
  m_histoSysts.insert({ "SysJET_JERPD_EffectiveNP_12restTerm", skipConfig}); 

  m_histoSysts.insert({ "SysJET_JERMC_DataVsMC_AFII", skipConfig});
  m_histoSysts.insert({ "SysJET_JERMC_DataVsMC_MC16", skipConfig});
  m_histoSysts.insert({ "SysJET_JERMC_EffectiveNP_1", skipConfig});
  m_histoSysts.insert({ "SysJET_JERMC_EffectiveNP_2", skipConfig});
  m_histoSysts.insert({ "SysJET_JERMC_EffectiveNP_3", skipConfig});
  m_histoSysts.insert({ "SysJET_JERMC_EffectiveNP_4", skipConfig});
  m_histoSysts.insert({ "SysJET_JERMC_EffectiveNP_5", skipConfig});
  m_histoSysts.insert({ "SysJET_JERMC_EffectiveNP_6", skipConfig});
  m_histoSysts.insert({ "SysJET_JERMC_EffectiveNP_7", skipConfig});
  m_histoSysts.insert({ "SysJET_JERMC_EffectiveNP_8", skipConfig});
  m_histoSysts.insert({ "SysJET_JERMC_EffectiveNP_9", skipConfig});
  m_histoSysts.insert({ "SysJET_JERMC_EffectiveNP_10", skipConfig});
  m_histoSysts.insert({ "SysJET_JERMC_EffectiveNP_11", skipConfig});
  m_histoSysts.insert({ "SysJET_JERMC_EffectiveNP_12restTerm", skipConfig}); 

  //egamma
  //4-vecotr systs
  m_histoSysts.insert({ "SysEG_RESOLUTION_ALL", smoothConfig});
  m_histoSysts.insert({ "SysEG_SCALE_AF2", smoothConfig});
  m_histoSysts.insert({ "SysEG_SCALE_ALL", smoothConfig});

  //muons
  //4-vector systs
  m_histoSysts.insert({ "SysMUON_ID", smoothConfig});
  m_histoSysts.insert({ "SysMUON_MS", smoothConfig});
  m_histoSysts.insert({ "SysMUON_SAGITTA_RESBIAS", smoothSymmAverageConfig});
  m_histoSysts.insert({ "SysMUON_SAGITTA_RHO", smoothConfig});
  m_histoSysts.insert({ "SysMUON_SCALE", smoothConfig});

  //weights
  m_histoSysts.insert({ "SysMUON_EFF_RECO_STAT", noSmoothConfig});
  m_histoSysts.insert({ "SysMUON_EFF_RECO_SYS", noSmoothConfig});
  m_histoSysts.insert({ "SysMUON_EFF_RECO_STAT_LOWPT", noSmoothConfig});
  m_histoSysts.insert({ "SysMUON_EFF_RECO_SYS_LOWPT", noSmoothConfig});
  m_histoSysts.insert({ "SysMUON_EFF_ISO_STAT", noSmoothConfig});
  m_histoSysts.insert({ "SysMUON_EFF_ISO_SYS", noSmoothConfig});
  m_histoSysts.insert({ "SysMUON_EFF_TTVA_STAT", noSmoothConfig});
  m_histoSysts.insert({ "SysMUON_EFF_TTVA_SYS", noSmoothConfig});
  
  m_histoSysts.insert({ "SysMUON_EFF_TrigStatUncertainty", noSmoothConfig});
  m_histoSysts.insert({ "SysMUON_EFF_TrigSystUncertainty", noSmoothConfig});

  //electrons
  //weights
  m_histoSysts.insert({ "SysEL_EFF_TriggerEff_TOTAL_1NPCOR_PLUS_UNCOR", skipConfig}); // no variation, only in lephad
  m_histoSysts.insert({ "SysEL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR", noSmoothConfig});
  m_histoSysts.insert({ "SysEL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR", noSmoothConfig});
  m_histoSysts.insert({ "SysEL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR", noSmoothConfig});
  m_histoSysts.insert({ "SysEL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR", noSmoothConfig});

  //bbtautau modelling uncertainties

  //ttbar modelling in HadHad SR
  //Calculated by Chris in 10/2020
  //These have to be shape only in combined fit (as norm floating with textrapolation uncertainties) and shape plus norm in bbtautau fit 


  //if(useFltNorms && fltTop && CombDecorrRatios){//LepHad+HadHad

  // if (CategoryIndex::hasMatching(P::spec=="TauLH")){//LepHad or LepHad+HadHad

  //ttbar shapes
    //new ttbar uncertainties from Chris in 04/2021 for hadhad and by Jordan in 06/2021 for lephad, hadhad ones don't need smoothing, lephad ME and PS ones do

    m_histoSysts.insert({"SysTHEO_ACC_TTBAR_ME", ShapeOnlySmoothLepHadOnlySymmConfig}); 
    m_histoSysts.insert({"SysTHEO_ACC_TTBAR_PS_HadHad", ShapeOnlynoSmoothSymmConfig});

    m_histoSysts.insert({"SysTHEO_ACC_TTBAR_PS_SLT", ShapeOnlySmoothSymmConfig});
    m_histoSysts.insert({"SysTHEO_ACC_TTBAR_ISR", ShapeOnlynoSmoothSymmConfig});
    m_histoSysts.insert({"SysTHEO_ACC_TTBAR_FSR", ShapeOnlySmoothSymmAverageConfig});
    

    // } else {//HadHad only

  //  m_histoSysts.insert({ "SysTTBAR_ACC_GEN", noSmoothConfig}); //ME NLO parametrizetion
  //  m_histoSysts.insert({ "SysTTBAR_ACC_PS", noSmoothConfig}); // PS parametrization
  //  m_histoSysts.insert({ "SysTTBAR_ACC_ISR_PARAM", noSmoothConfig}); //ISR parametrization
  //  m_histoSysts.insert({ "SysTTBAR_ACC_FSR", smoothConfig});// FSR using directly weights (no parametrization)

    //  }
   
    //singletop
    m_histoSysts.insert({  "SysTHEO_ACC_StopWt_TopInterference", ShapeOnlynoSmoothConfig});
    m_histoSysts.insert({  "SysTHEO_ACC_StopWt_FSR", ShapeOnlynoSmoothConfig});

  //Z+HF shape in HadHad SR 
    m_histoSysts.insert({ "SysTHEO_ACC_Zhf_GENERATOR", ShapeOnlynoSmoothConfig});
  
  //LepHad and HadHad Z+HF shapes for scales
    m_histoSysts.insert({  "SysTHEO_ACC_Zhf_SCALE", ShapeOnlynoSmoothConfig});

    //Possible ptZ dependence uncertainty
    m_histoSysts.insert({ "SysZJETS_PTV_DEPENDENCY_CR", skipConfig}); // Calculated by Bowen in 06/2021 -> Decide if include it or not

  //Resonant signal modelling
      m_histoSysts.insert({"SysTHEO_ACC_PS_ggFHH_PNN", noSmoothConfig});//hadhad and lephad resonant signal shape uncertainty calculated by Alessandra in 05/2020 and by Nicholas in 06/2021

      //VBF signal scale shape for hadhad
      m_histoSysts.insert({"SysTHEO_ACC_SCALE_VBFSMHH", noSmoothConfig}); //calculated by Chris in 06/2021

  //HadHad multi-jet FFs
  //Calculated by Chris in 01/2021
    m_histoSysts.insert({  "SysFF_OTHER_SUBTRACTION", noSmoothConfig});
    m_histoSysts.insert({  "SysFF_TRUE_TTBAR_SUBTRACTION", noSmoothConfig});
    m_histoSysts.insert({  "SysTTBAR_FAKESF_ANTITAU_DIFF", noSmoothConfig});
    m_histoSysts.insert({  "SysTTBAR_FAKESF_ANTITAU_OFFL_EIGEN0", noSmoothConfig});
    m_histoSysts.insert({  "SysTTBAR_FAKESF_ANTITAU_TAU25_EIGEN0", noSmoothConfig});
    m_histoSysts.insert({  "SysTTBAR_FAKESF_ANTITAU_TAU25EF_EIGEN0", noSmoothConfig});
    m_histoSysts.insert({  "SysTTBAR_FAKESF_ANTITAU_TAU25RNN_EIGEN0", noSmoothConfig});
    m_histoSysts.insert({  "SysFF_OSSS", noSmoothConfig});
    m_histoSysts.insert({  "SysTF_STAT_1P_LEAD", noSmoothConfig});
    m_histoSysts.insert({  "SysTF_STAT_1P_SUBL", noSmoothConfig});
    m_histoSysts.insert({  "SysTF_STAT_3P_LEAD", noSmoothConfig});
    m_histoSysts.insert({  "SysTF_STAT_3P_SUBL", noSmoothConfig});

  //HadHad ttbar fake-rate
  //Calculated by Chris in 10/2020
    //Not used anymore as FRs have been discarded
    /*
  m_histoSysts.insert({  "SysFakeRate_Stat", smoothConfig});
  m_histoSysts.insert({  "SysFakeRate_TTBarNorm", smoothConfig});
  m_histoSysts.insert({  "SysFakeRate_TTBarRW", smoothConfig});
  m_histoSysts.insert({  "SysFakeRate_StopSub", smoothConfig});
  m_histoSysts.insert({  "SysFakeRate_OtherSub", smoothConfig});
  m_histoSysts.insert({  "SysFakeRate_NonClosure", smoothConfig});
    */

  //HadHad ttbar SF
  //Calculated by Chris in 11/2020
  m_histoSysts.insert({   "SysTTBAR_FAKESF_OFFL_EIGEN0", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_OFFL_EIGEN1", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_OFFL_EIGEN2", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_OFFL_EIGEN3", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_OFFL_EIGEN4", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_OFFL_EIGEN5", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_OFFL_EIGEN6", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_OFFL_EIGEN7", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_OFFL_EIGEN8", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_OFFL_EIGEN9", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_OFFL_EIGEN10", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_OFFL_EIGEN11", noSmoothConfig});

  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU25_EIGEN0", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU25_EIGEN1", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU25_EIGEN2", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU25_EIGEN3", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU25_EIGEN4", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU25_EIGEN5", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU25_EIGEN6", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU25_EIGEN7", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU25_EIGEN8", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU25_EIGEN9", noSmoothConfig});

  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU25EF_EIGEN0", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU25EF_EIGEN1", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU25EF_EIGEN2", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU25EF_EIGEN3", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU25EF_EIGEN4", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU25EF_EIGEN5", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU25EF_EIGEN6", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU25EF_EIGEN7", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU25EF_EIGEN8", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU25EF_EIGEN9", noSmoothConfig});

  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU25RNN_EIGEN0", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU25RNN_EIGEN1", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU25RNN_EIGEN2", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU25RNN_EIGEN3", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU25RNN_EIGEN4", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU25RNN_EIGEN5", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU25RNN_EIGEN6", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU25RNN_EIGEN7", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU25RNN_EIGEN8", noSmoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU25RNN_EIGEN9", noSmoothConfig});

  /*//Not used anymore
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU35_EIGEN0", smoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU35_EIGEN1", smoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU35_EIGEN2", smoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU35_EIGEN3", smoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU35_EIGEN4", smoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU35_EIGEN5", smoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU35_EIGEN6", smoothConfig});

  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU35EF_EIGEN0", smoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU35EF_EIGEN1", smoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU35EF_EIGEN2", smoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU35EF_EIGEN3", smoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU35EF_EIGEN4", smoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU35EF_EIGEN5", smoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU35EF_EIGEN6", smoothConfig});

  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU35RNN_EIGEN0", smoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU35RNN_EIGEN1", smoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU35RNN_EIGEN2", smoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU35RNN_EIGEN3", smoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU35RNN_EIGEN4", smoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU35RNN_EIGEN5", smoothConfig});
  m_histoSysts.insert({   "SysTTBAR_FAKESF_TAU35RNN_EIGEN6", smoothConfig});
  */

  m_histoSysts.insert({  "SysTTBAR_FAKESF_TAU25_TAU35_DIFF", noSmoothConfig});
  m_histoSysts.insert({  "SysTTBAR_FAKESF_TAU25EF_TAU35EF_DIFF", noSmoothConfig});
  m_histoSysts.insert({  "SysTTBAR_FAKESF_TAU25RNN_TAU35RNN_DIFF", noSmoothConfig});

  m_histoSysts.insert({  "SysTTBAR_FAKESF_EXTRAPOL_TF_FT_1P", noSmoothConfig});
  m_histoSysts.insert({  "SysTTBAR_FAKESF_EXTRAPOL_TF_FT_3P", noSmoothConfig});
  m_histoSysts.insert({  "SysTTBAR_FAKESF_EXTRAPOL_TF_FT_PS", noSmoothConfig});
  m_histoSysts.insert({  "SysTTBAR_FAKESF_EXTRAPOL_FF", noSmoothConfig});



  //LepHad fakes
  //Calculated by Allison in 10/2020
  m_histoSysts.insert({  "SysFFStatTtbar", noSmoothConfig});
  m_histoSysts.insert({  "SysFFStatQCD", noSmoothConfig});
  //m_histoSysts.insert({  "SysSubtraction_ttbar", smoothConfig});
  m_histoSysts.insert({  "SysSubtraction_bkg", noSmoothConfig});
  m_histoSysts.insert({  "SysttReweighting", noSmoothSymmConfig});
  m_histoSysts.insert({  "SysFFStatrQCD", noSmoothConfig});
  m_histoSysts.insert({  "SysFFVarrQCD", noSmoothConfig});

  //Set below everything present in the inputs that should be SKIPPED

  //skip them all as not final

  m_histoSysts.insert({ "SysFF_STAT_1BTAG_DTT_1P_INCL_BIN1", skipConfig});
  m_histoSysts.insert({ "SysFF_STAT_1BTAG_DTT_3P_INCL_BIN1", skipConfig});
  m_histoSysts.insert({ "SysFF_STAT_1BTAG_DTT_1P_INCL_BIN2", skipConfig});
  m_histoSysts.insert({ "SysFF_STAT_1BTAG_DTT_3P_INCL_BIN2", skipConfig});
  m_histoSysts.insert({ "SysFF_STAT_1BTAG_DTT_1P_INCL_BIN3", skipConfig});
  m_histoSysts.insert({ "SysFF_STAT_1BTAG_DTT_3P_INCL_BIN3", skipConfig});
  m_histoSysts.insert({ "SysFF_STAT_1BTAG_DTT_1P_INCL_BIN4", skipConfig});
  m_histoSysts.insert({ "SysFF_STAT_1BTAG_DTT_3P_INCL_BIN4", skipConfig});
  m_histoSysts.insert({ "SysFF_STAT_1BTAG_DTT_1P_INCL_BIN5", skipConfig});
  m_histoSysts.insert({ "SysFF_STAT_1BTAG_DTT_1P_INCL_BIN6", skipConfig});
  m_histoSysts.insert({ "SysFF_STAT_1BTAG_DTT_1P_INCL_BIN7", skipConfig});

  m_histoSysts.insert({ "SysFF_STAT_1BTAG_STT_1P_LEAD_BIN1", skipConfig});
  m_histoSysts.insert({ "SysFF_STAT_1BTAG_STT_3P_LEAD_BIN1", skipConfig});
  m_histoSysts.insert({ "SysFF_STAT_1BTAG_STT_1P_SUBL_BIN1", skipConfig});
  m_histoSysts.insert({ "SysFF_STAT_1BTAG_STT_3P_SUBL_BIN1", skipConfig});


  //HadHad ttbar shapes
  //skip them all as parameterisations not final (and single variations should not be included)
  m_histoSysts.insert({ "SysTTBAR_ALPHAS", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_GEN", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_PS", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_ISR_PARAM", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_MUF", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_ACC_MUF", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_MUR", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_ACC_MUR", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_VAR3C", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_ACC_VAR3C" , skipConfig});
  m_histoSysts.insert({ "SysTTBAR_FSR", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_FSR_OUTLIERREMOVAL", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_PDF4LHC_0", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_PDF4LHC_1", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_PDF4LHC_2", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_PDF4LHC_3", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_PDF4LHC_4", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_PDF4LHC_5", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_PDF4LHC_6", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_PDF4LHC_7", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_PDF4LHC_8", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_PDF4LHC_9", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_PDF4LHC_10", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_PDF4LHC_11", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_PDF4LHC_12", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_PDF4LHC_13", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_PDF4LHC_14", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_PDF4LHC_15", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_PDF4LHC_16", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_PDF4LHC_17", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_PDF4LHC_18", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_PDF4LHC_19", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_PDF4LHC_20", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_PDF4LHC_21", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_PDF4LHC_22", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_PDF4LHC_23", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_PDF4LHC_24", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_PDF4LHC_25", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_PDF4LHC_26", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_PDF4LHC_27", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_PDF4LHC_28", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_PDF4LHC_29", skipConfig});
  m_histoSysts.insert({ "SysTTBAR_PDF4LHC_30", skipConfig}); 

  //HadHad Z+HF shapes
  //skip them all as shapes neglected in HadHad and individual variations should not be included
  m_histoSysts.insert({ "SysZJETS_MUR_MUF", skipConfig});
  m_histoSysts.insert({ "SysZJETS_MUR", skipConfig});
  m_histoSysts.insert({ "SysZJETS_MUF", skipConfig});
  m_histoSysts.insert({ "SysZJETS_AS", skipConfig});
  m_histoSysts.insert({ "SysZJETS_CKKW", skipConfig});
  m_histoSysts.insert({ "SysZJETS_QSF", skipConfig});
  m_histoSysts.insert({ "SysZJETS_CT14", skipConfig});
  m_histoSysts.insert({ "SysZJETS_MMHT2014", skipConfig});

  m_histoSysts.insert({ "SysZJETS_NNPDF261001", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261002", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261003", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261004", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261005", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261006", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261007", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261008", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261009", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261010", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261011", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261012", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261013", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261014", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261015", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261016", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261017", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261018", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261019", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261020", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261021", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261022", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261023", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261024", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261025", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261026", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261027", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261028", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261029", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261030", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261031", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261032", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261033", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261034", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261035", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261036", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261037", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261038", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261039", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261040", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261041", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261042", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261043", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261044", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261045", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261046", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261047", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261048", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261049", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261050", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261051", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261052", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261053", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261054", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261055", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261056", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261057", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261058", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261059", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261060", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261061", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261062", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261063", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261064", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261065", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261066", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261067", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261068", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261069", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261070", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261071", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261072", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261073", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261074", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261075", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261076", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261077", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261078", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261079", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261080", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261081", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261082", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261083", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261084", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261085", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261086", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261087", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261088", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261089", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261090", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261091", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261092", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261093", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261094", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261095", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261096", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261097", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261098", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261099", skipConfig});
  m_histoSysts.insert({ "SysZJETS_NNPDF261100", skipConfig});
 

  //ALL THIS BELOW HAS TO BE UPDATED
  /*

  //Old but useful to see how to skip only in one region
  //SysConfig noSLTConfig = { T::shape, S::smoothRebinMonotonic, SysConfig::Symmetrise::noSym, 
  //			    std::vector<TString>(), {{P::spec,"TauHH"}, {P::LTT, 1}}};
  //m_histoSysts.insert({ "SysTAUS_TRUEHADTAU_SME_TES_MODEL", noSLTConfig}); 
  //Old but useful to see how to use only in one region
  //SysConfig hadhadConfig = { T::shape, S::smoothRebinMonotonic, SysConfig::Symmetrise::noSym, std::vector<TString>(), {{P::spec,"TauHH"}}};
  // m_histoSysts.insert({ "SysANTITAU_BDT_CUT",  hadhadConfig});  // Replaced by CompFakes for lephad
   //m_histoSysts.insert({ "SysANTITAU_BDT_CUT",  smoothConfig});//hadhad still using it
  // m_histoSysts.insert({ "SysANTITAU_BDT_CUT",  skipConfig});// Replaced by CompFakes in both channels 


  //old but useful to see how to apply only on signal or bkg
  // m_histoSysts.insert({ "SysJET_JER_SINGLE_NP" , smoothSymmConfig});
//  m_histoSysts.insert({ "SysJET_JER_SINGLE_NP" , SysConfig{T::shape, S::noSmooth, false, {"Bkg"}}});
//  m_histoSysts.insert({ "SysJET_JER_SINGLE_NP" , SysConfig{T::skip, S::smooth, {"Sig"}}});



  m_histoSysts.insert({ "SysTTBAR_NNLO", noSmoothSymmConfig});

  // LepHad+HadHad FF: QCD decorrelated
  SysConfig decorrConfig = { T::shape, S::smoothRebinMonotonic, SysConfig::Symmetrise::noSym, std::vector<TString>(), {{}}, {{P::spec}}};
  m_histoSysts.insert({ "SysFFStatQCD", decorrConfig});
  m_histoSysts.insert({ "SysSubtraction_bkg", decorrConfig});
   m_histoSysts.insert({ "SysCompFakes",  decorrConfig});

  // LepHad FF
   m_histoSysts.insert({ "SysSS", smoothSymmConfig});
  m_histoSysts.insert({ "SysFFStatTtbar", smoothConfig});
  m_histoSysts.insert({ "SysFF_MTW", smoothSymmConfig});
  m_histoSysts.insert({ "SysCPVarFakes", smoothConfig});
  m_histoSysts.insert({ "SysTTbarGenFakes", smoothConfig});

  // Data-driven rQCD -> not currently used
  m_histoSysts.insert({ "SysFFStatrQCD", smoothConfig});
  m_histoSysts.insert({ "SysffMC40", skipConfig}); // Too large
  m_histoSysts.insert({ "SysMC40", smoothConfig});
  m_histoSysts.insert({ "Sysff", smoothConfig});
  m_histoSysts.insert({ "SysMT40GeV", smoothConfig});


  // HadHad FF -> old
  m_histoSysts.insert({ "SysSubtractbkg_FFQCD", smoothConfig});
  m_histoSysts.insert({ "SysFFQCD_0to2tag", smoothConfig});

  //HadHad QCD FF
  m_histoSysts.insert({ "Sys1tag2tagTF", smoothConfig});
  m_histoSysts.insert({ "SysOSSS", smoothConfig});

  //HadHad top FR
   m_histoSysts.insert({ "SysFR_MTW_CUT", smoothSymmConfig});
   m_histoSysts.insert({ "SysFR_Stat", smoothConfig});
   m_histoSysts.insert({ "SysFR_STTfraction", smoothSymmConfig});
   m_histoSysts.insert({ "SysFR_CPVar", ShapeOnlySmoothConfig});  // Correlated with lephad?
   m_histoSysts.insert({ "SysFR_ttbarGen", skipConfig}); // Correlated with lephad?

   SysConfig decorrNoSmoothConfig = { T::shape, S::smoothRebinMonotonic, 
				      SysConfig::Symmetrise::noSym, std::vector<TString>(), {{}}, {{P::spec}}};

  m_histoSysts.insert({ "SysTTbarMBB", noSmoothConfig});
  m_histoSysts.insert({ "SysTTbarPTH", noSmoothConfig});
  m_histoSysts.insert({ "SysZtautauMBB", noSmoothConfig});
  m_histoSysts.insert({ "SysZtautauPTH", noSmoothConfig});

  // TwoLepton CR
  m_histoSysts.insert({ "SysStopWtMBB", noSmoothConfig});
  m_histoSysts.insert({ "SysStopWtPTV", noSmoothConfig});
  m_histoSysts.insert({ "SysStoptMBB", noSmoothConfig});
  m_histoSysts.insert({ "SysStoptPTV", noSmoothConfig});
  m_histoSysts.insert({ "SysTTbarPTV", noSmoothConfig});
  //m_histoSysts.insert({ "SysTTbarMBB", noSmoothConfig});
  m_histoSysts.insert({ "SysMETTrigStat", skipConfig});
  m_histoSysts.insert({ "SysMETTrigTop", skipConfig});
  */

  // No Z modelling?

  /////////////////////////////////////////////////////////////////////////////////////////
  //
  //                          Tweaks, if needed
  //
  /////////////////////////////////////////////////////////////////////////////////////////

  // later can add some switches, e.g looping through m_histom_histoSysts.emplace( "Systs and
  // putting to T::skip all the JES NP



}

SysConfig::Smooth Smoothing_HH::SmoothLepHadOnly(const PropertiesSet& pset, const Sample& s)
{
  auto spec_variant = pset.properties.at(Property::spec);
  TString spec_here = mpark::get<TString>(spec_variant);

  if (spec_here == "TauLH") {
    return SysConfig::Smooth::smoothRebinMonotonic;
  } 
  
  return SysConfig::Smooth::noSmooth;
}
