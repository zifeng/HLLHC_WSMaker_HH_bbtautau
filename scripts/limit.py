import sys
import ROOT as R
from array import array

#chan = "lephad"
chan = "hadhad"
#chan = "tautau"

if chan == "lephad":
    #inver = "HH031016b"
    #inver = "HH141016"
    inver = "HH171116"                
elif chan == "hadhad":
    #inver = "HH170916"
    #inver = "HH171016hh" 
    #inver = "HH290517hhNoZCR"
    inver = "HH111017"
elif chan == "tautau":
    #inver = "HH041016comb"
    #inver = "HH171016comb" 
    inver = "HHComb020817"
    inver = "HHComb030817"       

exp = "obs"

chanmap = {"lephad" : 0.46, "hadhad" : 0.42, "tautau" : 0.46 + 0.42}

flambda = R.TF1("LHLambda", "pol2", -20, 20)
flambda.SetParameter(0,  0.0305647)
flambda.SetParameter(1,  -0.020403)
flambda.SetParameter(2,  0.00425099)

flambdaNLO = R.TF1("LHLambdaNLO", "pol2", -20, 20)
flambdaNLO.SetParameter(0,  0.0711561)
flambdaNLO.SetParameter(1,  -0.0476268)
flambdaNLO.SetParameter(2,  0.00991164)

def xsect_bbtautau_2hdm(chan):
    return xsect(False, 0, False, chan)

def xsect(scale=False, G=1, non_res=False, chan="lephad", hdecay=False, stat=False, scale_lumi = 1, lamb = None, reweight = True, ZZ = False, lambdaNLO = False):

    if chan == "lephad" or chan == "tautau":
        if ZZ:
            xsectstr ="361096     16.445          0.91       1.4307E-01      ZZ     Sherpa_CT10_ZqqZll_SHv21_improved"
            xs = float(xsectstr.split()[1]) * float(xsectstr.split()[2])
            if not stat:
                # Remove extra factor put in to help fit converge
                xs *= 1000

            return xs
                
        if non_res:
            if lamb is not None:
                xsectstr ="""
                345567   3.0565E-02	    1.0      0.02170188              hhttbb		   MGPy8EG_A14NNPDF23LO_hh_bbtt_lh_lambda00
                345568   1.4412E-02	    1.0      0.02238378              hhttbb		   MGPy8EG_A14NNPDF23LO_hh_bbtt_lh_lambda01
                345569   1.3229   	    1.0      0.02020361              hhttbb		   MGPy8EG_A14NNPDF23LO_hh_bbtt_lh_lambda20
                """

                if reweight and lambdaNLO:
                    # Quadratic fit to the NLO XS for lambda = 0,1,2,10
                    xs = flambdaNLO.Eval(lamb)
                elif reweight:
                    # Quadratic fit to the LO XS for lambda = 0,1,2,10
                    xs = flambda.Eval(lamb)
                else:
                    for line in xsectstr.split('\n'):
                        tok = line.split()
                        if not tok: continue 
                        l = int(tok[-1].split("_")[-1].replace("lambda", ""))
                        if (l != lamb): continue
                        xs = float(tok[1])

                if not stat:
                    # Remove extra factor put in to help fit converge
                    xs *= 1000

                if hdecay:
                    xs *= 1000 * 0.5825 *0.06272 * 2

                return xs                

            else:
                xsectstr ="""
                345836   0.00453679     1.0           1.4460E-01          hhttbb        aMcAtNloHerwig7EvtGen_UEEE5_CTEQ6L1_CT10ME_hh_ttbb_lh
                """

                xs = float(xsectstr.split()[1]) * 0.5/(0.5824*0.06272*2) # correct for 50% b, 50% tau production and put in correct BRs

            if not stat:
                # Remove extra factor put in to help fit converge
                xs *= 1000

            if hdecay:
                xs *= 1000 * 0.5824 *0.06272 * 2
                    
            return xs

        #  XS below are for generating hh -> allall

        if G == 2:
            xsectstr ="""
            308452   8.7134             1.0            0.022998       Ghhbbtautau260c20        MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_lh_c20_M260
            303417   9.9983	        1.0            0.022079      Ghhbbtautau300c20         MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_lh_c20_M300
            308453   8.5579	   	1.0            0.023215       Ghhbbtautau400c20        MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_lh_c20_M400
            308454   3.7496	        1.0            0.024922       Ghhbbtautau500c20        MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_lh_c20_M500
            308455   1.6569	        1.0            0.026267       Ghhbbtautau600c20        MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_lh_c20_M600
            303421   0.78979	        1.0            0.027230      Ghhbbtautau700c20         MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_lh_c20_M700
            308456   0.40415	        1.0            0.027822       Ghhbbtautau800c20        MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_lh_c20_M800
            308457   0.21937            1.0            0.028450       Ghhbbtautau900c20        MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_lh_c20_M900
            308458   0.12499            1.0            0.029059       Ghhbbtautau1000c20       MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_lh_c20_M1000
            308272    8.9536	        1.0	       0.022563      Ghhbbtautau275c20         MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_lh_c20_M275
            308273    10.886	        1.0            0.022092      Ghhbbtautau325c20         MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_lh_c20_M325
            308274    11.085            1.0            0.022253      Ghhbbtautau350c20         MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_lh_c20_M350
            308289    5.7364            1.0            0.024092      Ghhbbtautau450c20         MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_lh_c20_M450
            308290    2.4708	        1.0            0.025630      Ghhbbtautau550c20         MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_lh_c20_M550
            """
        elif G == 1:
            xsectstr ="""
            303395   0.2719             1.0            4.7893E-02      Ghhbbtautau260c10        MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_lh_c10_M260
            303396   1.3199             1.0            4.7993E-02      Ghhbbtautau300c10        MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_lh_c10_M300
            303397   1.901              1.0            5.3828E-02      Ghhbbtautau400c10        MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_lh_c10_M400
            303398   0.8924             1.0            5.8507E-02      Ghhbbtautau500c10        MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_lh_c10_M500
            303399   0.4104             1.0            6.1981E-02      Ghhbbtautau600c10        MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_lh_c10_M600
            303400   0.20148            1.0            6.4597E-02      Ghhbbtautau700c10        MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_lh_c10_M700
            303401   0.10549            1.0            6.6660E-02      Ghhbbtautau800c10        MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_lh_c10_M800
            303402   0.05835            1.0            6.8206E-02      Ghhbbtautau900c10        MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_lh_c10_M900
            303403   0.03368            1.0            6.9496E-02      Ghhbbtautau1000c10       MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_lh_c10_M1000
            X        0.590              1.0	       0.0193752       Ghhbbtautau275c10        MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_lh_c10_M275
            X        1.92               1.0            0.0204102       Ghhbbtautau325c10        MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_lh_c10_M325
            X        2.24               1.0            0.0210726       Ghhbbtautau350c10        MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_lh_c10_M350
            X        1.33               1.0            0.0234738       Ghhbbtautau450c10        MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_lh_c10_M450
            X        0.601              1.0            0.0249228       Ghhbbtautau550c10        MGPy8EG_A14NNPDF23LO_RS_G_hh_bbtt_lh_c10_M550
            """
        else:
            xsectstr ="""            
            450131   0.146112512    1.0           0.11284             Hhhbbtautau251    MadGraphHerwig7EvtGen_PDF23LO_X251tohh_bbtautau_lephad
            450132   0.146112512    1.0           0.1137              Hhhbbtautau260    MadGraphHerwig7EvtGen_PDF23LO_X260tohh_bbtautau_lephad
            450133   0.146112512    1.0           0.11601             Hhhbbtautau280    MadGraphHerwig7EvtGen_PDF23LO_X280tohh_bbtautau_lephad
            450134   0.146112512    1.0           0.11911             Hhhbbtautau300    MadGraphHerwig7EvtGen_PDF23LO_X300tohh_bbtautau_lephad
            450135   0.146112512    1.0           0.12326             Hhhbbtautau325    MadGraphHerwig7EvtGen_PDF23LO_X325tohh_bbtautau_lephad
            450136   0.146112512    1.0           0.12792             Hhhbbtautau350    MadGraphHerwig7EvtGen_PDF23LO_X350tohh_bbtautau_lephad
            450137   0.146112512    1.0           0.13572             Hhhbbtautau400    MadGraphHerwig7EvtGen_PDF23LO_X400tohh_bbtautau_lephad
            450138   0.146112512    1.0           0.14324             Hhhbbtautau450    MadGraphHerwig7EvtGen_PDF23LO_X450tohh_bbtautau_lephad
            450139   0.146112512    1.0           0.14959             Hhhbbtautau500    MadGraphHerwig7EvtGen_PDF23LO_X500tohh_bbtautau_lephad
            450140   0.146112512    1.0           0.1548              Hhhbbtautau550    MadGraphHerwig7EvtGen_PDF23LO_X550tohh_bbtautau_lephad
            450141   0.146112512    1.0           0.16022             Hhhbbtautau600    MadGraphHerwig7EvtGen_PDF23LO_X600tohh_bbtautau_lephad
            450142   0.146112512    1.0           0.1684              Hhhbbtautau700    MadGraphHerwig7EvtGen_PDF23LO_X700tohh_bbtautau_lephad
            450143   0.146112512    1.0           0.17447             Hhhbbtautau800    MadGraphHerwig7EvtGen_PDF23LO_X800tohh_bbtautau_lephad
            450144   0.146112512    1.0           0.18006             Hhhbbtautau900    MadGraphHerwig7EvtGen_PDF23LO_X900tohh_bbtautau_lephad
            450145   0.146112512    1.0           0.18441             Hhhbbtautau1000   MadGraphHerwig7EvtGen_PDF23LO_X1000tohh_bbtautau_lephad
            """
    elif chan == "hadhad":
        if ZZ:
            xsectstr ="361096     16.445          0.91       1.4307E-01      ZZ     Sherpa_CT10_ZqqZll_SHv21_improved"
            xs = float(xsectstr.split()[1]) * float(xsectstr.split()[2])
            if not stat:
                # Remove extra factor put in to help fit converge
                xs *= 1000

            return xs      

        if non_res:
            if lamb is not None:
                xsectstr ="""
                TODO
                """

                if reweight:
                    # Quadratic fit to the XS for lambda = 0,1,2,10
                    xs = flambda.Eval(lamb)

                else:
                    for line in xsectstr.split('\n'):
                        tok = line.split()
                        if not tok: continue 
                        l = int(tok[-1].split("_")[-1].replace("lambda", ""))
                        if (l != lamb): continue
                        xs = float(tok[1])

                if not stat:
                    # Remove extra factor put in to help fit converge
                    xs *= 1000

                if hdecay:
                    xs *= 1000 * 0.5825 *0.06272 * 2

                return xs 

            else:
                xsectstr ="""
                345835   0.00453679     1.0           1.5629E-01          hhttbb        aMcAtNloHerwig7EvtGen_UEEE5_CTEQ6L1_CT10ME_hh_ttbb_hh
                """
                
                xs = float(xsectstr.split()[1]) * 0.5/(0.5824*0.06272*2) # correct for 50% b, 50% tau production and put in correct BRs
                
                if not stat:
                    # Remove extra factor put in to help fit converge
                    xs *= 1000

                if hdecay:
                    xs *= 1000 * 0.5824 *0.06272 * 2
                    
            return xs

        #  XS below are for generating hh -> allall

        if G == 2:
            xsectstr ="""
            308461         8.7134        1.0       0.025405        RS_G_hh_bbtt_hh_c20_M260        MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbtt_hh_c20_M260
            308314         8.9354        1.0       0.025146        RS_G_hh_bbtt_hh_c20_M275        MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbtt_hh_c20_M275
            303373         10.0          1.0       0.02491        RS_G_hh_bbtt_hh_c20_M300        MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbtt_hh_c20_M300
            308315         10.886        1.0       0.024752        RS_G_hh_bbtt_hh_c20_M325        MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbtt_hh_c20_M325
            308316         11.085        1.0       0.025022        RS_G_hh_bbtt_hh_c20_M350        MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbtt_hh_c20_M350
            308462         8.5579        1.0       0.025502        RS_G_hh_bbtt_hh_c20_M400        MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbtt_hh_c20_M400
            308317         5.7364        1.0       0.02597643       RS_G_hh_bbtt_hh_c20_M450        MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbtt_hh_c20_M450
            308463         3.7498        1.0       0.0264211       RS_G_hh_bbtt_hh_c20_M500        MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbtt_hh_c20_M500
            308318         2.4708        1.0       0.0267018       RS_G_hh_bbtt_hh_c20_M550        MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbtt_hh_c20_M550
            308464         1.6569        1.0       0.0271009       RS_G_hh_bbtt_hh_c20_M600        MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbtt_hh_c20_M600
            303377         7.90E-01      1.0       0.02759        RS_G_hh_bbtt_hh_c20_M700        MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbtt_hh_c20_M700
            308465         0.40414       1.0       0.0280506       RS_G_hh_bbtt_hh_c20_M800        MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbtt_hh_c20_M800
            308466         0.21936       1.0       0.0284128       RS_G_hh_bbtt_hh_c20_M900        MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbtt_hh_c20_M900
            308467         0.12498       1.0       0.0287288       RS_G_hh_bbtt_hh_c20_M1000       MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbtt_hh_c20_M1000       
            """
        elif G == 1:
            xsectstr ="""
            303351         2.72E-01      1.0       0.025416        RS_G_hh_bbtt_hh_c10_M260        MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbtt_hh_c10_M260
            303351         5.9E-01       1.0       0.023805        RS_G_hh_bbtt_hh_c10_M275        MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbtt_hh_c10_M275
            303352         1.32          1.0       0.026731        RS_G_hh_bbtt_hh_c10_M300        MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbtt_hh_c10_M300
            303352         1.92         1.0       0.024219        RS_G_hh_bbtt_hh_c10_M325        MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbtt_hh_c10_M325
            303352         2.24           1.0       0.0242604        RS_G_hh_bbtt_hh_c10_M350        MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbtt_hh_c10_M350
            303353         1.91          1.0       0.027195        RS_G_hh_bbtt_hh_c10_M400        MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbtt_hh_c10_M400
            303352         1.33          1.0       0.0256260        RS_G_hh_bbtt_hh_c10_M450        MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbtt_hh_c10_M450
            303354         8.90E-01      1.0       0.025802        RS_G_hh_bbtt_hh_c10_M500        MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbtt_hh_c10_M500
            303352         6.01E-01      1.0       0.0263718        RS_G_hh_bbtt_hh_c10_M550        MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbtt_hh_c10_M550
            303355         4.10E-01      1.0       0.025583        RS_G_hh_bbtt_hh_c10_M600        MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbtt_hh_c10_M600
            303356         2.01E-01      1.0       0.026132        RS_G_hh_bbtt_hh_c10_M700        MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbtt_hh_c10_M700
            303357         1.05E-01      1.0       0.025095        RS_G_hh_bbtt_hh_c10_M800        MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbtt_hh_c10_M800
            303358         5.80E-02      1.0       0.026149        RS_G_hh_bbtt_hh_c10_M900        MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbtt_hh_c10_M900
            303359         3.40E-02      1.0       0.026197        RS_G_hh_bbtt_hh_c10_M1000       MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbtt_hh_c10_M1000
            """            
        else:
            xsectstr ="""
            450146   0.146112512    1.0           0.11838             Hhhbbtautau251    MadGraphHerwig7EvtGen_PDF23LO_X251tohh_bbtautau_hadhad
            450147   0.146112512    1.0           0.12103             Hhhbbtautau260    MadGraphHerwig7EvtGen_PDF23LO_X260tohh_bbtautau_hadhad
            450148   0.146112512    1.0           0.12591             Hhhbbtautau280    MadGraphHerwig7EvtGen_PDF23LO_X280tohh_bbtautau_hadhad
            450149   0.146112512    1.0           0.13065             Hhhbbtautau300    MadGraphHerwig7EvtGen_PDF23LO_X300tohh_bbtautau_hadhad
            450150   0.146112512    1.0           0.13634             Hhhbbtautau325    MadGraphHerwig7EvtGen_PDF23LO_X325tohh_bbtautau_hadhad
            450151   0.146112512    1.0           0.14106             Hhhbbtautau350    MadGraphHerwig7EvtGen_PDF23LO_X350tohh_bbtautau_hadhad
            450152   0.146112512    1.0           0.149               Hhhbbtautau400    MadGraphHerwig7EvtGen_PDF23LO_X400tohh_bbtautau_hadhad
            450153   0.146112512    1.0           0.15542             Hhhbbtautau450    MadGraphHerwig7EvtGen_PDF23LO_X450tohh_bbtautau_hadhad
            450154   0.146112512    1.0           0.16039             Hhhbbtautau500    MadGraphHerwig7EvtGen_PDF23LO_X500tohh_bbtautau_hadhad
            450155   0.146112512    1.0           0.16486             Hhhbbtautau550    MadGraphHerwig7EvtGen_PDF23LO_X550tohh_bbtautau_hadhad
            450156   0.146112512    1.0           0.16783             Hhhbbtautau600    MadGraphHerwig7EvtGen_PDF23LO_X600tohh_bbtautau_hadhad
            450157   0.146112512    1.0           0.17361             Hhhbbtautau700    MadGraphHerwig7EvtGen_PDF23LO_X700tohh_bbtautau_hadhad
            450158   0.146112512    1.0           0.17806             Hhhbbtautau800    MadGraphHerwig7EvtGen_PDF23LO_X800tohh_bbtautau_hadhad
            450159   0.146112512    1.0           0.18133             Hhhbbtautau900    MadGraphHerwig7EvtGen_PDF23LO_X900tohh_bbtautau_hadhad
            450160   0.146112512    1.0           0.18473             Hhhbbtautau1000   MadGraphHerwig7EvtGen_PDF23LO_X1000tohh_bbtautau_hadhad

            """
            
    else:
        sys.exit("Unknown channel %s" % chan)

    BR = 1 #* 0.15/0.458  #0.577 * 0.0632 * 2 * 0.458

    scaling = {}
    if scale:
        f = open("scripts/HHbbtautau/xsratios8_13.txt")
        for l in f:
            tok = l.split()
            m = float(tok[0])
            r = float(tok[1])
            scaling[m] = r

    result = {}
    for l in xsectstr.split('\n'):
        tok = l.split()
        if not tok: continue        
        if G:
            m = int(tok[-1].split("_")[-1].replace("M", ""))
        else:
            m = int(tok[-1].split("_")[2].replace("X", "").replace("tohh", ""))
        #xs = float(tok[1]) * BR
        xs = float(tok[1]) * 0.5/(0.5824*0.06272*2)

        # Remove extra factor put in to help fit converge
        if G and not stat and m in [260]: xs *= 1000
        
        if scale:
            xs /= scaling[m]
        if hdecay:
            xs *= 0.5824 *0.06272 * 2
        if scale_lumi != 1:
            xs *= scale_lumi**0.5

        result[m] = xs
    return result

def run1(hdecay=True):

    m = array('f', [260, 300, 350, 400, 500, 600, 700, 800, 900, 1000])
    #e = array('f', [2.03, 2.57, 1.73, 0.87, 0.48, 0.30, 0.27])    # no systs from Keita
    lim = array('f', [2.6, 3.1, 2.2, 0.97, 0.66, 0.48, 0.31, 0.31, 0.30, 0.28])  
     
    if hdecay:
        e = array('f', [])
        for l in lim:
            e.append(l * 0.577 * 0.0632 * 2)
    else:
        e = lim

    scaling = {}
    f = open("scripts/HHbbtautau/xsratios8_13.txt")
    for l in f:
        tok = l.split()
        mass = float(tok[0])
        r = float(tok[1])
        scaling[mass] = r

    escale = array('f', [])
    for mass, lim in zip(m, e):
        escale.append(lim * scaling[mass])

    return m, e, escale


def bbyy():
    m = array('f', [275, 300, 350, 400, 500]) # H -> hh in pb
    e = array('f', [7.5, 6.9, 5.9, 4.8, 3.0])

    es = array('f', [l * (3.2/13.2)**0.5 for l in e])
    return m, es

def bbbb():
    Br4b   = 0.33293

    # 13/fb 
    l4b = [
        (300, 2.0/Br4b),  
        (400, 0.550/Br4b), 
        (500, 0.085/Br4b), 
        (600, 0.045/Br4b), 
        (700, 0.030/Br4b), 
        (800, 0.020/Br4b), 
        (900, 0.019/Br4b),
        (1000,0.015/Br4b),
#         (1500,0.0055/Br4b),
#         (2000,0.0035/Br4b),
#         (2500,0.0030/Br4b),
#         (3000,0.0030/Br4b),
        ]

    m = array('f', [mass for (mass,_) in l4b])
    e = array('f', [e for (_,e) in l4b])

    return m, e

def cms(chan = "tautau"):
    # Expected limits on H->hh->bbtautau
    xs = {}

    if chan == "tautau" or "lephad":
        xs[250] = 1.5  
        xs[260] = 1.5  
        xs[270] = 1.8    
        xs[280] = 2.1  
        xs[300] = 2.5  
        xs[320] = 1.6  
        xs[340] = 1.1  
        xs[350] = 0.44 
        xs[400] = 0.23   
        xs[450] = 0.18 
        xs[500] = 0.13 
        xs[550] = 0.075
        xs[600] = 0.043
        xs[650] = 0.040
        xs[700] = 0.027
        xs[800] = 0.022
        xs[900] = 0.016
#     elif chan == "lephad":
#         xs[250] = 2.1    +     2.4  
#         xs[260] = 2.3    +     2.6  
#         xs[270] = 2.9    +     2.7  
#         xs[280] = 2.9    +     3.5  
#         xs[300] = 4.3    +     4.2  
#         xs[320] = 3.2    +     3.8  
#         xs[340] = 2.6    +     2.9  
#         xs[350] = 0.97   +     1.6  
#         xs[400] = 0.50   +     0.80 
#         xs[450] = 0.36   +     0.53 
#         xs[500] = 0.20   +     0.38 
#         xs[550] = 0.092  +     0.31 
#         xs[600] = 0.051  +     0.16 
#         xs[650] = 0.060  +     0.11 
#         xs[700] = 0.050  +     0.076
#         xs[800] = 0.040  +     0.061
#         xs[900] = 0.031  +     0.053
    elif chan == "hadhad":
        xs[250] = 3.9
        xs[260] = 2.9
        xs[270] = 3.7
        xs[280] = 4.5
        xs[300] = 3.6
        xs[320] = 1.8
        xs[340] = 1.4
        xs[400] = 0.45 
        xs[450] = 0.24 
        xs[500] = 0.20 
        xs[550] = 0.17 
        xs[600] = 0.17 
        xs[650] = 0.13 
        xs[700] = 0.068
        xs[750] = 0.040
        xs[800] = 0.035
        xs[900] = 0.024        
        

    e = array('f', [])
    #es = array('f', [])    
    m = array('f', [])    

    for k in sorted(xs.keys()):
        m.append(k)
        e.append(xs[k])
#         es.append(xs[k] * (12.9/13.2)**0.5)        

    
    return m, e #, es


def singlet(chan, hdecay = False):
    # From YR4 (arXiv:1610.07922v1), Table 66 

    m = array('f', [260, 275, 300, 325, 350, 400, 450, 500, 600, 700, 750, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500, 1600, 1800, 2000, 2250, 2500, 2750, 3000])
    xsect = [240.06, 268.80, 260.78, 248.87, 262.72, 242.67, 183.37, 137.85, 86.49, 63.49, 57.05, 52.49, 46.86, 43.81, 42.02, 40.91, 40.18, 39.70, 39.34, 39.08, 38.70, 38.44, 38.16, 37.88, 37.57, 37.22]
    
    xs = array('f', [])
    for x in xsect:        
        x /= 1000

        if hdecay:
            #x *= 0.577 * 0.0632 * 2
            x *= 0.5825 * 0.06272 * 2

        #x *= chanmap[chan]
        xs.append(x)        

    return m, xs

def hMSSM(chan, hdecay = False, full = False):

    if full:
        m = array('f', [255.18899536132812, 263.406005859375, 271.88800048828125, 280.5769958496094, 289.4309997558594, 298.42001342773443, 307.5190124511719, 316.71099853515625, 325.98199462890625, 335.3190002441406, 344.71398925781256, 354.15899658203125, 363.64801025390625, 373.17498779296875, 382.73699951171875, 392.3290100097656, 401.947998046875, 411.59100341796875, 421.25698852539057, 430.9419860839844, 440.64599609375006, 450.3659973144532, 460.10198974609375, 469.8510131835937, 479.6130065917969, 489.3869934082031, 499.17199707031244, 508.9670104980469, 518.77099609375, 528.583984375, 538.405029296875, 548.2340087890625, 558.0700073242186, 567.9119873046875, 577.760986328125, 587.6160278320311, 597.4760131835939, 607.3419799804688, 617.2119750976562, 627.0869750976562, 636.9660034179688, 646.8499755859375, 656.7369995117188, 666.6290283203125, 676.5230102539062, 686.4219970703124, 696.3229980468751, 706.2269897460938, 716.135009765625, 726.0449829101564, 735.9580078125, 745.8729858398438, 755.791015625, 765.7109985351562, 775.6329956054688, 785.5579833984375, 795.4840087890625, 805.4130249023438, 815.3430175781249, 825.2750244140625, 835.2089843749999, 845.1439819335936, 855.0819702148438, 865.02001953125, 874.9600219726562, 884.9019775390626, 894.8449707031251, 904.7890014648439, 914.7349853515625, 924.6820068359375, 934.6300048828125, 944.5789794921876, 954.5289916992189, 964.4810180664062, 974.4329833984375, 984.3870239257812, 994.3410034179686, 1004.2999877929686])
        xsect = [2.2872371807131917, 2.8135428858130354, 2.9215654112092295, 2.8906712369536933, 2.8054091204316762, 2.7034821630650185, 2.602908596166884, 2.514955685492424, 2.4470240005567376, 2.409672418888676, 2.4083771240412943, 1.9498802666174695, 1.4485705947782037, 1.0959550297211182, 0.847550772577563, 0.6681776528477387, 0.5346459504526848, 0.4332700227029527, 0.3548567043626356, 0.29286567818315845, 0.24379598374009606, 0.20408960301405799, 0.17188582122841822, 0.1454816491718063, 0.12370115723079422, 0.1056092450466366, 0.09050928140664283, 0.07784758520685386, 0.06716917009575461, 0.05813820180397667, 0.05046511350204552, 0.0439154031994784, 0.03831678997224497, 0.03351450169939608, 0.029375675269573742, 0.025810061201359424, 0.02272358250067174, 0.02004280193352348, 0.01771427796753899, 0.01568353458748195, 0.013912591075002641, 0.01236099057157114, 0.011001313848811312, 0.009803389010304753, 0.008753583107483085, 0.007825357170110903, 0.0070044575965535205, 0.0062802243721207796, 0.005637019466532502, 0.005067572196792519, 0.004559658608984689, 0.004109667541201511, 0.00370626296178736, 0.003347301768424792, 0.0030265166682832967, 0.0027396457450396673, 0.002481797985951092, 0.0022511134199910414, 0.002044541670469246, 0.0018576591418176386, 0.0016902503749593456, 0.0015386079680755338, 0.0014026866721802977, 0.0012794058550784917, 0.0011675349681908307, 0.0010672075713680227, 0.0009760443852912037, 0.0008931683215429588, 0.0008177984873856126, 0.0007497039625211019, 0.0006877089859333268, 0.0006312409249280246, 0.000580176797999668, 0.0005332517943338541, 0.0004904563754298125, 0.00045141402832644603, 0.0004160860794064254, 0.00038353201583537366]
    else:
        m = array('f', [260, 275, 300, 325, 350, 400, 450, 500, 600, 700, 750, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500, 1600, 1800, 2000, 2250, 2500, 2750, 3000])
        xsect = [2.6742310387, 2.9190982626, 2.6859151204, 2.4535980702, 2.2189669757, 0.5593503107, 0.2054836430, 0.0893764994, 0.0428614977, 0.0220075085, 0.0067277170, 0.0023742485, 0.0009316554, 0.0003971562]
    
    xs = array('f', [])
    for x in xsect:        

        if hdecay:
            #x *= 0.577 * 0.0632 * 2
            x *= 0.5825 *0.06272 * 2

        #x *= chanmap[chan]
        xs.append(x)        
    return m, xs
    
def graviton(chan, hdecay = False, c = None):
    # Based on https://github.com/CrossSectionsLHC/WED/tree/master/KKGraviton_Bulk
    # Refs:
    # [1] https://arxiv.org/pdf/1404.0102v2.pdf                                                                                                 
    # [2] https://arxiv.org/abs/1510.03865                                                                                                      
    # [3] https://github.com/CrossSectionsLHC/WED
    # For table and code to reproduce see: /afs/cern.ch/user/g/gwilliam/public/DBL/RSG/
    
    #m = array('f', [260,300,400,500,600,700,750,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2100,2200,2300,2400,2500,2600,2700,2800,2900,3000])
    #xsect = [8.10e+01,1.60e+03,2.74e+03,1.45e+03,6.76e+02,3.35e+02,2.41e+02,1.77e+02,9.63e+01,5.59e+01,3.54e+01,2.24e+01,1.42e+01,8.99e+00,5.70e+00,3.90e+00,2.67e+00,1.82e+00,1.28e+00,9.03e-01,6.58e-01,4.80e-01,3.50e-01,2.55e-01,1.86e-01,1.40e-01,1.05e-01,7.83e-02,5.87e-02,4.40e-02]

    m = array('f', range(260, 501, 1) + [600,700,750,800,900,1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2100,2200,2300,2400,2500,2600,2700,2800,2900,3000])

    xsect = [80.9872,99.9539,120.843,143.591,168.128,194.376,222.259,251.693,282.595,314.882,348.468,383.269,419.202,456.184,494.134,532.972,572.62,613.002,654.043,695.674,737.823,780.424,823.411,866.723,910.298,954.08,998.013,1042.04,1086.12,1130.2,1174.23,1218.17,1261.97,1305.6,1349.02,1392.2,1435.09,1477.67,1519.91,1561.78,1603.25,1643.93,1684.15,1723.89,1763.12,1801.83,1840,1877.61,1914.66,1951.12,1986.98,2022.24,2056.89,2090.91,2124.3,2157.04,2189.15,2220.6,2251.4,2281.55,2311.03,2339.85,2368.01,2395.51,2422.34,2448.51,2474.02,2498.87,2523.05,2546.59,2569.47,2591.7,2613.29,2634.24,2654.55,2674.22,2693.28,2711.71,2729.52,2746.73,2763.33,2779.34,2794.75,2809.58,2823.84,2837.52,2850.64,2863.21,2875.22,2886.7,2897.64,2907.56,2916.55,2924.73,2932.17,2938.9,2944.95,2950.33,2955.07,2959.18,2962.68,2965.58,2967.89,2969.64,2970.83,2971.47,2971.58,2971.17,2970.26,2968.84,2966.95,2964.59,2961.76,2958.49,2954.78,2950.65,2946.1,2941.15,2935.81,2930.09,2923.99,2917.54,2910.74,2903.61,2896.14,2888.36,2880.26,2871.87,2863.19,2854.24,2845.01,2835.52,2825.78,2815.8,2805.59,2795.15,2784.49,2773.63,2762.56,2751.31,2739.87,2728.18,2716.33,2704.32,2692.16,2679.85,2667.39,2654.81,2642.1,2629.27,2616.33,2603.28,2590.13,2576.88,2563.55,2550.13,2536.63,2523.06,2509.42,2495.72,2481.96,2468.15,2454.29,2440.39,2426.45,2412.47,2398.46,2384.43,2370.37,2356.3,2342.2,2328.1,2313.99,2299.88,2285.76,2271.65,2257.54,2243.43,2229.34,2215.27,2201.21,2187.16,2173.14,2159.15,2145.18,2131.24,2117.33,2103.45,2089.61,2075.8,2062.04,2048.31,2034.63,2020.99,2007.4,1993.85,1980.35,1966.91,1953.51,1940.17,1926.88,1913.65,1900.47,1887.35,1874.29,1861.29,1848.35,1835.47,1822.66,1809.9,1797.21,1784.59,1772.03,1759.54,1747.11,1734.75,1722.46,1710.23,1698.08,1685.99,1673.97,1662.03,1650.15,1638.34,1626.61,1614.94,1603.35,1591.83,1580.37,1568.99,1557.69,1546.45,1535.29,1524.19,1513.17,1502.23,1491.35,1480.55,1469.82,1459.16,1448.57]

    xsect += [6.76e+02,3.35e+02,2.41e+02,1.77e+02,9.63e+01,5.59e+01,3.54e+01,2.24e+01,1.42e+01,8.99e+00,5.70e+00,3.90e+00,2.67e+00,1.82e+00,1.28e+00,9.03e-01,6.58e-01,4.80e-01,3.50e-01,2.55e-01,1.86e-01,1.40e-01,1.05e-01,7.83e-02,5.87e-02,4.40e-02]

    #print m
    #print xsect

    print len(m), len(xsect)
    
    xs = array('f', [])
    for x in xsect:        
        x /= 1000.

        if c is not None:
            x *= (c/1.0)**2

        if hdecay:
            #x *= 0.577 * 0.0632 * 2
            x *= 0.5825 *0.06272 * 2

        #x *= chanmap[chan]
        xs.append(x)        

    return m, xs


def ATLASLabel(suffix = "Internal", pos = [300,0.6], hdecay = True, lamb = False, plot2 = False):
#def ATLASLabel(suffix = "Preliminary", pos = [300,2], hdecay = True, lamb = False, plot2 = False):
#def ATLASLabel(suffix = "", pos = [300,2], hdecay = True, lamb = False, plot2 = False):
#def ATLASLabel(suffix = "Internal", pos = [300,2]):
    x = pos[0]
    y = pos[1]

    if plot2:
        delx = 110
    else:
        delx = 110
        
    y2 = 0.3
    
    if not hdecay:
        y = 20
        y2 = 10

    if lamb:
        x = -20
        y = 900
        y2 = 840
        delx = 7

    if plot2:
        y2 = y - 0.7

        #ts = 0.1
        ts = 0.07
    else:
        ts = 0.05
    
    l = R.TLatex()
    l.SetTextSize(ts)    
    l.SetTextFont(72)
    l.DrawLatex(x, y, "ATLAS")
    p = R.TLatex()
    p.SetTextFont(42)
    p.SetTextSize(ts)
    p.DrawLatex(x+delx, y, suffix)

    e = R.TLatex()
    e.SetTextFont(42)
    e.SetTextSize(ts)
    e.DrawLatex(x, y2, "13 TeV, 139 fb^{-1}")

    return l,p

def compareLines(line1, line2, lowMass, highMass):
    points = []

    for loopMass in range(lowMass,highMass):
        curMass = loopMass
        points.append(abs(line1.Eval(curMass)-line2.Eval(curMass)))

    print "Cross-over mass:", (lowMass+points.index(min(points)))

    return

def plot(name, masses, same=False, color=R.kRed, legend="exp (G, SLT, 10 pb)", chan="lephad", dobbyy=False, dobbbb=False, dorun1=False, docms=False, dosinglet = False, dograviton = False,
         dohMSSM=False, scalecms=False, hdecay=True, dump=True, cont=False, scale_lumi = 1, minjection=None, RSG = None, noobs=False, plot2 = False, outputName="Limit"):

    m = array('f', [])
    o = array('f', []);e = array('f', [])
    m2 = array('f', []);m1 = array('f', [])
    p1 = array('f', []);p2 = array('f', [])

    stat_only = "StatOnly" in name

    if RSG is None:
        RSG = 2 if 'G2' in legend else (1 if "G" in legend else 0)
    
    xs = xsect(scale=False, G=RSG, chan=chan, hdecay=hdecay, stat = stat_only, scale_lumi=scale_lumi)

    for mass in masses:
        if 'afs' in name:
            f = R.TFile.Open(name.format(mass) + "/" + str(mass) + ".root")
        else:
            #f = R.TFile.Open("root-files/" + name.format(mass) + "/" + str(mass) + ".root")
            f = R.TFile.Open("output/" + name.format(mass) + "/root-files/obs/" + str(mass) + ".root")

        if f:
            info = f.Get("limit")
        else:
            info = None
            
        if info:
            vo  = info.GetBinContent(1) * xs[mass] 
            if minjection:
            #if minjection == mass:                                
                print "Found injected mass", minjection
                ve  = info.GetBinContent(7) * xs[mass] 
            else:
                ve  = info.GetBinContent(2) * xs[mass] 
            vm1 = ve - info.GetBinContent(5) * xs[mass]
            vp1 = info.GetBinContent(4) * xs[mass] - ve
            vm2 = ve - info.GetBinContent(6) * xs[mass]
            vp2 = info.GetBinContent(3) * xs[mass] - ve
        else:
            vo = ve = vm1 = vp1 = vm2 = vp2 = 0

        m.append(mass)
        #o.append(vo)
        o.append(0)
        e.append(ve)
        m1.append(vm1)
        m2.append(vm2)
        p1.append(vp1)
        p2.append(vp2)

        if f:
            f.Close()

    if dump:
        print "#"*100
        print legend
        print "#"*100        

        print m
        print e
        print o

        for i, mass in enumerate(m):            
            print "{:4.0f} & {:7.2f} & {:7.2f} & {:7.2f} & {:7.2f} & {:7.2f} & {:7.2f} \\\\".format(mass, o[i]*1000, (e[i] - m2[i])*1000, (e[i] - m1[i])*1000, e[i]*1000, (e[i] + p1[i])*1000, (e[i] + p2[i])*1000)
    
    z = array('f', [0]*len(m))

    ge = R.TGraphAsymmErrors(len(m), m, e, z, z, z, z)
    if same:
        ge.SetLineColor(color); ge.SetMarkerColor(color);ge.SetMarkerStyle(10);ge.SetMarkerSize(0);    
        if chan == "tautau":
            ge.SetLineWidth(3);
        else:
            ge.SetLineWidth(2);
    else:
        ge.SetLineColor(R.kBlack); ge.SetMarkerColor(R.kBlack);ge.SetMarkerStyle(10); ge.SetMarkerSize(0);      
        if chan == "tautau":
            ge.SetLineWidth(3);
        else:
            ge.SetLineWidth(2);

    go = R.TGraphAsymmErrors(len(m), m, o, z, z, z, z)
    go.SetLineColor(R.kBlack);go.SetMarkerColor(R.kBlack);go.SetMarkerStyle(10); go.SetMarkerSize(0.8);    

    if not same or cont:
        ge.SetLineStyle(2)

    #ge.SetLineStyle(2 if '10' in legend else 1)        
    
    g1 = R.TGraphAsymmErrors(len(m), m, e, z, z, m1, p1)
    g1.SetFillColor(R.kGreen)

    g2 = R.TGraphAsymmErrors(len(m), m, e, z, z, m2, p2)
    g2.SetFillColor(R.kYellow)

    #if dorun1:
     #   mr1, er1, er1s = run1(hdecay=hdecay)
      #  gr1 = R.TGraphAsymmErrors(len(mr1), mr1, er1, z, z, z, z)
       # gr1.SetLineColor(R.kBlue);gr1.SetMarkerColor(R.kBlue)
      #  gr1s = R.TGraphAsymmErrors(len(mr1), mr1, er1s, z, z, z, z)
      #  gr1s.SetLineColor(R.kBlue);gr1s.SetMarkerColor(R.kBlue);gr1s.SetLineStyle(2)        

   # if dobbyy:
   #     mby, eby = bbyy()
    #    gby = R.TGraphAsymmErrors(len(mby), mby, eby, z, z, z, z)
     #   gby.SetLineColor(R.kMagenta+3);gby.SetMarkerColor(R.kMagenta+3)    

   # if dobbbb:
   #     m4b, e4b = bbbb()
    #    g4b = R.TGraphAsymmErrors(len(m4b), m4b, e4b, z, z, z, z)
    #    g4b.SetLineColor(R.kMagenta);g4b.SetMarkerColor(R.kMagenta)#;g4b.SetLineStyle(2)

   # if docms:
    #    mc, ec = cms(chan)
     #   gc = R.TGraphAsymmErrors(len(mc), mc, ec, z, z, z, z)
      #  gc.SetLineColor(R.kMagenta);gc.SetMarkerColor(R.kMagenta)
#     gcs = R.TGraphAsymmErrors(len(mc), mc, ecs, z, z, z, z)
#     gcs.SetLineColor(R.kMagenta);gcs.SetMarkerColor(R.kMagenta)        
#     gcs.SetLineStyle(2)

    ms, xs = singlet(chan, hdecay)
    gs = R.TGraphAsymmErrors(len(ms), ms, xs, z, z, z, z)
    gs.SetLineColor(R.kMagenta);gs.SetMarkerColor(R.kMagenta)

    mh, xh = hMSSM(chan, hdecay, full=True)
    gh = R.TGraph(len(mh), mh, xh)
    gh.SetLineColor(R.kMagenta);gh.SetMarkerColor(R.kMagenta); gh.SetMarkerStyle(10); gh.SetMarkerSize(0.8);    

    mg, xg = graviton(chan, hdecay, RSG)
    gg = R.TGraph(len(mg), mg, xg)
    gg.SetLineColor(R.kMagenta);gg.SetMarkerColor(R.kMagenta); gg.SetMarkerStyle(10); gg.SetMarkerSize(0.8);    

    if dograviton:
        if RSG == 1:
            compareLines(gg,go,300,400)
            compareLines(gg,go,800,900)


    if dohMSSM:
        compareLines(gh,go,260,350)
        compareLines(gh,go,350,500)




    if RSG is None:        
        X = 'G_{kk}' if 'G_{kk}' in legend else 'X'
    else:
        X = 'G_{kk}' if RSG else 'X'


    if plot2:
        g = R.TMultiGraph("", "; Resonance Mass [GeV]; #sigma (%s #rightarrow HH #rightarrow bb#tau#tau) [pb]" % X)
    elif RSG ==1 or RSG == 2:
        g = R.TMultiGraph("", "; m(G_{kk}) [GeV]; #sigma (%s #rightarrow HH #rightarrow bb#tau#tau) [pb]" % X)
    else:
        #g = R.TMultiGraph("", "Limits for " + chan + (";m_{"+X+"} [GeV]; 95% CL Limit on #sigma_{"+X+"} x BR_{"+X+"#rightarrowhh#rightarrowbb#tau#tau} [pb]" if hdecay else
                                                  #";m_{"+X+"} [GeV]; 95% CL Limit on #sigma_{"+X+"} x BR_{"+X+"#rightarrowhh} [pb]"))
        #g = R.TMultiGraph("", "; Resonance Mass [GeV]; #sigma (%s #rightarrow hh #rightarrow bb#tau#tau) [pb]" % X)
        g = R.TMultiGraph("", "; m(X) [GeV]; #sigma (%s #rightarrow HH #rightarrow bb#tau#tau) [pb]" % X)

    if not same or cont:
        g.Add(g2, "E3")
        g.Add(g1, "E3")
        #if not noobs:
            #g.Add(go, "PL*")
         #   g.Add(go, "PL")

    #g.Add(ge, "PL*")
    g.Add(ge, "PL")
    if same:
        leg.AddEntry(ge, legend, "lp")
    elif not cont:
        if not noobs:
            leg.AddEntry(go, legend.replace("exp", "obs").replace("Exp", "Obs"), "lp")    
        leg.AddEntry(ge, legend, "lp")
        if plot2:
            leg.AddEntry(g1, "#pm 1#sigma", "f")
            leg.AddEntry(g2, "#pm 2#sigma", "f")
        else:
            leg.AddEntry(g1, "#pm 1#sigma", "f")
            leg.AddEntry(g2, "#pm 2#sigma", "f")
        
        if dorun1:
            g.Add(gr1, "PL*")
            leg.AddEntry(gr1, "run 1 H (unscaled 8TeV, syst)", "l")
            #g.Add(gr1s, "PL*")
            #leg.AddEntry(gr1s, "run 1 H (scaled 8TeV -> 13TeV, syst)", "l")                    


        if dobbyy and not hdecay:
            g.Add(gby, "PL*")
            leg.AddEntry(gby, "run 2 H bbyy (scaled to 13.2 fb-1, syst)", "l")

        if dobbbb and not hdecay:
            g.Add(g4b, "PL*")
            leg.AddEntry(g4b, "run 2 G 4b (13.2 fb-1, syst)", "l")                

        if docms and not 'G' in legend:
            g.Add(gc, "PL*")
            leg.AddEntry(gc, "run 2 CMS H (12.9 fb-1 " + (",lh + hh)" if chan == "lephad" else ")"), "l")
            #if scalecms:
            #    g.Add(gcs, "PL*")
            #    leg.AddEntry(gcs, "run 2 CMS H (13.2 fb-1 " + ",lh + hh)" if chan == "lephad" else ")", "l")

        if dosinglet:
            g.Add(gs, "C")
            leg.AddEntry(gs, "H singlet", "l")

        if dohMSSM:
            g.Add(gh, "C")
            if plot2 == 1:
                leg1.AddEntry(gh, "hMSSM Scalar (tan#beta = 2)", "l")
            else:
                leg1.AddEntry(gh, "hMSSM Scalar (tan#beta = 2)", "l")     

        if dograviton:
            g.Add(gg, "C")
            if plot2 == 2:
                leg2.AddEntry(gg, "Bulk RS Graviton (k/#bar{{M}}_{{pl}} = {0})".format("2.0" if RSG == 2 else "1.0"), "l")
            else:
                leg2.AddEntry(gg, "Bulk RS Graviton (k/#bar{{M}}_{{pl}} = {0})".format("2.0" if RSG == 2 else "1.0"), "l")    

    R.gPad.SetLogy(True)
    g.Draw("P" if same else "AP")

    if not same and not plot2 == 2:
        l = R.TLatex()
        if plot2:
            l = ATLASLabel(hdecay=hdecay, pos = [720, 1], plot2 = plot2)
        else:
            l = ATLASLabel(hdecay=hdecay)
        c._hists.append(l)

    if not same and plot2 == 1:
        l = R.TLatex()
        l.SetTextSize(0.07)    
        l.SetTextFont(42)
        #l.DrawLatex(300, 1, "Scalar")
    elif not same and plot2 == 2:
        l = R.TLatex()
        l.SetTextSize(0.07)    
        l.SetTextFont(42)
        #l.DrawLatex(300, 1, "Bulk RS")
    elif not plot2:
        l = R.TLatex()
        l.SetTextSize(0.05)    
        l.SetTextFont(42)
        #l.DrawLatex(300, 0.002, "Bulk RS")

    if not same:
        g.GetXaxis().SetLimits(250, 1010)
        if hdecay and plot2:            
            g.GetYaxis().SetRangeUser(1.1e-3, 3.5)
        elif hdecay:
            g.GetYaxis().SetRangeUser(4*1e-4, 1)
        else:
            g.GetYaxis().SetRangeUser(1e-2, 30)

    c._hists.append(g)
    c._hists.append(ge)

    c.Print(outputName+".eps")
    g.Delete()


    return m, e


def ratio(m, exps, name="ratio.eps", bottom=False, title = "Ratio", legends = [], ymin = 0, ymax = 1.5, lamb = False):

    gm = R.TMultiGraph("", title + (";#lambda" if lamb else ";m_{X} [GeV]") + "; Ratio of expected limits")
    c = R.TCanvas()
    c._hists = []

    if bottom:
        leg = R.TLegend(0.58,0.20,0.89,0.49)
    else:
        leg = R.TLegend(0.2,0.80,0.89,0.89)
    leg.SetBorderSize(0)
    leg.SetFillColor(0)
    
    nom = exps[0]
    for i,e in enumerate(exps[1:]):
        ratio = array('f', [r[0]/r[1] if r[1] != 0 else 0 for r in zip(e,nom)])
        g = R.TGraph(len(m), m, ratio)
        g.SetLineColor(i+1)
        g.SetMarkerColor(i+1)
        gm.Add(g, "PL*")        
        c._hists.append(g)        
        leg.AddEntry(g, legends[i] if legends else "new/old", "l")

        gm.SetMinimum(ymin)
        gm.SetMaximum(ymax)
    gm.Draw("AP")
    leg.Draw()
    l = R.TLine(250, 1, 1000, 1)
    l.SetLineColor(1)
    l.SetLineStyle(2)

    l2u = R.TLine(250, 0.95, 1000, 0.95)
    l2u.SetLineColor(2)
    l2u.SetLineStyle(2)

    l2d = R.TLine(250, 1.05, 1000, 1.05)
    l2d.SetLineColor(2)
    l2d.SetLineStyle(2)    
    
    l.Draw()
    #l2u.Draw()
    #l2d.Draw()
    
    c.Print(name)

def non_res(name, same=False, chan="lephad", lamb = None, reweight=True, noprint = False, hdecay=False, label="", ZZ = False, lambdaNLO = False):
    #f = R.TFile.Open("root-files/" + name + "/0.root")
    f = R.TFile.Open("output/" + name + "/root-files/obs/0.root")
    info = f.Get("limit")
    stat_only = "StatOnly" in name    
    xs = xsect(scale=False, non_res=True, chan=chan, stat=stat_only, hdecay=hdecay, lamb=lamb, reweight=reweight, ZZ = ZZ, lambdaNLO = lambdaNLO)
    #vo  = info.GetBinContent(1) * xs
    vo = 0
    ve  = info.GetBinContent(2) * xs 
    vm1 = info.GetBinContent(5) * xs
    vp1 = info.GetBinContent(4) * xs 
    vm2 = info.GetBinContent(6) * xs
    vp2 = info.GetBinContent(3) * xs 

    if ZZ:
        sm = 16.445 * 0.91
    else:
        if hdecay:
            sm = 31.05 * 0.5824 *0.06272 * 2
        else:
            sm = 31.05 / 1000. # fb -> pb

    if noprint:
        return vo, ve - vm2, ve - vm1, ve, vp1 - ve, vp2 - ve

    if not same:
        print "{:10s} {:10s} {:10s} {:10s} {:10s} {:10s} {:10s}".format("non-res", "obs", "-2sig", "-1sig", "exp", "+1sig", "+2sig")

    print label or name
    if hdecay:
        print "{:10s} {:10.2f} {:10.2f} {:10.2f} {:10.2f} {:10.2f} {:10.2f}".format("xs [fb]", vo, vm2, vm1, ve, vp1, vp2)
        print "{:10s} {:10.2f} {:10.2f} {:10.2f} {:10.2f} {:10.2f} {:10.2f}".format("xs/xs(SM)", vo/sm, vm2/sm, vm1/sm, ve/sm, vp1/sm, vp2/sm)
    else:
        print "{:10s} {:10.2f} {:10.2f} {:10.2f} {:10.2f} {:10.2f} {:10.2f}".format("xs [pb]", vo, vm2, vm1, ve, vp1, vp2)
        print "{:10s} {:10.2f} {:10.2f} {:10.2f} {:10.2f} {:10.2f} {:10.2f}".format("xs/xs(SM)", vo/sm, vm2/sm, vm1/sm, ve/sm, vp1/sm, vp2/sm)

    with open("limit.bbtt.SM."+chan+".BDT.txt", "a" if same else "w" ) as f:
        if not same:
            f.write("{:10s} {:10s} {:10s} {:10s} {:10s} {:10s} {:10s}\n".format("non-res", "obs", "-2sig", "-1sig", "exp", "+1sig", "+2sig"))
        f.write(label or name)
        f.write("\n")
        f.write("{:10s} {:10.2f} {:10.2f} {:10.2f} {:10.2f} {:10.2f} {:10.2f}\n".format("xs [pb]", vo, vm2, vm1, ve, vp1, vp2))
        f.write("{:10s} {:10.2f} {:10.2f} {:10.2f} {:10.2f} {:10.2f} {:10.2f}\n".format("xs/xs(SM)", vo/sm, vm2/sm, vm1/sm, ve/sm, vp1/sm, vp2/sm))

    return vo, ve - vm2, ve - vm1, ve, vp1 - ve, vp2 - ve

def plot_lambda(name, lambdas, hdecay = True, reweight=True, legend = "", color = R.kBlack, same = False, marker = False, lambdaNLO = False):

    lamb = array('f', [])
    o = array('f', []);e = array('f', [])
    m2 = array('f', []);m1 = array('f', [])
    p1 = array('f', []);p2 = array('f', [])

    for l in lambdas:
        if reweight:
            sl = "L{:02d}".format(l) if l >= 0 else "Lm{:02d}".format(abs(l))
        else:
            sl = "{:02d}".format(l) if l >= 0 else "m{:02d}".format(abs(l))        
        vo, vm2, vm1, ve, vp1, vp2 = non_res(name.format(sl), chan="lephad", lamb = l, reweight=reweight, hdecay = hdecay, noprint = True, lambdaNLO = lambdaNLO)

        lamb.append(l)
        o.append(vo*1000)
        e.append(ve*1000)
        m1.append(vm1*1000)
        m2.append(vm2*1000)
        p1.append(vp1*1000)
        p2.append(vp2*1000)

    z = array('f', [0]*len(lamb))
    ge = R.TGraphAsymmErrors(len(lamb), lamb, e, z, z, z, z)
    if same:
        ge.SetLineColor(color);ge.SetMarkerColor(color)  
    else:
        ge.SetLineColor(R.kBlack);ge.SetMarkerColor(R.kBlack)
        ge.SetLineStyle(2)

    go = R.TGraphAsymmErrors(len(lamb), lamb, o, z, z, z, z)
    go.SetLineColor(R.kBlack);go.SetMarkerColor(R.kBlack)

    g1 = R.TGraphAsymmErrors(len(lamb), lamb, e, z, z, m1, p1)
    g1.SetFillColor(R.kGreen)

    g2 = R.TGraphAsymmErrors(len(lamb), lamb, e, z, z, m2, p2)
    g2.SetFillColor(R.kYellow)

    g = R.TMultiGraph("", " ; #lambda/#lambda_{SM}; #sigma (HH #rightarrow bb#tau#tau) [fb]" if hdecay else
                      "  ; #lambda/#lambda_{SM}; #sigma (HH #rightarrow bb#tau#tau) [fb]")

    if not same:
        g.Add(g2, "E3")
        g.Add(g1, "E3")

    if marker:
        ge.SetMarkerStyle(10)
        g.Add(ge, "P")
    else:
        g.Add(ge, "PL")    
        g.Add(go, "PL")

    if same:
        legl.AddEntry(ge, legend, "p" if marker else "l")
    else:
        legl.AddEntry(ge, legend, "lp" if marker else "l")
        leg.AddEntry(go, legend.replace("exp", "obs").replace("Exp", "Obs"), "lp")

        leg.AddEntry(g1, "#pm 1#sigma", "f")
        leg.AddEntry(g2, "#pm 2#sigma", "f")

    g.Draw("P" if same else "AP")

    if not same:
        hXS = R.TH1F("", "hXS", 400, -20, 20)
        #hXS.Add(flambda , 33.41/(1.4412E-02)) # Add, scaling by lambda = 1 k-fact and converting to fb       
        hXS.Add(flambdaNLO, 1000.) 
        if hdecay:
            #hXS.Scale(0.577 * 0.0632 * 2)
            hXS.Scale(0.5825 *0.06272 * 2)

        hXS.SetLineColor(R.kRed+3)
        hXS.Draw("lsame")
        #legl.AddEntry(hXS, "Theory Prediction (#lambda = 1 k-fact)", "l")
        legl.AddEntry(hXS, "Theory Prediction (YR4 pol2)", "l")        
        c._hists.append(hXS)

    if not same:
        l = R.TLatex()        
        l = ATLASLabel(lamb = True)
        c._hists.append(l)

    c._hists.append(g)
    c._hists.append(ge)

    return lamb, e

    
if __name__ == '__main__':

    R.gROOT.SetBatch(True)

    chan = "hadhad"

    non_res("26092019.14102019New_HH_13TeV_14102019New_Systs_hadhad_SM_BDT_0", chan="hadhad", hdecay=True)
    non_res("26092019.14102019New_HH_13TeV_14102019New_FloatOnly_hadhad_SM_BDT_0", chan="hadhad", hdecay=True)
    non_res("26092019.14102019New_HH_13TeV_14102019New_StatOnly_hadhad_SM_BDT_0", chan="hadhad", hdecay=True)
   
    c = R.TCanvas()
    #c = R.TCanvas("", "", 600, 600)
    c._hists = []
    leg = R.TLegend(0.58,0.60,0.89,0.89)
    leg.SetBorderSize(0)
    leg.SetFillColor(0)

    legl = R.TLegend(0.15,0.50,0.40,0.7)
    legl.SetBorderSize(0)
    legl.SetFillColor(0)    

    #outver = sys.argv[1] if len(sys.argv) > 1 else "150916"

    R.gStyle.SetOptTitle(0)
    tsize = 0.06
    R.gStyle.SetLabelSize(tsize,"x")
    R.gStyle.SetTitleSize(tsize,"x")
    R.gStyle.SetLabelSize(tsize,"y")
    R.gStyle.SetTitleSize(tsize,"y")
    R.gStyle.SetLabelSize(tsize,"z")
    R.gStyle.SetTitleSize(tsize,"z")
    R.gStyle.SetTitleOffset(0.7, "y")
   

    masses = [251, 280, 300, 325, 350, 400, 450, 500, 550, 600, 700, 800, 900, 1000]
   
  
    leg = R.TLegend(0.58,0.55,0.89,0.89)    
    leg.SetBorderSize(0)
    leg.SetFillColor(0)
    leg2 = R.TLegend(0.12,0.1,0.4,0.26)    
    leg2.SetBorderSize(0)
    leg2.SetFillColor(0)
    leg2.SetFillStyle(0)
    leg2.SetTextSize(0.038)
    leg1 = R.TLegend(0.1,0.1,0.3,0.26)    
    leg1.SetBorderSize(0)
    leg1.SetFillColor(0)
    leg1.SetFillStyle(0)
    leg1.SetTextSize(0.038)
    tsize = 0.035
    R.gStyle.SetLabelSize(tsize,"x")
    R.gStyle.SetTitleSize(tsize,"x")
    R.gStyle.SetLabelSize(tsize,"y")
    R.gStyle.SetTitleSize(tsize,"y")
    R.gStyle.SetLabelSize(tsize,"z")
    R.gStyle.SetTitleSize(tsize,"z")
    R.gStyle.SetTitleOffset(1, "y")
    R.gStyle.SetTitleOffset(1.2, "x")

    plot("26092019.14102019New_HH_13TeV_14102019New_Systs_hadhad_2HDM_BDT_{0}", masses, same=False, dorun1=False, dograviton=False, RSG=0, chan=chan, legend="Exp 95% CL limit", plot2=False)
    leg.Draw()
    leg2.Draw()
    leg.Clear()
    leg2.Clear()
    c.Clear()


    sys.exit()

    chan = "tautau"

    non_res("HHCombo080318.080318HH_HH_13TeV_080318HH_StatOnly_tautau_SMRW_BDT_0", chan="tautau", hdecay=True)
    non_res("HHCombo080318.080318HH_HH_13TeV_080318HH_Systs_tautau_SMRW_BDT_0", chan="tautau", hdecay=True)

    non_res("HHComboTest.Test_HH_13TeV_Test_StatOnly_tautau_SMRW_BDT_0", chan="tautau", hdecay=True)
    non_res("HHComboTest.Test_HH_13TeV_Test_Systs_tautau_SMRW_BDT_0", chan="tautau", hdecay=True)

    sys.exit()

    chan="tautau"
    non_res("HHCombo080318.080318HH_HH_13TeV_080318HH_Systs_tautau_SMRW_BDT_0_obs", chan="tautau", hdecay=True)
    non_res("HHCombo080318.080318HH_HH_13TeV_080318HH_StatOnly_tautau_SMRW_BDT_0_obs", chan="tautau", hdecay=True)
    non_res("HHCombo080318.240718HHNoXsecSyst_HH_13TeV_240718HHNoXsecSyst_Systs_tautau_SMRW_BDT_0_obs", chan="tautau", hdecay=True)
    non_res("HHCombo080318.240718HHNoXsecSyst_HH_13TeV_240718HHNoXsecSyst_StatOnly_tautau_SMRW_BDT_0_obs", chan="tautau", hdecay=True)
  

    sys.exit()
    
    chan = "lephad"

    non_res("HHSLT080318.080318SLT_HH_13TeV_080318SLT_Systs_lephad_SM_BDT_0_obs", chan="lephad", hdecay=True)
    non_res("HHSLT080318.080318SLT_HH_13TeV_080318SLT_Systs_lephad_SMRW_BDT_0_obs", chan="lephad", hdecay=True)

    non_res("HHLTT080318.HHLTT080318_HH_13TeV_HHLTT080318_Systs_lephad_SM_BDT_0_obs", chan="lephad", hdecay=True)
    non_res("HHLTT080318.HHLTT080318_HH_13TeV_HHLTT080318_Systs_lephad_SMRW_BDT_0_obs", chan="lephad", hdecay=True)

    non_res("HHCombo080318.080318HH_HH_13TeV_080318HH_Systs_lephad_SM_BDT_0_obs", chan="lephad", hdecay=True)
    non_res("HHCombo080318.080318HH_HH_13TeV_080318HH_Systs_lephad_SMRW_BDT_0_obs", chan="lephad", hdecay=True)

    chan = "hadhad"

    non_res("HHCombo080318.080318HH_HH_13TeV_080318HH_Systs_hadhad_SM_BDT_0_obs", chan="hadhad", hdecay=True)
    non_res("HHCombo080318.080318HH_HH_13TeV_080318HH_Systs_hadhad_SMRW_BDT_0_obs", chan="hadhad", hdecay=True)

    chan = "tautau"

    non_res("HHCombo080318.080318HH_HH_13TeV_080318HH_Systs_tautau_SM_BDT_0_obs", chan="hadhad", hdecay=True)
    non_res("HHCombo080318.080318HH_HH_13TeV_080318HH_Systs_tautau_SMRW_BDT_0_obs", chan="hadhad", hdecay=True)
    
    chan = "lephad"

    non_res("HHSLT080318.080318SLT_HH_13TeV_080318SLT_Systs_lephad_SM_BDT_0_obs", chan="lephad", hdecay=True)
    non_res("HHSLT080318.080318SLT_HH_13TeV_080318SLT_Systs_lephad_SMRW_BDT_0_obs", chan="lephad", hdecay=True)

    non_res("HHLTT080318.HHLTT080318_HH_13TeV_HHLTT080318_Systs_lephad_SM_BDT_0_obs", chan="lephad", hdecay=True)
    non_res("HHLTT080318.HHLTT080318_HH_13TeV_HHLTT080318_Systs_lephad_SMRW_BDT_0_obs", chan="lephad", hdecay=True)

    non_res("HHCombo080318.080318HH_HH_13TeV_080318HH_Systs_lephad_SM_BDT_0_obs", chan="lephad", hdecay=True)
    non_res("HHCombo080318.080318HH_HH_13TeV_080318HH_Systs_lephad_SMRW_BDT_0_obs", chan="lephad", hdecay=True)

    chan = "hadhad"

    non_res("HHCombo080318.080318HH_HH_13TeV_080318HH_Systs_hadhad_SM_BDT_0_obs", chan="hadhad", hdecay=True)
    non_res("HHCombo080318.080318HH_HH_13TeV_080318HH_Systs_hadhad_SMRW_BDT_0_obs", chan="hadhad", hdecay=True)

    chan = "tautau"

    non_res("HHCombo080318.080318HH_HH_13TeV_080318HH_Systs_tautau_SM_BDT_0_obs", chan="tautau", hdecay=True)
    non_res("HHCombo080318.080318HH_HH_13TeV_080318HH_Systs_tautau_SMRW_BDT_0_obs", chan="tautau", hdecay=True)

    sys.exit()
    


    c = R.TCanvas()
    #c = R.TCanvas("", "", 600, 600)
    c._hists = []
    leg = R.TLegend(0.58,0.60,0.89,0.89)
    leg.SetBorderSize(0)
    leg.SetFillColor(0)

    legl = R.TLegend(0.15,0.50,0.40,0.7)
    legl.SetBorderSize(0)
    legl.SetFillColor(0)    

    #outver = sys.argv[1] if len(sys.argv) > 1 else "150916"

    R.gStyle.SetOptTitle(0)
    tsize = 0.06
    R.gStyle.SetLabelSize(tsize,"x")
    R.gStyle.SetTitleSize(tsize,"x")
    R.gStyle.SetLabelSize(tsize,"y")
    R.gStyle.SetTitleSize(tsize,"y")
    R.gStyle.SetLabelSize(tsize,"z")
    R.gStyle.SetTitleSize(tsize,"z")
    R.gStyle.SetTitleOffset(0.7, "y")

    masses = [260, 275, 300, 325, 350, 400, 450, 500, 550, 600, 700, 800, 900, 1000]
    c.Clear()
    p1 = R.TPad("p1", "p1", 0, 0.5, 1, 1)
    p1.SetBottomMargin(0)
    p2 = R.TPad("p2", "p2", 0, 0, 1, 0.5)    
    p2.SetTopMargin(0)
    p2.SetBottomMargin(0.15)    
    p1.Draw()
    p2.Draw()
    p1.cd()
    leg = R.TLegend(0.58,0.55,0.89,0.95)    
    leg.SetBorderSize(0)
    leg.SetFillColor(0)
    leg1 = R.TLegend(0.12,1.5,0.4,0.12)    
    leg1.SetBorderSize(0)
    leg1.SetFillColor(0)
    leg1.SetFillStyle(0)
    leg1.SetTextSize(0.055)
    leg2 = R.TLegend(0.12,1.5,0.4,0.26)    
    leg2.SetBorderSize(0)
    leg2.SetFillColor(0)
    leg2.SetFillStyle(0)
    leg2.SetTextSize(0.055)

    
    m,e1 = plot("HHCombo080318.080318HH_HH_13TeV_080318HH_Systs_tautau_2HDM_BDT_{0}_obs", masses, dohMSSM = True, same=False, chan=chan, legend="Exp 95% CL limit", RSG=0, plot2 = 1)
    #m,e22=plot("HHCombo080318.080318HH_HH_13TeV_080318HH_Systs_lephad_2HDM_BDT_{0}_obs", masses, color=R.kBlue, same=True, dorun1=False, dograviton=False, chan="lephad", legend="Exp 95% CL limit #tau_{lep}#tau_{had}", RSG=0, plot2 = 1)
    #m,e33=plot("HHCombo080318.080318HH_HH_13TeV_080318HH_Systs_hadhad_2HDM_BDT_{0}_obs", masses, color=R.kRed, same=True, dorun1=False, dograviton=False, chan="hadhad", legend="Exp 95% CL limit #tau_{had}#tau_{had}", RSG=0, plot2 = 1)
    leg.Clear()
    leg1.Draw()
    p2.cd()
    masses = [260, 300, 400, 500, 600, 700, 800, 900, 1000]
    m,e1 = plot("HHCombo080318.080318HH_HH_13TeV_080318HH_Systs_tautau_RSGc1_BDT_{0}_obs", masses, dograviton = True, same=False, chan=chan, legend="Exp 95% CL limit", RSG=1, plot2 = 2)
    #m,e22=plot("HHCombo080318.080318HH_HH_13TeV_080318HH_Systs_lephad_RSGc1_BDT_{0}_obs", masses, color=R.kBlue, same=True, dorun1=False, dograviton=False, chan="lephad", legend="Exp 95% CL limit #tau_{lep}#tau_{had}", RSG=1, plot2 = 2)
    #m,e33=plot("HHCombo080318.080318HH_HH_13TeV_080318HH_Systs_hadhad_RSGc1_BDT_{0}_obs", masses, color=R.kRed, same=True, dorun1=False, dograviton=False, chan="hadhad", legend="Exp 95% CL limit #tau_{had}#tau_{had}", RSG=1, plot2 = 2)
    p2.cd()
    leg.Draw()
    leg2.Draw()
    c.Print("Limit_paper.eps")
    c.Print("Limit_paper.pdf")
    c.Print("Limit_paper.C")
    c.Clear()
    leg.Clear()
    leg1.Clear()
    leg2.Clear()
    

    #sys.exit()

    masses = [260, 275, 300, 325, 350, 400, 450, 500, 550, 600, 700, 800, 900, 1000]
    c.Clear()
    leg = R.TLegend(0.58,0.55,0.89,0.89)    
    leg.SetBorderSize(0)
    leg.SetFillColor(0)
    leg2 = R.TLegend(0.12,0.1,0.4,0.26)    
    leg2.SetBorderSize(0)
    leg2.SetFillColor(0)
    leg2.SetFillStyle(0)
    leg2.SetTextSize(0.038)
    leg1 = R.TLegend(0.1,0.1,0.3,0.26)    
    leg1.SetBorderSize(0)
    leg1.SetFillColor(0)
    leg1.SetFillStyle(0)
    leg1.SetTextSize(0.038)
    tsize = 0.035
    R.gStyle.SetLabelSize(tsize,"x")
    R.gStyle.SetTitleSize(tsize,"x")
    R.gStyle.SetLabelSize(tsize,"y")
    R.gStyle.SetTitleSize(tsize,"y")
    R.gStyle.SetLabelSize(tsize,"z")
    R.gStyle.SetTitleSize(tsize,"z")
    R.gStyle.SetTitleOffset(1, "y")
    R.gStyle.SetTitleOffset(1.2, "x")
    m,e11=plot("HHCombo080318.080318HH_HH_13TeV_080318HH_Systs_tautau_RSGc2_BDT_{0}_obs", masses, same=False, dorun1=False, dograviton=True, chan=chan, legend="Exp 95% CL limit", RSG=2, plot2=False)
    m,e22=plot("HHCombo080318.080318HH_HH_13TeV_080318HH_Systs_lephad_RSGc2_BDT_{0}_obs", masses, color=R.kBlue, same=True, dorun1=False, dograviton=False, chan="lephad", legend="Exp 95% CL limit #tau_{lep}#tau_{had}", RSG=2, plot2=False)
    m,e33=plot("HHCombo080318.080318HH_HH_13TeV_080318HH_Systs_hadhad_RSGc2_BDT_{0}_obs", masses, color=R.kRed, same=True, dorun1=False, dograviton=False, chan="hadhad", legend="Exp 95% CL limit #tau_{had}#tau_{had}", RSG=2, plot2=False)
    leg.Draw()
    leg2.Draw()
    c.Print("Limit_paper_RSGc2_all.eps") 
    c.Print("Limit_paper_RSGc2_all.pdf") 
    c.Print("Limit_paper_RSGc2_all.C") 
    leg.Clear()
    leg2.Clear()


    masses = [260, 275, 300, 325, 350, 400, 450, 500, 550, 600, 700, 800, 900, 1000]
    m,e11=plot("HHCombo080318.080318HH_HH_13TeV_080318HH_Systs_tautau_2HDM_BDT_{0}_obs", masses, dohMSSM = True, same=False, dorun1=False, chan=chan, legend="Exp 95% CL limit", RSG=0, plot2=False)
    m,e22=plot("HHCombo080318.080318HH_HH_13TeV_080318HH_Systs_lephad_2HDM_BDT_{0}_obs", masses, color=R.kBlue, same=True, dorun1=False, dograviton=False, chan="lephad", legend="Exp 95% CL limit #tau_{lep}#tau_{had}", RSG=0, plot2=False)
    m,e33=plot("HHCombo080318.080318HH_HH_13TeV_080318HH_Systs_hadhad_2HDM_BDT_{0}_obs", masses, color=R.kRed, same=True, dorun1=False, dograviton=False, chan="hadhad", legend="Exp 95% CL limit #tau_{had}#tau_{had}", RSG=0, plot2=False)
    leg.Draw()
    leg1.Draw()
    c.Print("Limit_paper_2HDM_all.eps") 
    c.Print("Limit_paper_2HDM_all.pdf") 
    c.Print("Limit_paper_2HDM_all.C") 
    leg.Clear()
    leg2.Clear()


    masses = [260, 300, 400, 500, 600, 700, 800, 900, 1000]
    m,e111=plot("HHCombo080318.080318HH_HH_13TeV_080318HH_Systs_tautau_RSGc1_BDT_{0}_obs", masses, same=False, dorun1=False, dograviton=True, chan=chan, legend="Exp 95% CL limit", RSG=1, plot2=False)
    m,e222=plot("HHCombo080318.080318HH_HH_13TeV_080318HH_Systs_lephad_RSGc1_BDT_{0}_obs", masses, color=R.kBlue, same=True, dorun1=False, dograviton=False, chan="lephad", legend="Exp 95% CL limit #tau_{lep}#tau_{had}", RSG=1, plot2=False)
    m,e333=plot("HHCombo080318.080318HH_HH_13TeV_080318HH_Systs_hadhad_RSGc1_BDT_{0}_obs", masses, color=R.kRed, same=True, dorun1=False, dograviton=False, chan="hadhad", legend="Exp 95% CL limit #tau_{had}#tau_{had}", RSG=1, plot2=False)
    leg.Draw()
    leg2.Draw()
    c.Print("Limit_paper_RSGc1_all.eps") 
    c.Print("Limit_paper_RSGc1_all.pdf") 
    c.Print("Limit_paper_RSGc1_all.C") 
    leg.Clear()
    leg2.Clear()



    sys.exit()


    lambdas = range(-20, 21, 1)

    chan = "tautau"

    plot_lambda("HHLambda130418.250418LambdaHHLHSLT_HH_13TeV_250418LambdaHHLHSLT_Systs_tautau_SM{0}RW_BDT_0_obs", lambdas, legend = "Exp 95% CL limit", same=False, lambdaNLO = True)    
    leg.Draw()   
    legl.Draw()
    c.Print("limit_bbtt_Lambdas."+chan+".BDT.eps") 
    c.Print("limit_bbtt_Lambdas."+chan+".BDT.pdf") 
    c.Print("limit_bbtt_Lambdas."+chan+".BDT.C") 
    legl.Clear()
    leg.Clear()

    plot_lambda("HHLambda130418.250418LambdaHHLHSLT_HH_13TeV_250418LambdaHHLHSLT_Systs_tautau_SM{0}RW_BDT_0_obs", lambdas, legend = "Exp 95% CL limit", same=False, lambdaNLO = True)    
    l,e1=plot_lambda("HHLambda130418.250418Lambda_HH_13TeV_250418Lambda_Systs_hadhad_SM{0}RW_BDT_0_obs", lambdas, legend = "Exp 95% CL limit #tau_{had}#tau_{had}", same=True, color=R.kRed, lambdaNLO = True)
    l,e2=plot_lambda("HHLambdaSLT.250418Lambda_HH_13TeV_250418Lambda_Systs_lephad_SM{0}RW_BDT_0_obs", lambdas, legend = "Exp 95% CL limit #tau_{lep}#tau_{had}", same=True, color=R.kBlue, lambdaNLO = True)    
    legl.Draw()
    leg.Draw()
    c.Print("limit_bbtt_Lambdas."+chan+".BDT_Overlayed.eps")
    c.Print("limit_bbtt_Lambdas."+chan+".BDT_Overlayed.pdf")
    c.Print("limit_bbtt_Lambdas."+chan+".BDT_Overlayed.C") 
    legl.Clear()
    leg.Clear()
    #sys.exit()

    chan = "hadhad"
    
    plot_lambda("HHLambda130418.250418Lambda_HH_13TeV_250418Lambda_Systs_hadhad_SM{0}RW_BDT_0_obs", lambdas, legend = "Exp 95% CL limit", same=False, lambdaNLO = True)    
    #plot_lambda("HHLambda130418.250418Lambda_HH_13TeV_250418Lambda_StatOnly_hadhad_SM{0}RW_BDT_0_obs", lambdas, legend = "StatOnly", same=True, color=R.kBlue, lambdaNLO = True)    
    #plot_lambda("HHLambda130418.250418Lambda_HH_13TeV_250418Lambda_FloatOnly_hadhad_SM{0}RW_BDT_0_obs", lambdas, legend = "FloatOnly", same=True, color=R.kRed, lambdaNLO = True)    
    legl.Draw()
    leg.Draw()
    c.Print("limit_bbtt_Lambdas."+chan+".BDT.eps") 
    c.Print("limit_bbtt_Lambdas."+chan+".BDT.pdf") 
    c.Print("limit_bbtt_Lambdas."+chan+".BDT.C") 
    legl.Clear()
    leg.Clear()

    chan = "lephad"

    plot_lambda("HHLambdaSLT.250418Lambda_HH_13TeV_250418Lambda_Systs_lephad_SM{0}RW_BDT_0_obs", lambdas, legend = "Exp 95% CL limit", same=False, lambdaNLO = True)    
    #plot_lambda("HHLambdaSLT.250418Lambda_HH_13TeV_250418Lambda_StatOnly_lephad_SM{0}RW_BDT_0_obs", lambdas, legend = "StatOnly", same=True, color=R.kBlue, lambdaNLO = True)    
    #plot_lambda("HHLambdaSLT.250418Lambda_HH_13TeV_250418Lambda_FloatOnly_lephad_SM{0}RW_BDT_0_obs", lambdas, legend = "FloatOnly", same=True, color=R.kRed, lambdaNLO = True)    
    #plot_lambda("HHLambdaSLT.Test_HH_13TeV_Test_Systs_lephad_SM{0}RW_BDT_0_obs", lambdas, legend = "no sig unc", same=True, color=R.kMagenta, lambdaNLO = True)    
    leg.Draw()
    legl.Draw()
    c.Print("limit_bbtt_Lambdas."+chan+".BDT.eps") 
    c.Print("limit_bbtt_Lambdas."+chan+".BDT.pdf") 
    c.Print("limit_bbtt_Lambdas."+chan+".BDT.C") 
    legl.Clear()
    leg.Clear()

    sys.exit()
  



