from AnalysisMgr import WorkspaceConfig

def massvar():
    m = [251, 260, 280, 300, 325, 350, 375, 400, 450, 500, 550, 600, 700, 800, 900, 1000, 1100, 1200, 1400, 1600]
    mhi = dict(zip(m, m[1:] + [None]))
    mlo = dict(zip(m, [None] + m))
    return mlo, mhi

class WorkspaceConfig_HH(WorkspaceConfig):
    def __init__(self, *args, **kwargs):
        kwargs["Analysis"] = "HH"
        super(WorkspaceConfig_HH, self).__init__(*args, **kwargs)
        return


    def set_regions(self, channel = "lephad", signal = "", mass = None, topcr = False, non_res = False, lq3 = False, lamb = None, rw = False,
                    one_tag = False, fakecr=False, BDT=True, BDTCut=False, varmass = 0, zcr = False, plots=False, LTT=False, plot_region = "_LL_OS_"):

        self["Regions"] = []

        bdtcrs = False
        if plots == "BDTCRs":
            plots = False
            bdtcrs = True

        if plots:
            if channel == "lephad":
                vars = plots if isinstance(plots, list) else ["MET", "MtW", "dPhiHBB", "Mhh", "mbb", "METCent", "pTB2", "dPtLepTau", "dRbb", "DRTauTau", "mMMC"]
            elif channel == "hadhad":
                vars = plots if isinstance(plots, list) else ["dRBB", "DRTauTau", "mBB", "mHH", "mMMC"]
        #elif lq3:
        #    vars = ["MLQMin"]

        elif BDT:
            oldmass = mass
            if varmass == -1:
                mass = massvar()[0][int(mass)]
                print "Varied mass: %s -> %s" % (oldmass, mass)
            elif varmass == 1:
                mass = massvar()[1][int(mass)]
                print "Varied mass: %s -> %s" % (oldmass, mass)

            if isinstance(BDT, basestring):
                #var = "2HDM_BDT_" + str(mass)             
                vars = [BDT + "_BDT_" + str(mass)]
            else:
                if lamb is not None:
                    # Training for each value
                    #vars = ["SM{:02d}_BDT".format(lamb) if lamb >= 0 else "SMm{:02d}_BDT".format(abs(lamb))]
                    #if lamb == -1:
                    #    vars ="SMm1_BDT"
                    # Training for lambda=20
                    #vars = ["SM20_BDT"]
                    # Training for SM
                    #vars = ["SM_BDT"]   
                    if rw:
                        vars = ["SML20RW_BDT"] # finite top mass
                    else:
                        vars = ["SML20_BDT"]           
                elif "SMRW" in signal:
                    vars = ["SMRW_BDT"]      
                else:
                    if non_res:
                        vars= ["SMBDT"]
                       # vars = ["SMBDTPhHw7"]
                    else:
                        vars= [signal + "_PNN" + str(mass)]
 #vars = ["SM_BDT" if non_res else signal + "_BDT_" + str(mass)]

        elif BDTCut:
            vars = [BDTCut + "_mHH" + str(mass)]
            
        else:
            vars = ["mMMC" if non_res else "mHH"]

        for var in vars:
            if channel in ["lephad", "tautau"] :            
                if BDTCut:
                    self["Regions"] += [
                        "13TeV_TauLH_2tag2pjet_0ptv_" + var
                        ]
                else:
                    if non_res:
                        self["Regions"] += [
                            "13TeV_TauLH_2tag2pjet_0ptv" + (plot_region if plots else "_") + "SM_NN"
                        ]
                    else:
                        self["Regions"] += [
                            "13TeV_TauLH_2tag2pjet_0ptv" + (plot_region if plots else "_") + signal + "_PNN_" + str(mass)
                            ]

                if one_tag and non_res:
                    self["Regions"] += [
                        "13TeV_TauLH_1tag2pjet_0ptv" + (plot_region if plots else "_SR_") + var
                        ]

                if lq3:
                    self["Regions"] += [
                        "13TeV_TauLH_1tag2pjet_0ptv" + (plot_region if plots else "_SR_") + var
                        ]

                if bdtcrs:
                    self["Regions"] += [
                        "13TeV_TauLH_2tag2pjet_0ptv_SS_SR_" + var,                               
                        "13TeV_TauLH_1tag2pjet_0ptv_SR_" + var,                  
                        ]

                #if LTT and int(mass) != 900 and int(mass) != 1000:
                #print mass
                 #   self["Regions"] += [
                 #       "13TeV_TauLH_LTT_2tag2pjet_0ptv" + "_SR_" + var
                 #       ]

            if channel in ["hadhad", "tautau"] :
                if non_res:
                    self["Regions"] += [
                        #"13TeV_TauHH_2tag2pjet_0ptv" + (plot_region if plots else "_LL_OS_") + "SMBDT"
                        "13TeV_TauHH_2tag2pjet_0ptv" + (plot_region if plots else "_LL_OS_") + var
                    #"13TeV_TwoLepton_2tag2pjet_0ptv_ZllbbCR_mLL"

                     #   "13TeV_TauHH_2tag2pjet_0ptv" + (plot_region if plots else "_LL_OS_") + "SMNNNonNested"
                    ]
                else:
                    self["Regions"] += [
                        "13TeV_TauHH_2tag2pjet_0ptv" + (plot_region if plots else "_LL_OS_") + "PNN" + str(mass)
                        ]

                if one_tag and non_res:
                    self["Regions"] += [
                        "13TeV_TauHH_1tag2pjet_0ptv" + (plot_region if plots else "_SR_") + var
                        ]                       
                    
                if bdtcrs:
                    if non_res:
                        self["Regions"] += [
                        #"13TeV_TauHH_2tag2pjet_0ptv_BDTScoreControlTop_" + var,
                        #"13TeV_TauHH_2tag2pjet_0ptv_BDTScoreControlZJet_" + var,   
                            "13TeV_TauHH_2tag2pjet_0ptv_LL_SS_" + "SMBDT",                               
                            "13TeV_TauHH_1tag2pjet_0ptv_LL_OS_" + "SMBDT",                                                                         
                        ]
                    else:
                        self["Regions"] += [
                        #"13TeV_TauHH_2tag2pjet_0ptv_BDTScoreControlTop_" + var,
                        #"13TeV_TauHH_2tag2pjet_0ptv_BDTScoreControlZJet_" + var,   
                            "13TeV_TauHH_2tag2pjet_0ptv_LL_SS_" + "PNN" + str(mass),                               
                            "13TeV_TauHH_1tag2pjet_0ptv_LL_OS_" + "PNN" + str(mass),                                                                         
                        ]
                        

        if channel in ["lephad", "tautau"] :             
            if topcr:
                self["Regions"] += [
                    "13TeV_TauLH_2tag2pjet_0ptv_TopCRMjj_TauPt"
                    ]

            if zcr:
                self["Regions"] += [
                    "13TeV_TwoLepton_2tag2pjet_0ptv_ZllbbCR_mLL"
                ]

            #if LTT and int(mass) != 900 and int(mass) != 1000:
            if LTT:
                if non_res:
                #print mass
                    self["Regions"] += [
                        "13TeV_TauLHLTT_2tag2pjet_0ptv_"  + "SM_NN"
                    ]
                else:
                    self["Regions"] += [
                        "13TeV_TauLHLTT_2tag2pjet_0ptv_"  + (plot_region if plots else "_") + signal + "_PNN_" + str(mass)
                    ]

            
            if fakecr:
                self["Regions"] += [
                    "13TeV_TauLH_2tag2pjet_0ptv_TopCRMjj_SS_TauPt"
                    ]

        if channel == "hadhad":
            if zcr:
                self["Regions"] += [
                    "13TeV_TwoLepton_2tag2pjet_0ptv_ZllbbCR_mLL"
                ]

        
                    
        self.check_regions()
