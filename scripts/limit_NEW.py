import sys
import ROOT as R
from array import array

#chan = "lephad"
chan = "hadhad"
#chan = "tautau"

if chan == "lephad":
    inver = "HH022020LepHad"               
elif chan == "hadhad":
    inver = "HHv13HadHadNew2"
elif chan == "tautau":
    inver = "HHCombbtautau"       

exp = "obs"

chanmap = {"lephad" : 0.46, "hadhad" : 0.42, "tautau" : 0.46 + 0.42}

flambda = R.TF1("LHLambda", "pol2", -20, 20)
flambda.SetParameter(0,  0.0305647)
flambda.SetParameter(1,  -0.020403)
flambda.SetParameter(2,  0.00425099)

flambdaNLO = R.TF1("LHLambdaNLO", "pol2", -20, 20)
flambdaNLO.SetParameter(0,  0.0711561)
flambdaNLO.SetParameter(1,  -0.0476268)
flambdaNLO.SetParameter(2,  0.00991164)


def xsect(scale=False, G=0, non_res=False, chan="lephad", hdecay=False, stat=False, scale_lumi = 1, lamb = None, reweight = True, ZZ = False, lambdaNLO = False, ggFVBF=False):

    if chan == "lephad" or chan == "tautau":
        if ZZ:
            xsectstr ="361096     16.445          0.91       1.4307E-01      ZZ     Sherpa_CT10_ZqqZll_SHv21_improved"
            xs = float(xsectstr.split()[1]) * float(xsectstr.split()[2])
            if not stat:
                # Remove extra factor put in to help fit converge
                xs *= 1000

            return xs
                
        if non_res:
            if lamb is not None:
                xsectstr ="""
                345567   3.0565E-02	    1.0      0.02170188              hhttbb		   MGPy8EG_A14NNPDF23LO_hh_bbtt_lh_lambda00
                345568   1.4412E-02	    1.0      0.02238378              hhttbb		   MGPy8EG_A14NNPDF23LO_hh_bbtt_lh_lambda01
                345569   1.3229   	    1.0      0.02020361              hhttbb		   MGPy8EG_A14NNPDF23LO_hh_bbtt_lh_lambda20
                """

                if reweight and lambdaNLO:
                    # Quadratic fit to the NLO XS for lambda = 0,1,2,10
                    xs = flambdaNLO.Eval(lamb)
                elif reweight:
                    # Quadratic fit to the LO XS for lambda = 0,1,2,10
                    xs = flambda.Eval(lamb)
                else:
                    for line in xsectstr.split('\n'):
                        tok = line.split()
                        if not tok: continue 
                        l = int(tok[-1].split("_")[-1].replace("lambda", ""))
                        if (l != lamb): continue
                        xs = float(tok[1])

                if not stat:
                    # Remove extra factor put in to help fit converge
                    xs *= 1000

                if hdecay:
                    xs *= 1000* 0.5824 *0.06272 * 2 #bbtautau BR and * 1000 as fb

                return xs                

            else:
                xsectstr ="""
                345836   0.00226839      1.0           0.2892           hhttbb        aMcAtNloHerwig7EvtGen_UEEE5_CTEQ6L1_CT10ME_hh_ttbb_lh
                """

                #xs = float(xsectstr.split()[1]) / (0.5824* 0.06272*2) 
                if ggFVBF:
                    xs = (0.03105 + 0.001726) * 1000
                else:
                    xs = (0.03105) * 1000

            if not stat:
                # Remove extra factor put in to help fit converge
                xs *= 1000

            if hdecay:
                xs *= 1000* 0.5824 *0.06272 * 2 #bbtautau BR and * 1000 as fb
                    
            return xs


            
        if G == 0:
             xsectstr ="""            
            450131   0.073056256     1.0          0.22568             Hhhbbtautau251    MadGraphHerwig7EvtGen_PDF23LO_X251tohh_bbtautau_lephad
            450132   0.073056256     1.0          0.2274              Hhhbbtautau260    MadGraphHerwig7EvtGen_PDF23LO_X260tohh_bbtautau_lephad
            450133   0.073056256     1.0          0.23202             Hhhbbtautau280    MadGraphHerwig7EvtGen_PDF23LO_X280tohh_bbtautau_lephad
            450134   0.073056256     1.0          0.23822             Hhhbbtautau300    MadGraphHerwig7EvtGen_PDF23LO_X300tohh_bbtautau_lephad
            450135   0.073056256     1.0          0.24652             Hhhbbtautau325    MadGraphHerwig7EvtGen_PDF23LO_X325tohh_bbtautau_lephad
            450136   0.073056256     1.0          0.25584             Hhhbbtautau350    MadGraphHerwig7EvtGen_PDF23LO_X350tohh_bbtautau_lephad
             451927   0.073056256    1.0           0.26386           Hhhbbtautau375    MadGraphHerwig7EvtGen_PDF23LO_X375tohh_bbtautau_lephad
            450137   0.073056256     1.0          0.27144             Hhhbbtautau400    MadGraphHerwig7EvtGen_PDF23LO_X400tohh_bbtautau_lephad
            450138   0.073056256     1.0          0.28648             Hhhbbtautau450    MadGraphHerwig7EvtGen_PDF23LO_X450tohh_bbtautau_lephad
            450139   0.073056256     1.0          0.29918             Hhhbbtautau500    MadGraphHerwig7EvtGen_PDF23LO_X500tohh_bbtautau_lephad
            450140   0.073056256     1.0          0.3096              Hhhbbtautau550    MadGraphHerwig7EvtGen_PDF23LO_X550tohh_bbtautau_lephad
            450141   0.073056256     1.0          0.32044             Hhhbbtautau600    MadGraphHerwig7EvtGen_PDF23LO_X600tohh_bbtautau_lephad
            450142   0.073056256     1.0          0.3368              Hhhbbtautau700    MadGraphHerwig7EvtGen_PDF23LO_X700tohh_bbtautau_lephad
            450143   0.073056256     1.0          0.34894             Hhhbbtautau800    MadGraphHerwig7EvtGen_PDF23LO_X800tohh_bbtautau_lephad
            450144   0.073056256     1.0          0.36012             Hhhbbtautau900    MadGraphHerwig7EvtGen_PDF23LO_X900tohh_bbtautau_lephad
            450145   0.073056256     1.0          0.36882             Hhhbbtautau1000   MadGraphHerwig7EvtGen_PDF23LO_X1000tohh_bbtautau_lephad
            450161   0.073056256     1.0          0.37664             Hhhbbtautau1100   MadGraphHerwig7EvtGen_PDF23LO_X1100tohh_bbtautau_lephad 
            450162   0.073056256     1.0          0.38302             Hhhbbtautau1200   MadGraphHerwig7EvtGen_PDF23LO_X1200tohh_bbtautau_lephad 
            450164   0.073056256     1.0          0.39322             Hhhbbtautau1400   MadGraphHerwig7EvtGen_PDF23LO_X1400tohh_bbtautau_lephad
            450166   0.073056256     1.0          0.4012              Hhhbbtautau1600   MadGraphHerwig7EvtGen_PDF23LO_X1600tohh_bbtautau_lephad
            """
    elif chan == "hadhad":
        if ZZ:
            xsectstr ="361096     16.445          0.91       1.4307E-01      ZZ     Sherpa_CT10_ZqqZll_SHv21_improved"
            xs = float(xsectstr.split()[1]) * float(xsectstr.split()[2])
            if not stat:
                # Remove extra factor put in to help fit converge
                xs *= 1000

            return xs      

        if non_res:
            if lamb is not None:
                xsectstr ="""
                TODO
                """

                if reweight:
                    # Quadratic fit to the XS for lambda = 0,1,2,10
                    xs = flambda.Eval(lamb)

                else:
                    for line in xsectstr.split('\n'):
                        tok = line.split()
                        if not tok: continue 
                        l = int(tok[-1].split("_")[-1].replace("lambda", ""))
                        if (l != lamb): continue
                        xs = float(tok[1])

                if not stat:
                    # Remove extra factor put in to help fit converge
                    xs *= 1000

                if hdecay:
                    xs *= 1000* 0.5824 *0.06272 * 2

                return xs 

            else:
                xsectstr ="""
                345835   0.00226839     1.0           0.31212           hhttbb        aMcAtNloHerwig7EvtGen_UEEE5_CTEQ6L1_CT10ME_hh_ttbb_hh
                """
                
                #xs = float(xsectstr.split()[1]) / (0.5824*0.06272*2)

                if ggFVBF:
                    xs = (0.03105 + 0.001726) * 1000
                else:
                    xs = (0.03105) * 1000                             

                if not stat:
                    # Remove extra factor put in to help fit converge
                    xs *= 1000

                if hdecay:
                    xs *= 1000* 0.5824 *0.06272 * 2
                    
            return xs

            
        if G == 0:
            xsectstr ="""
            450146   0.073056256    1.0           0.23676             Hhhbbtautau251    MadGraphHerwig7EvtGen_PDF23LO_X251tohh_bbtautau_hadhad
            450147   0.073056256    1.0           0.24206             Hhhbbtautau260    MadGraphHerwig7EvtGen_PDF23LO_X260tohh_bbtautau_hadhad
            450148   0.073056256    1.0           0.25182             Hhhbbtautau280    MadGraphHerwig7EvtGen_PDF23LO_X280tohh_bbtautau_hadhad
            450149   0.073056256    1.0           0.2613             Hhhbbtautau300    MadGraphHerwig7EvtGen_PDF23LO_X300tohh_bbtautau_hadhad
            450150   0.073056256    1.0           0.27268             Hhhbbtautau325    MadGraphHerwig7EvtGen_PDF23LO_X325tohh_bbtautau_hadhad
            450151   0.073056256    1.0           0.28212             Hhhbbtautau350    MadGraphHerwig7EvtGen_PDF23LO_X350tohh_bbtautau_hadhad
            451928   0.073056256    1.0           0.2906            Hhhbbtautau375    MadGraphHerwig7EvtGen_PDF23LO_X375tohh_bbtautau_hadhad
            450152   0.073056256    1.0           0.298               Hhhbbtautau400    MadGraphHerwig7EvtGen_PDF23LO_X400tohh_bbtautau_hadhad
            450153   0.073056256    1.0           0.31084             Hhhbbtautau450    MadGraphHerwig7EvtGen_PDF23LO_X450tohh_bbtautau_hadhad
            450154   0.073056256    1.0           0.32078             Hhhbbtautau500    MadGraphHerwig7EvtGen_PDF23LO_X500tohh_bbtautau_hadhad
            450155   0.073056256    1.0           0.32972             Hhhbbtautau550    MadGraphHerwig7EvtGen_PDF23LO_X550tohh_bbtautau_hadhad
            450156   0.073056256    1.0           0.33566             Hhhbbtautau600    MadGraphHerwig7EvtGen_PDF23LO_X600tohh_bbtautau_hadhad
            450157   0.073056256    1.0           0.34722             Hhhbbtautau700    MadGraphHerwig7EvtGen_PDF23LO_X700tohh_bbtautau_hadhad
            450158   0.073056256    1.0           0.35612             Hhhbbtautau800    MadGraphHerwig7EvtGen_PDF23LO_X800tohh_bbtautau_hadhad
            450159   0.073056256    1.0           0.36266             Hhhbbtautau900    MadGraphHerwig7EvtGen_PDF23LO_X900tohh_bbtautau_hadhad
            450160   0.073056256    1.0           0.36946             Hhhbbtautau1000   MadGraphHerwig7EvtGen_PDF23LO_X1000tohh_bbtautau_hadhad
            450173   0.073056256    1.0           0.3737            Hhhbbtautau1100   MadGraphHerwig7EvtGen_PDF23LO_X1100tohh_bbtautau_hadhad
            450174   0.073056256    1.0           0.37772           Hhhbbtautau1200   MadGraphHerwig7EvtGen_PDF23LO_X1200tohh_bbtautau_hadhad
            450176   0.073056256    1.0           0.3844            Hhhbbtautau1400   MadGraphHerwig7EvtGen_PDF23LO_X1400tohh_bbtautau_hadhad
            450178   0.073056256    1.0           0.3893            Hhhbbtautau1600   MadGraphHerwig7EvtGen_PDF23LO_X1600tohh_bbtautau_hadhad

            """
            
    else:
        sys.exit("Unknown channel %s" % chan)

    BR = 1 #* 0.15/0.458  #0.577 * 0.0632 * 2 * 0.458

   

    result = {}
    for l in xsectstr.split('\n'):
        tok = l.split()
        if not tok: continue        
        if G:
            m = int(tok[-1].split("_")[-1].replace("M", ""))
        else:
            m = int(tok[-1].split("_")[2].replace("X", "").replace("tohh", ""))
        #xs = float(tok[1]) * BR
        xs = float(tok[1]) /(0.5824*0.06272*2) * 1000

        # Remove extra factor put in to help fit converge
        if G and not stat and m in [260]: xs *= 1000
        
        if hdecay:
            xs *= 0.5824 *0.06272 * 2
       

        result[m] = xs
    return result



def singlet(chan, hdecay = False):
    # From YR4 (arXiv:1610.07922v1), Table 66 

    m = array('f', [260, 275, 300, 325, 350, 400, 450, 500, 600, 700, 750, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500, 1600, 1800, 2000, 2250, 2500, 2750, 3000])
    xsect = [240.06, 268.80, 260.78, 248.87, 262.72, 242.67, 183.37, 137.85, 86.49, 63.49, 57.05, 52.49, 46.86, 43.81, 42.02, 40.91, 40.18, 39.70, 39.34, 39.08, 38.70, 38.44, 38.16, 37.88, 37.57, 37.22]
    
    xs = array('f', [])
    for x in xsect:        
        x /= 1000

        if hdecay:
            x *= 0.5824 * 0.06272 * 2

        #x *= chanmap[chan]
        xs.append(x)        

    return m, xs

def hMSSM(chan, hdecay = False, full = False):

    if full:
        m = array('f', [255.18899536132812, 263.406005859375, 271.88800048828125, 280.5769958496094, 289.4309997558594, 298.42001342773443, 307.5190124511719, 316.71099853515625, 325.98199462890625, 335.3190002441406, 344.71398925781256, 354.15899658203125, 363.64801025390625, 373.17498779296875, 382.73699951171875, 392.3290100097656, 401.947998046875, 411.59100341796875, 421.25698852539057, 430.9419860839844, 440.64599609375006, 450.3659973144532, 460.10198974609375, 469.8510131835937, 479.6130065917969, 489.3869934082031, 499.17199707031244, 508.9670104980469, 518.77099609375, 528.583984375, 538.405029296875, 548.2340087890625, 558.0700073242186, 567.9119873046875, 577.760986328125, 587.6160278320311, 597.4760131835939, 607.3419799804688, 617.2119750976562, 627.0869750976562, 636.9660034179688, 646.8499755859375, 656.7369995117188, 666.6290283203125, 676.5230102539062, 686.4219970703124, 696.3229980468751, 706.2269897460938, 716.135009765625, 726.0449829101564, 735.9580078125, 745.8729858398438, 755.791015625, 765.7109985351562, 775.6329956054688, 785.5579833984375, 795.4840087890625, 805.4130249023438, 815.3430175781249, 825.2750244140625, 835.2089843749999, 845.1439819335936, 855.0819702148438, 865.02001953125, 874.9600219726562, 884.9019775390626, 894.8449707031251, 904.7890014648439, 914.7349853515625, 924.6820068359375, 934.6300048828125, 944.5789794921876, 954.5289916992189, 964.4810180664062, 974.4329833984375, 984.3870239257812, 994.3410034179686, 1004.2999877929686])
        xsect = [2.2872371807131917, 2.8135428858130354, 2.9215654112092295, 2.8906712369536933, 2.8054091204316762, 2.7034821630650185, 2.602908596166884, 2.514955685492424, 2.4470240005567376, 2.409672418888676, 2.4083771240412943, 1.9498802666174695, 1.4485705947782037, 1.0959550297211182, 0.847550772577563, 0.6681776528477387, 0.5346459504526848, 0.4332700227029527, 0.3548567043626356, 0.29286567818315845, 0.24379598374009606, 0.20408960301405799, 0.17188582122841822, 0.1454816491718063, 0.12370115723079422, 0.1056092450466366, 0.09050928140664283, 0.07784758520685386, 0.06716917009575461, 0.05813820180397667, 0.05046511350204552, 0.0439154031994784, 0.03831678997224497, 0.03351450169939608, 0.029375675269573742, 0.025810061201359424, 0.02272358250067174, 0.02004280193352348, 0.01771427796753899, 0.01568353458748195, 0.013912591075002641, 0.01236099057157114, 0.011001313848811312, 0.009803389010304753, 0.008753583107483085, 0.007825357170110903, 0.0070044575965535205, 0.0062802243721207796, 0.005637019466532502, 0.005067572196792519, 0.004559658608984689, 0.004109667541201511, 0.00370626296178736, 0.003347301768424792, 0.0030265166682832967, 0.0027396457450396673, 0.002481797985951092, 0.0022511134199910414, 0.002044541670469246, 0.0018576591418176386, 0.0016902503749593456, 0.0015386079680755338, 0.0014026866721802977, 0.0012794058550784917, 0.0011675349681908307, 0.0010672075713680227, 0.0009760443852912037, 0.0008931683215429588, 0.0008177984873856126, 0.0007497039625211019, 0.0006877089859333268, 0.0006312409249280246, 0.000580176797999668, 0.0005332517943338541, 0.0004904563754298125, 0.00045141402832644603, 0.0004160860794064254, 0.00038353201583537366]
    else:
        m = array('f', [260, 275, 300, 325, 350, 400, 450, 500, 600, 700, 750, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500, 1600, 1800, 2000, 2250, 2500, 2750, 3000])
        xsect = [2.6742310387, 2.9190982626, 2.6859151204, 2.4535980702, 2.2189669757, 0.5593503107, 0.2054836430, 0.0893764994, 0.0428614977, 0.0220075085, 0.0067277170, 0.0023742485, 0.0009316554, 0.0003971562]
    
    xs = array('f', [])
    for x in xsect:        

        if hdecay:
            x *= 0.5824 *0.06272 * 2

        #x *= chanmap[chan]
        xs.append(x)        
    return m, xs


def ATLASLabel(suffix = "Internal", pos = [300,0.6], hdecay = True, lamb = False, plot2 = False):
#def ATLASLabel(suffix = "Preliminary", pos = [300,2], hdecay = True, lamb = False, plot2 = False):
#def ATLASLabel(suffix = "", pos = [300,2], hdecay = True, lamb = False, plot2 = False):
#def ATLASLabel(suffix = "Internal", pos = [300,2]):
    x = pos[0]
    y = pos[1]

    if plot2:
        #delx = 110
        delx = 210
    else:
        #delx = 110
        delx = 210
        
    y2 = 0.3
    
    if not hdecay:
        #y = 6
        #y2 = 3
        y = 3000
        y2 = 1800

    if lamb:
        x = -20
        y = 900
        y2 = 840
        delx = 7

    if plot2:
        y2 = y - 0.7

        #ts = 0.1
        ts = 0.07
    else:
        ts = 0.05
    
    l = R.TLatex()
    l.SetTextSize(ts)    
    l.SetTextFont(72)
    l.DrawLatex(x, y, "ATLAS")
    p = R.TLatex()
    p.SetTextFont(42)
    p.SetTextSize(ts)
    p.DrawLatex(x+delx, y, suffix)

    e = R.TLatex()
    e.SetTextFont(42)
    e.SetTextSize(ts)
    e.DrawLatex(x, y2, "13 TeV, 139 fb^{-1}")

    return l,p

def compareLines(line1, line2, lowMass, highMass):
    points = []

    for loopMass in range(lowMass,highMass):
        curMass = loopMass
        points.append(abs(line1.Eval(curMass)-line2.Eval(curMass)))

    print "Cross-over mass:", (lowMass+points.index(min(points)))

    return

def plot(name, masses, same=False, color=R.kRed, legend="exp (G, SLT, 10 pb)", chan="lephad", dosinglet = False, dograviton = False,
         dohMSSM=False, hdecay=False, dump=True, cont=False, scale_lumi = 1, minjection=None, RSG = None, noobs=False, plot2 = False, outputName="Limit", lastGraph=False):

    m = array('f', [])
    o = array('f', []);e = array('f', [])
    m2 = array('f', []);m1 = array('f', [])
    p1 = array('f', []);p2 = array('f', [])

    stat_only = "StatOnly" in name

    if RSG is None:
        RSG = 2 if 'G2' in legend else (1 if "G" in legend else 0)
    
    xs = xsect(scale=False, G=RSG, chan=chan, hdecay=hdecay, stat = stat_only, scale_lumi=scale_lumi)

    for mass in masses:
        if 'afs' in name:
            f = R.TFile.Open(name.format(mass) + "/" + str(mass) + ".root")
        else:
            f = R.TFile.Open("output/" + name.format(mass) + "/root-files/obs/" + str(mass) + ".root")

        if f:
            info = f.Get("limit")
        else:
            info = None
            
        if info:
            vo  = info.GetBinContent(1) * xs[mass] 
            if minjection:
            #if minjection == mass:                                
                print "Found injected mass", minjection
                ve  = info.GetBinContent(7) * xs[mass] 
            else:
                ve  = info.GetBinContent(2) * xs[mass] 
            vm1 = ve - info.GetBinContent(5) * xs[mass]
            vp1 = info.GetBinContent(4) * xs[mass] - ve
            vm2 = ve - info.GetBinContent(6) * xs[mass]
            vp2 = info.GetBinContent(3) * xs[mass] - ve
        else:
            vo = ve = vm1 = vp1 = vm2 = vp2 = 0

        m.append(mass)
        o.append(vo) #blind data
        #o.append(0)
        e.append(ve)
        m1.append(vm1)
        m2.append(vm2)
        p1.append(vp1)
        p2.append(vp2)

        if f:
            f.Close()

    if dump:
        print "#"*100
        print legend
        print "#"*100        

        print m
        print e
        print o

        for i, mass in enumerate(m):            
            print "{:4.0f} & {:7.4f} & {:7.4f} & {:7.4f} & {:7.4f} & {:7.4f} & {:7.4f} \\\\".format(mass, o[i], (e[i] - m2[i]), (e[i] - m1[i]), e[i], (e[i] + p1[i]), (e[i] + p2[i]))
    
    z = array('f', [0]*len(m))

    ge = R.TGraphAsymmErrors(len(m), m, e, z, z, z, z)
    if same:
        ge.SetLineColor(color); ge.SetMarkerColor(color);ge.SetMarkerStyle(10);ge.SetMarkerSize(0);    
        if chan == "tautau":
            ge.SetLineWidth(3);
        else:
            ge.SetLineWidth(2);
    else:
        ge.SetLineColor(R.kBlack); ge.SetMarkerColor(R.kBlack);ge.SetMarkerStyle(10); ge.SetMarkerSize(0);      
        if chan == "tautau":
            ge.SetLineWidth(3);
        else:
            ge.SetLineWidth(2);

    go = R.TGraphAsymmErrors(len(m), m, o, z, z, z, z)
    go.SetLineColor(R.kBlack);go.SetMarkerColor(R.kBlack);go.SetMarkerStyle(10); go.SetMarkerSize(0.8);    

    if not same:
        ge.SetLineStyle(2)

    #ge.SetLineStyle(2 if '10' in legend else 1)        
    
    g1 = R.TGraphAsymmErrors(len(m), m, e, z, z, m1, p1)
    g1.SetFillColor(R.kGreen)

    g2 = R.TGraphAsymmErrors(len(m), m, e, z, z, m2, p2)
    g2.SetFillColor(R.kYellow)

   

    ms, xs = singlet(chan, hdecay)
    gs = R.TGraphAsymmErrors(len(ms), ms, xs, z, z, z, z)
    gs.SetLineColor(R.kMagenta);gs.SetMarkerColor(R.kMagenta)

    mh, xh = hMSSM(chan, hdecay, full=True)
    gh = R.TGraph(len(mh), mh, xh)
    gh.SetLineColor(R.kMagenta);gh.SetMarkerColor(R.kMagenta); gh.SetMarkerStyle(10); gh.SetMarkerSize(0.8);    


    if dohMSSM:
        compareLines(gh,go,260,350)
        compareLines(gh,go,350,500)




    if RSG is None:        
        X = 'G_{kk}' if 'G_{kk}' in legend else 'X'
    else:
        X = 'G_{kk}' if RSG else 'X'


    if plot2:
        g = R.TMultiGraph("", "; Resonance Mass [GeV]; #sigma (%s #rightarrow HH #rightarrow bb#tau#tau) [pb]" % X)
    elif RSG ==1 or RSG == 2:
        g = R.TMultiGraph("", "; m(G_{kk}) [GeV]; #sigma (%s #rightarrow HH #rightarrow bb#tau#tau) [pb]" % X)
    else:
        #g = R.TMultiGraph("", "Limits for " + chan + (";m_{"+X+"} [GeV]; 95% CL Limit on #sigma_{"+X+"} x BR_{"+X+"#rightarrowhh#rightarrowbb#tau#tau} [pb]" if hdecay else
                                                  #";m_{"+X+"} [GeV]; 95% CL Limit on #sigma_{"+X+"} x BR_{"+X+"#rightarrowhh} [pb]"))
        #g = R.TMultiGraph("", "; Resonance Mass [GeV]; #sigma (%s #rightarrow hh #rightarrow bb#tau#tau) [pb]" % X)
        if hdecay:
            g = R.TMultiGraph("", "; m(X) [GeV]; #sigma (%s #rightarrow HH #rightarrow bb#tau#tau) [pb]" % X)
        else:
            g = R.TMultiGraph("", "; m(X) [GeV]; #sigma (%s #rightarrow HH) [fb]" % X)

    if not same:
        g.Add(g2, "E3")
        g.Add(g1, "E3")
        #if not noobs:
            #g.Add(go, "PL*")
        g.Add(go, "PL")

    #g.Add(ge, "PL*")
    g.Add(ge, "PL")
    if same:
        leg.AddEntry(ge, legend, "lp")
    else:
        if not noobs:
            leg.AddEntry(go, legend.replace("exp", "obs").replace("Exp", "Obs"), "lp")    
        leg.AddEntry(ge, legend, "lp")
        if plot2:
            leg.AddEntry(g1, "#pm 1#sigma", "f")
            leg.AddEntry(g2, "#pm 2#sigma", "f")
        else:
            leg.AddEntry(g1, "#pm 1#sigma", "f")
            leg.AddEntry(g2, "#pm 2#sigma", "f")
        
       

        if dosinglet:
            g.Add(gs, "C")
            leg.AddEntry(gs, "H singlet", "l")

        if dohMSSM:
            g.Add(gh, "C")
            if plot2 == 1:
                leg1.AddEntry(gh, "hMSSM Scalar (tan#beta = 2)", "l")
            else:
                leg1.AddEntry(gh, "hMSSM Scalar (tan#beta = 2)", "l")     

        if dograviton:
            g.Add(gg, "C")
            if plot2 == 2:
                leg2.AddEntry(gg, "Bulk RS Graviton (k/#bar{{M}}_{{pl}} = {0})".format("2.0" if RSG == 2 else "1.0"), "l")
            else:
                leg2.AddEntry(gg, "Bulk RS Graviton (k/#bar{{M}}_{{pl}} = {0})".format("2.0" if RSG == 2 else "1.0"), "l")    

    R.gPad.SetLogy(True)
    g.Draw("P" if same else "AP")

    if not same and not plot2 == 2:
        l = R.TLatex()
        if plot2:
            l = ATLASLabel(hdecay=hdecay, pos = [720, 1], plot2 = plot2)
        else:
            l = ATLASLabel(hdecay=hdecay)
        c._hists.append(l)

    if not same and plot2 == 1:
        l = R.TLatex()
        l.SetTextSize(0.07)    
        l.SetTextFont(42)
        #l.DrawLatex(300, 1, "Scalar")
    elif not same and plot2 == 2:
        l = R.TLatex()
        l.SetTextSize(0.07)    
        l.SetTextFont(42)
        #l.DrawLatex(300, 1, "Bulk RS")
    elif not plot2:
        l = R.TLatex()
        l.SetTextSize(0.05)    
        l.SetTextFont(42)
        #l.DrawLatex(300, 0.002, "Bulk RS")

    if not same:
        g.GetXaxis().SetLimits(240, 1610)
        if hdecay and plot2:            
            g.GetYaxis().SetRangeUser(1.1e-3, 3.5)
        elif hdecay:
            g.GetYaxis().SetRangeUser(3e-4, 1)
        else:
            g.GetYaxis().SetRangeUser(5., 5000)

    c._hists.append(g)
    c._hists.append(ge)

    #if lastGraph:

    leg.Draw()
        
    c.Print(outputName+".eps")
    #    g.Delete()


    return m, e


def ratio(m, exps, name="ratio.eps", bottom=False, title = "Ratio", legends = [], ymin = 0, ymax = 1.5, lamb = False):

    gm = R.TMultiGraph("", title + (";#lambda" if lamb else ";m_{X} [GeV]") + "; Ratio of expected limits")
    c = R.TCanvas()
    c._hists = []

    if bottom:
        leg = R.TLegend(0.58,0.20,0.89,0.49)
    else:
        leg = R.TLegend(0.2,0.80,0.89,0.89)
    leg.SetBorderSize(0)
    leg.SetFillColor(0)
    
    nom = exps[0]
    for i,e in enumerate(exps[1:]):
        ratio = array('f', [r[0]/r[1] if r[1] != 0 else 0 for r in zip(e,nom)])
        g = R.TGraph(len(m), m, ratio)
        g.SetLineColor(i+1)
        g.SetMarkerColor(i+1)
        gm.Add(g, "PL*")        
        c._hists.append(g)        
        leg.AddEntry(g, legends[i] if legends else "new/old", "l")

        gm.SetMinimum(ymin)
        gm.SetMaximum(ymax)
    gm.Draw("AP")
    leg.Draw()
    l = R.TLine(240, 1, 1600, 1)
    l.SetLineColor(1)
    l.SetLineStyle(2)

    l2u = R.TLine(240, 0.95, 1600, 0.95)
    l2u.SetLineColor(2)
    l2u.SetLineStyle(2)

    l2d = R.TLine(240, 1.05, 1600, 1.05)
    l2d.SetLineColor(2)
    l2d.SetLineStyle(2)    
    
    l.Draw()
    #l2u.Draw()
    #l2d.Draw()
    
    c.Print(name)

def non_res(name, same=False, chan="lephad", lamb = None, reweight=True, noprint = False, hdecay=False, label="", ZZ = False, lambdaNLO = False, ggFVBF=False):
    #f = R.TFile.Open("root-files/" + name + "/0.root")
    f = R.TFile.Open("output/" + name + "/root-files/obs/0.root")
    info = f.Get("limit")
    stat_only = "StatOnly" in name    
    xs = xsect(scale=False, non_res=True, chan=chan, stat=stat_only, hdecay=hdecay, lamb=lamb, reweight=reweight, ZZ = ZZ, lambdaNLO = lambdaNLO, ggFVBF=ggFVBF)
    vo  = info.GetBinContent(1) * xs
    #vo = 0
    ve  = info.GetBinContent(2) * xs 
    vm1 = info.GetBinContent(5) * xs
    vp1 = info.GetBinContent(4) * xs 
    vm2 = info.GetBinContent(6) * xs
    vp2 = info.GetBinContent(3) * xs 

    if ZZ:
        sm = 16.445 * 0.91
    else:
        if ggFVBF:
            if hdecay:
                sm = (31.05 + 1.726) * 0.5824 *0.06272 * 2
            else:
                sm = (31.05 + 1.726) / 1000. * 1000. # fb -> pb -> fb
        else:
            if hdecay:
                sm = 31.05 * 0.5824 *0.06272 * 2
            else:
                sm = 31.05 / 1000. * 1000. # fb -> pb -> fb

    if noprint:
        return vo, ve - vm2, ve - vm1, ve, vp1 - ve, vp2 - ve

    if not same:
        print "{:10s} {:10s} {:10s} {:10s} {:10s} {:10s} {:10s}".format("non-res", "obs", "-2sig", "-1sig", "exp", "+1sig", "+2sig")

    print label or name
    if hdecay:
        print "{:10s} {:10.2f} {:10.2f} {:10.2f} {:10.2f} {:10.2f} {:10.2f}".format("xs [fb]", vo, vm2, vm1, ve, vp1, vp2)
        print "{:10s} {:10.2f} {:10.2f} {:10.2f} {:10.2f} {:10.2f} {:10.2f}".format("xs/xs(SM)", vo/sm, vm2/sm, vm1/sm, ve/sm, vp1/sm, vp2/sm)
    else:
        print "{:10s} {:10.2f} {:10.2f} {:10.2f} {:10.17f} {:10.2f} {:10.2f}".format("xs [fb]", vo, vm2, vm1, ve, vp1, vp2)
        print "{:10s} {:10.2f} {:10.2f} {:10.2f} {:10.2f} {:10.2f} {:10.2f}".format("xs/xs(SM)", vo/sm, vm2/sm, vm1/sm, ve/sm, vp1/sm, vp2/sm)

    with open("limit.bbtt.SM."+chan+".BDT.txt", "a" if same else "w" ) as f:
        if not same:
            f.write("{:10s} {:10s} {:10s} {:10s} {:10s} {:10s} {:10s}\n".format("non-res", "obs", "-2sig", "-1sig", "exp", "+1sig", "+2sig"))
        f.write(label or name)
        f.write("\n")
        f.write("{:10s} {:10.2f} {:10.2f} {:10.2f} {:10.2f} {:10.2f} {:10.2f}\n".format("xs [pb]", vo, vm2, vm1, ve, vp1, vp2))
        f.write("{:10s} {:10.2f} {:10.2f} {:10.2f} {:10.2f} {:10.2f} {:10.2f}\n".format("xs/xs(SM)", vo/sm, vm2/sm, vm1/sm, ve/sm, vp1/sm, vp2/sm))

    return vo, ve - vm2, ve - vm1, ve, vp1 - ve, vp2 - ve

def plot_lambda(name, lambdas, hdecay = True, reweight=True, legend = "", color = R.kBlack, same = False, marker = False, lambdaNLO = False):

    lamb = array('f', [])
    o = array('f', []);e = array('f', [])
    m2 = array('f', []);m1 = array('f', [])
    p1 = array('f', []);p2 = array('f', [])

    for l in lambdas:
        if reweight:
            sl = "L{:02d}".format(l) if l >= 0 else "Lm{:02d}".format(abs(l))
        else:
            sl = "{:02d}".format(l) if l >= 0 else "m{:02d}".format(abs(l))        
        vo, vm2, vm1, ve, vp1, vp2 = non_res(name.format(sl), chan="lephad", lamb = l, reweight=reweight, hdecay = hdecay, noprint = True, lambdaNLO = lambdaNLO)

        lamb.append(l)
        o.append(vo*1000)
        e.append(ve*1000)
        m1.append(vm1*1000)
        m2.append(vm2*1000)
        p1.append(vp1*1000)
        p2.append(vp2*1000)

    z = array('f', [0]*len(lamb))
    ge = R.TGraphAsymmErrors(len(lamb), lamb, e, z, z, z, z)
    if same:
        ge.SetLineColor(color);ge.SetMarkerColor(color)  
    else:
        ge.SetLineColor(R.kBlack);ge.SetMarkerColor(R.kBlack)
        ge.SetLineStyle(2)

    go = R.TGraphAsymmErrors(len(lamb), lamb, o, z, z, z, z)
    go.SetLineColor(R.kBlack);go.SetMarkerColor(R.kBlack)

    g1 = R.TGraphAsymmErrors(len(lamb), lamb, e, z, z, m1, p1)
    g1.SetFillColor(R.kGreen)

    g2 = R.TGraphAsymmErrors(len(lamb), lamb, e, z, z, m2, p2)
    g2.SetFillColor(R.kYellow)

    g = R.TMultiGraph("", " ; #lambda/#lambda_{SM}; #sigma (HH #rightarrow bb#tau#tau) [fb]" if hdecay else
                      "  ; #lambda/#lambda_{SM}; #sigma (HH #rightarrow bb#tau#tau) [fb]")

    if not same:
        g.Add(g2, "E3")
        g.Add(g1, "E3")

    if marker:
        ge.SetMarkerStyle(10)
        g.Add(ge, "P")
    else:
        g.Add(ge, "PL")    
        g.Add(go, "PL")

    if same:
        legl.AddEntry(ge, legend, "p" if marker else "l")
    else:
        legl.AddEntry(ge, legend, "lp" if marker else "l")
        leg.AddEntry(go, legend.replace("exp", "obs").replace("Exp", "Obs"), "lp")

        leg.AddEntry(g1, "#pm 1#sigma", "f")
        leg.AddEntry(g2, "#pm 2#sigma", "f")

    g.Draw("P" if same else "AP")

    if not same:
        hXS = R.TH1F("", "hXS", 400, -20, 20)
        #hXS.Add(flambda , 33.41/(1.4412E-02)) # Add, scaling by lambda = 1 k-fact and converting to fb       
        hXS.Add(flambdaNLO, 1000.) 
        if hdecay:
            #hXS.Scale(0.577 * 0.0632 * 2)
            hXS.Scale(0.5825 *0.06272 * 2)

        hXS.SetLineColor(R.kRed+3)
        hXS.Draw("lsame")
        #legl.AddEntry(hXS, "Theory Prediction (#lambda = 1 k-fact)", "l")
        legl.AddEntry(hXS, "Theory Prediction (YR4 pol2)", "l")        
        c._hists.append(hXS)

    if not same:
        l = R.TLatex()        
        l = ATLASLabel(lamb = True)
        c._hists.append(l)

    c._hists.append(g)
    c._hists.append(ge)

    return lamb, e

    
if __name__ == '__main__':

    R.gROOT.SetBatch(True)

    

    chan = "hadhad"

    non_res("25052021PostProc.01062021_HH_13TeV_01062021_Systs_hadhad_SMggFVBF_BDT_0", chan="hadhad", hdecay=False, ggFVBF=True)
    non_res("25052021PostProc.01062021_HH_13TeV_01062021_StatOnly_hadhad_SMggFVBF_BDT_0", chan="hadhad", hdecay=False, ggFVBF=True)

    non_res("25052021PostProc.01062021_HH_13TeV_01062021_Systs_hadhad_SM_BDT_0", chan="hadhad", hdecay=False, ggFVBF=False)
    non_res("25052021PostProc.01062021_HH_13TeV_01062021_StatOnly_hadhad_SM_BDT_0", chan="hadhad", hdecay=False, ggFVBF=False)

    #sys.exit()


    #sys.exit()

    c = R.TCanvas()
    #c = R.TCanvas("", "", 600, 600)
    c._hists = []
    leg = R.TLegend(0.58,0.60,0.89,0.89)
    leg.SetBorderSize(0)
    leg.SetFillColor(0)

    legl = R.TLegend(0.15,0.50,0.40,0.7)
    legl.SetBorderSize(0)
    legl.SetFillColor(0)    


    R.gStyle.SetOptTitle(0)
    tsize = 0.06
    R.gStyle.SetLabelSize(tsize,"x")
    R.gStyle.SetTitleSize(tsize,"x")
    R.gStyle.SetLabelSize(tsize,"y")
    R.gStyle.SetTitleSize(tsize,"y")
    R.gStyle.SetLabelSize(tsize,"z")
    R.gStyle.SetTitleSize(tsize,"z")
    R.gStyle.SetTitleOffset(0.7, "y")
   

    masses = [251, 260, 280, 300, 325, 350, 375, 400, 450, 500, 550, 600, 700, 800, 900, 1000, 1100, 1200, 1400, 1600]
   
  
    leg = R.TLegend(0.58,0.55,0.89,0.89)    
    leg.SetBorderSize(0)
    leg.SetFillColor(0)
    leg2 = R.TLegend(0.12,0.1,0.4,0.26)    
    leg2.SetBorderSize(0)
    leg2.SetFillColor(0)
    leg2.SetFillStyle(0)
    leg2.SetTextSize(0.038)
    leg1 = R.TLegend(0.1,0.1,0.3,0.26)    
    leg1.SetBorderSize(0)
    leg1.SetFillColor(0)
    leg1.SetFillStyle(0)
    leg1.SetTextSize(0.038)
    tsize = 0.035
    R.gStyle.SetLabelSize(tsize,"x")
    R.gStyle.SetTitleSize(tsize,"x")
    R.gStyle.SetLabelSize(tsize,"y")
    R.gStyle.SetTitleSize(tsize,"y")
    R.gStyle.SetLabelSize(tsize,"z")
    R.gStyle.SetTitleSize(tsize,"z")
    R.gStyle.SetTitleOffset(1, "y")
    R.gStyle.SetTitleOffset(1.2, "x")

    chan = "hadhad"

    m,e1=plot("HHv13HadHadNew2.02032020_HH_13TeV_02032020_Systs_hadhad_2HDM_BDT_{0}", masses, same=False, dograviton=False, RSG=0, chan=chan, legend="Exp 95%", plot2=False, outputName="HadHad_v13_Syst")
   
   
    leg.Clear()

    m,e1=plot("HHv13HadHadNew2.02032020_HH_13TeV_02032020_StatOnly_hadhad_2HDM_BDT_{0}", masses, same=False, dograviton=False, RSG=0, chan=chan, legend="Exp 95%", plot2=False, outputName="HadHad_v13_Stat")
   
    leg.Clear()
   

    masses = [260, 300, 325, 350, 400, 450, 500, 550, 600, 700, 800, 900, 1000, 1100, 1200, 1400, 1600]

    chan = "lephad"

    m,e1=plot("HH022020LepHad.12032020LHNew_HH_13TeV_12032020LHNew_Systs_lephad_2HDM_BDT_{0}", masses, same=False, dograviton=False, RSG=0, chan=chan, legend="Exp 95%", plot2=False, outputName="LepHad_v13_Systs")
    m,e2=plot("HH022020LepHad.12032020LHSLTNew_HH_13TeV_12032020LHSLTNew_Systs_lephad_2HDM_BDT_{0}", masses, same=True, color=R.kRed, dograviton=False, RSG=0, chan=chan, legend="Exp 95% SLT", plot2=False, outputName="LepHad_v13_Systs")
    m,e3=plot("HH022020LepHadLTT.12032020LHLTTNew_HH_13TeV_12032020LHLTTNew_Systs_lephad_2HDM_BDT_{0}", masses, same=True, color=R.kBlue, dograviton=False, RSG=0, chan=chan, legend="Exp 95% LTT", plot2=False, outputName="LepHad_v13_Systs", lastGraph=True)

    leg.Clear() 
   

    m,e1=plot("HH022020LepHad.12032020LHNew_HH_13TeV_12032020LHNew_StatOnly_lephad_2HDM_BDT_{0}", masses, same=False, dograviton=False, RSG=0, chan=chan, legend="Exp 95%", plot2=False, outputName="LepHad_v13_Stat")
    m,e2=plot("HH022020LepHad.12032020LHSLTNew_HH_13TeV_12032020LHSLTNew_StatOnly_lephad_2HDM_BDT_{0}", masses, same=True, color=R.kRed, dograviton=False, RSG=0, chan=chan, legend="Exp 95% SLT", plot2=False, outputName="LepHad_v13_Stat")
    m,e3=plot("HH022020LepHadLTT.12032020LHLTTNew_HH_13TeV_12032020LHLTTNew_StatOnly_lephad_2HDM_BDT_{0}", masses, same=True, color=R.kBlue, dograviton=False, RSG=0, chan=chan, legend="Exp 95% LTT", plot2=False, outputName="LepHad_v13_Stat", lastGraph=True)

    leg.Clear()

    m,e1=plot("HHCombv13.13032020_HH_13TeV_13032020_StatOnly_tautau_2HDM_BDT_{0}", masses, same=False, dograviton=False, RSG=0, chan=chan, legend="Exp 95%", plot2=False, outputName="TauTau_v13_Stat")
    m,e2=plot("HHv13HadHadNew2.02032020_HH_13TeV_02032020_StatOnly_hadhad_2HDM_BDT_{0}", masses, same=True, color=R.kRed, dograviton=False, RSG=0, chan=chan, legend="Exp 95% HadHad", plot2=False, outputName="TauTau_v13_Stat")
    m,e3=plot("HH022020LepHad.12032020LHNew_HH_13TeV_12032020LHNew_StatOnly_lephad_2HDM_BDT_{0}", masses, same=True, color=R.kBlue, dograviton=False, RSG=0, chan=chan, legend="Exp 95% LepHad", plot2=False, outputName="TauTau_v13_Stat", lastGraph=True)

    leg.Clear()


    m,e1=plot("HHCombv13.13032020_HH_13TeV_13032020_Systs_tautau_2HDM_BDT_{0}", masses, same=False, dograviton=False, RSG=0, chan=chan, legend="Exp 95%", plot2=False, outputName="TauTau_v13_Stat")
    m,e2=plot("HHv13HadHadNew2.02032020_HH_13TeV_02032020_Systs_hadhad_2HDM_BDT_{0}", masses, same=True, color=R.kRed, dograviton=False, RSG=0, chan=chan, legend="Exp 95% HadHad", plot2=False, outputName="TauTau_v13_Stat")
    m,e3=plot("HH022020LepHad.12032020LHNew_HH_13TeV_12032020LHNew_Systs_lephad_2HDM_BDT_{0}", masses, same=True, color=R.kBlue, dograviton=False, RSG=0, chan=chan, legend="Exp 95% LepHad", plot2=False, outputName="TauTau_v13_Systs", lastGraph=True)

    leg.Clear()



    m,e1=plot("HHCombv13.13032020_HH_13TeV_13032020_StatOnly_tautau_2HDM_BDT_{0}", masses, same=False, dograviton=False, RSG=0, chan=chan, legend="Exp 95%", plot2=False, hdecay=True, outputName="TauTau_v13_Stat_hdecay")
    m,e2=plot("HHv13HadHadNew2.02032020_HH_13TeV_02032020_StatOnly_hadhad_2HDM_BDT_{0}", masses, same=True, color=R.kRed, dograviton=False, RSG=0, chan=chan, legend="Exp 95% HadHad", plot2=False, hdecay=True, outputName="TauTau_v13_Stat_hdecay")
    m,e3=plot("HH022020LepHad.12032020LHNew_HH_13TeV_12032020LHNew_StatOnly_lephad_2HDM_BDT_{0}", masses, same=True, color=R.kBlue, dograviton=False, RSG=0, chan=chan, legend="Exp 95% LepHad", plot2=False, hdecay=True, outputName="TauTau_v13_Stat_hdecay", lastGraph=True)

    leg.Clear()

    sys.exit()

 

    






 


 

 
   
   
   
 

    
 

 


 



 

 
   
  



