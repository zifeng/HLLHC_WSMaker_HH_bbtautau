import os, sys, argparse
from ROOT import TFile, TH1, TDirectory

"""
TODO
1) string match -> regex match
2) rename process
3) add norm
"""


class cHistogram:
    """
    old_name -> the name in original root file, object can be retrieved by f->Get(old_name)
    new_name -> the name that user defines, used to write to new root file by h->Clone(new_name)
    where -> where the histogram is store for cloning and saving to output later
    """
    def __init__(self, name):
        self._old = name
        self._new = name
        self._where = "file" # or "cache"

    # properties
    def old_name(self):
        return self._old

    def new_name(self):
        return self._new

    def where(self):
        return self._where

    # interface
    def setName(self, new_name):
        self._new = new_name
        return self

    def setStore(self, location):
        if location in {"file", "cache"}:
            self._where = location
        else:
            print "WARN cannot set to [{}], use default (file)".format(location)
        return self


class cWorkspaceInput:
    def __init__(self, input_path, output_path):
        self.bDebug = False
        self.bSaved = False
        
        # check existance
        self._in_file = TFile.Open(input_path, "READ")
        self._in_syst = self._in_file.Get("Systematics")
        self._out_path = output_path

        self._setNomInit = set()
        self._setSystInit = set()
        self._setNom = set()
        self._setSyst = set()
        
        # set of strings
        self._setSystTitleInit = set()
        
        # define new hists and save them here
        self._mapHistCaches = dict() 
        # look up table by process
        self._mapProcLoopUp = dict()

        # initilaisation
        self._initNominalHists()
        self._initSystHists()
        self._updateProcLoopUp()

        self._mapCommands = {
            "rename_syst": self._renameSyst,
            "remove_proc": self._removeProcess,
            "remove_syst": self._removeSyst,
            "remove_hist": self._removeHist,
            # to be added here and implemented below
        }
        self._lExecutions = list()

    def _initNominalHists(self):
        # initialise the set
        for key in self._in_file.GetListOfKeys():
            if isinstance(key.ReadObj(), TH1):
                if self.bDebug:
                    print "INFO NOMINAL Adding {}".format(key.GetName())
                self._setNom.add(cHistogram(key.GetName()))
        self._setNomInit = set(h for h in self._setNom)

    def _initSystHists(self):
        # initialise the set
        for key in self._in_syst.GetListOfKeys():
            if isinstance(key.ReadObj(), TH1):
                if self.bDebug:
                    print "INFO SYSTEMATICS Adding {}".format(key.GetName())
                self._setSyst.add(cHistogram(key.GetName()))
                self._setSystTitleInit.add(key.GetTitle().split('Sys')[1])
        self._setSystInit = set(h for h in self._setSyst)

    def _updateProcLoopUp(self):
        for h_nom in self._setNom:
            proc_name = h_nom.old_name()
            for h_syst in self._setSyst:
                if not h_syst.old_name().startswith(proc_name + "_Sys"):
                    continue
                if proc_name not in self._mapProcLoopUp.keys():
                    self._mapProcLoopUp[proc_name] = set()
                self._mapProcLoopUp[proc_name].add(h_syst.old_name())

    def _renameSyst(self, old, new):
        """
        operating on a set of cHistogram objects
        NOTE must provide the full name
        e.g. W_Sys[JET_EffectiveNP_Detector2__1down]
        """
        for h in self._setSyst:
            if h.old_name().endswith("Sys" + old):
                h.setName(h.old_name().replace(old, new, 1))
    
    def _removeProcess(self, proc_name):
        """
        operating on a set of cHistogram objects
        NOTE systematic histogram name matching hardcoded  
        """
        proc_to_remove = set(h for h in self._setNom if h.old_name() == proc_name)
        self._setNom = self._setNom - proc_to_remove
        
        syst_to_remove = set(h for h in self._setSyst if h.old_name().startswith(proc_name + "_Sys"))
        self._setSyst = self._setSyst - syst_to_remove

    def _removeSyst(self, syst_name):
        """
        operating on a set of cHistogram objects
        NOTE must provide the full name
        e.g. W_Sys[JET_EffectiveNP_Detector2__1down]
        """
        syst_to_remove = set(h for h in self._setSyst if h.old_name().endswith("Sys" + syst_name))
        self._setSyst = self._setSyst - syst_to_remove
    
    def _removeHist(self, hist_name):
        hist_to_remove = set(h for h in self._setSyst if h.old_name() == hist_name)
        self._setSyst = self._setSyst - hist_to_remove

    def _checkCommand(self, exe):
        if exe[0].startswith("rename") and len(exe[1:]) != 2:
            return False
        elif exe[0].startswith("remove") and len(exe[1:]) != 1:
            return False
        else:
            return True
    
    def _printSummary(self):
        setNomDiff = self._setNomInit - self._setNom
        setSystDiff = self._setSystInit - self._setSyst
        msgs =  ["RENAME [{}] -> [{}]".format(h.old_name(), h.new_name()) for h in self._setSyst if h.new_name() != h.old_name()]
        msgs += ["REMOVE PROC [{}]".format(h.old_name()) for h in setNomDiff]
        msgs += ["REMOVE HIST [{}]".format(h.old_name()) for h in setSystDiff]
        msgs += ["ADDED  HIST [{}]".format(h_name) for h_name in self._mapHistCaches.keys()]
        for m in sorted(msgs): print m

    # interfaces
    def setDebug(self, dbg):
        self.bDebug = dbg
        return self

    def nominalSet(self):
        return self._setNom

    def systematicsSet(self):
        return self._setSyst

    def systematicsList(self, sort=True):
        systs = set()
        for h in self._setSyst:
            syst_name = h.new_name().split('Sys')[1]
            systs.add(syst_name)
        if not sort:
            return systs
        return sorted(list(systs))

    def systematicsInitTitleList(self, sort=True):
        if not sort:
            return self._setSystTitleInit
        return sorted(list(self._setSystTitleInit))

    def lookUpProc(self, proc):
        return sorted([h.new_name() for h in self._setSyst if proc in h.new_name()])

    def addCommand(self, command, *args):
        if command not in self._mapCommands.keys():
            raise RuntimeError("command called [{}] is not defined".format(command))
        
        self._lExecutions.append((command,) + args)
        return self

    def execute(self):
        """
        list of commands 
        """
        for exe in self._lExecutions:
            if not self._checkCommand(exe):
                print "WARN wrong number of argument, skipping command {}".format(exe)
                continue
            self._mapCommands[exe[0]](*(exe[1:]))
        if self.bDebug:
            self._printSummary()
        return self

    def save(self):
        """
        TODO refactor this
        """
        if self.bSaved:
            return self

        out_file = TFile.Open(self._out_path, "RECREATE")
        out_file.cd()
        for proc in self._setNom:
            h = None
            if proc.where() == "file":
                h = self._in_file.Get(proc.old_name()).Clone(proc.new_name())
            elif proc.where() == "cache":
                h = self._mapHistCaches[proc.old_name()].Clone(proc.new_name())
            h.SetDirectory(0)
            h.Write()
        syst_dir = out_file.mkdir("Systematics")
        syst_dir.cd()
        for syst in self._setSyst:
            h = None
            if syst.where() == "file":
                h = self._in_syst.Get(syst.old_name()).Clone(syst.new_name())
            elif syst.where() == "cache":
                h = self._mapHistCaches[syst.old_name()].Clone(syst.new_name())
            h.SetDirectory(0)
            h.Write()
        out_file.cd()
        out_file.Close()
        self.bSaved = True
        print "-> saved in {}".format(self._out_path)
        return self
