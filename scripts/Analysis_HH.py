#!/usr/bin/env python
# For combined need to scale both to same input CR first

import sys
import os
import AnalysisMgr_HH as Mgr
import BatchMgrOld as Batch

lq3  = False
#chan = "lephad"
chan = "hadhad"
#chan = "tautau"

if chan == "lephad":
    if lq3:
        InputVersion = "LQ270917" # lephad    
        InputVersion = "LQ101016" # lephad
    else:
        #InputVersion = "HHSLT080318"
        #InputVersion = "HHLTT080318"
        #InputVersion = "HHCombo080318"
        #InputVersion = "LepHadTopCR"
        #InputVersion = "HHLambdaSLT"
        InputVersion = "HHLambda130418"
        
elif chan == "hadhad": 
      #InputVersion = "HHCombo080318"
      #InputVersion = "HHTopCR"
      #InputVersion = "HHZCR"
      #InputVersion = "HHTopCRNew"
      #InputVersion = "HHZCRNew"
      #InputVersion = "HHLambda130418"#Final Lambda ws
    #InputVersion = "ZZ020818"
    #InputVersion = "HHv15HadHad18032021"
    InputVersion = "HadHad17062021"

elif chan == "tautau":
    #InputVersion = "HHCombo080318"
    #InputVersion = "HHLambda130418"   
    InputVersion = "HHComb26032021"
    
def add_config(mass, unc = "Systs", mcstats=True, one_bin=False, signal = "RSG", chan="lephad", BDT = False, BDTCut=False, topcr = True, one_tag=False, fakecr=False, zcr = False,
               plots = False, plot_region = "_BDTVarsPreselection", LTT=False, reweight = False,  lamb = None, ggFVBF = False, VBF = False, k2V = None, SMXSecLimit=False):

    sigtype = signal

    if ggFVBF:
        sigtype += "ggFVBF"

    if VBF:
        sigtype += "VBF"


    if lamb is not None:
        # Reweighted
        sigtype += "l{:02d}".format(lamb) if lamb >= 0 else "lm{:02d}".format(abs(lamb))
    
        if reweight:
            sigtype += "RW" #finite top
        # Non-reweighted samples
        #sigtype += "{:02d}".format(lamb) if lamb >= 0 else "m{:02d}".format(abs(lamb))       


    elif reweight:
        sigtype +="SMRW"

    if k2V is not None:
        sigtype += "c2v{:02d}".format(k2V) if isinstance(k2V, int) else "c2v"+(str(k2V).replace(".", "p"))

       
    conf = Mgr.WorkspaceConfig_HH(unc, InputVersion=InputVersion, Type = sigtype, MassPoint=mass, OneBin=one_bin, DoInjection=0,
                                  UseStatSystematics=mcstats, StatThreshold=0.00, Debug=True, DoShapePlots=True, DoSystsPlots=True, LogScalePlots=True,
                                  ShowMCStatBandInSysPlot = True, SmoothingControlPlots = True, PruneAfterMerge= True, SmoothAfterMerge = True, Postfit = True if plots else False, LTT = LTT, ggFVBF= ggFVBF, VBF = VBF, SMXSecLimit=SMXSecLimit)
    #conf.set_channels(channel)
    if signal == "LQ3": topcr = False
    topcr = topcr and not BDT
    conf.set_regions(channel = chan, signal = signal, mass = mass, topcr = topcr, non_res = (signal == "SM" or signal == "ZZ"), lq3 = (signal == "LQ3"), one_tag = one_tag, fakecr = fakecr,
                     BDT = BDT, BDTCut = BDTCut, zcr = zcr, plots = plots, plot_region = plot_region, LTT = LTT, lamb = lamb, rw = (reweight and lamb is not None))
    fullversion = "HH_13TeV_" + outversion + "_" + unc + "_" + chan + "_" + sigtype
    if BDT:
        fullversion += "_BDT"        
        if isinstance(BDT, basestring): fullversion += BDT
    if BDTCut: fullversion += "_" + BDTCut
    fullversion += "_" + mass
    return conf.write_configs(fullversion)

if __name__ == "__main__":

    if len(sys.argv) is not 2:
        print "Usage: Analysis_HH.py <outversion>"
        exit(0)

    outversion = sys.argv[1]

    # What to run ...
    dolimits = True
    dotables = False
    dopostfit = False # of fit vars
    dopulls = False
    doranking = False
    #plotreg = "_ControlTau_"
    doplots = False #"BDTCRs"
    #if chan == "lephad":
    #    plotreg = "_BDTVarsPreselection_"
    #elif chan == "hadhad":
    #    plotreg = "_SR_"        
        #plotreg = "_Preselection_"        
    #else:
    #    plotreg = "_SR_"        

    #doplots = "BDTCRs"
    #doplots = ["Tau0Pt", "Tau1Pt", "SS_Tau0Pt", "SS_Tau1Pt"]
    #doplots = True
    
    dofcc = False

    # Fit setup ...
    dozcr = True
    dobdt = True 
    doLTT = True

    dozz = False

    # For running on existing workspaces (-k option):
    #WSlink = "/afs/cern.ch/user/g/gwilliam/workspace/WSMaker/workspaces/MonoH0708152p.180815_MonoH_13TeV_180815_Systs_Scalar_2000/combined/2000.root" 
    #os.system("ls -lh " + WSlink)

    ########## create config files

    print "Adding config files..."
    configfiles_stat = []
    configfiles = []

    if lq3:
        masses = [200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500]
    else:
        #masses2 = [260, 300, 400, 500, 600, 700, 800, 900, 1000]
        #masses = [251, 260, 280, 300, 325, 350, 375, 400, 450, 500, 550, 600, 700, 800, 900, 1000, 1100, 1200, 1400, 1600]

        masses = []

        #lambdas = xrange(-20, 21, 1)
        #lambdas = [-5, 1, 10, 0, 2, 5]
        #lambdas = [3]
        lambdas = []

    if not dolimits:
        masses = [300, 500, 1000]

    #masses = []
    
    # Can be difficult to converge unless multiply small signal
    # (done in systematicslistbuilder_hh but only for syst/float limits -> hack to use FloatOnly here and add a return in the function)

    if dozz:
        configfiles_stat += add_config("0", "StatOnly", signal="ZZ", chan=chan, BDT=dobdt, zcr=dozcr, plots= doplots, LTT=False)
        configfiles += add_config("0", "FloatOnly", signal="ZZ", chan=chan, BDT=dobdt, zcr=dozcr, plots= doplots, LTT=False)
    else:
        if not lq3:
            configfiles += add_config("0", "Systs", signal="SM", chan=chan, BDT = dobdt, zcr = dozcr, plots = doplots, LTT = doLTT, ggFVBF = True, SMXSecLimit=False)    
            #configfiles += add_config("0", "Systs", signal="SMRW", chan=chan, BDT = dobdt, zcr = dozcr, plots = doplots, LTT = doLTT)     
            if dolimits:
                configfiles_stat += add_config("0", "StatOnly", signal="SM", chan=chan, BDT = dobdt, zcr = dozcr, plots = doplots, LTT = doLTT, ggFVBF = True, SMXSecLimit=False)  
                #configfiles_stat += add_config("0", "StatOnly", signal="SMRW", chan=chan, BDT = dobdt, zcr = dozcr, plots = doplots, LTT = doLTT)  
           

            for l in lambdas:
                configfiles += add_config("0", "Systs", signal="SM", chan=chan, BDT = dobdt, zcr = dozcr, plots = doplots, LTT = False, lamb = l, reweight = True)
                configfiles_stat += add_config("0", "StatOnly", signal="SM", chan=chan, BDT = dobdt, zcr = dozcr, plots = doplots, LTT = False, lamb = l, reweight = True)


    for m in masses:
        if lq3:
            configfiles      += add_config(str(m), "Systs", signal="LQ3", chan=chan, BDT = True, zcr = dozcr, plots = doplots, LTT = False, one_tag = False)  
            if dolimits:
                configfiles_stat += add_config(str(m), "FloatOnly", signal="LQ3", chan=chan,  BDT = True, zcr = dozcr, plots = doplots, LTT = False, one_tag = False)  
                configfiles_stat += add_config(str(m), "StatOnly", signal="LQ3", chan=chan,  BDT = True, zcr = dozcr, plots = doplots, LTT = False, one_tag = False)  
        else:
            #configfiles += add_config(str(m), "Systs", signal="RSGc1", chan=chan, BDT = dobdt, zcr = dozcr, one_tag= False, plots = doplots, LTT = doLTT)
            configfiles += add_config(str(m), "Systs", signal="2HDM", chan=chan, BDT = dobdt, zcr = dozcr, one_tag= False, plots = doplots, LTT = doLTT)   
            #configfiles += add_config(str(m), "Systs", signal="RSGc2", chan=chan, BDT = dobdt, zcr = dozcr, one_tag= False, plots = doplots, LTT = doLTT)    

            if dolimits:
               # configfiles_stat += add_config(str(m), "StatOnly", signal="RSGc1", chan=chan, BDT = dobdt, zcr = dozcr, plots = doplots, LTT = doLTT)
                configfiles_stat += add_config(str(m), "StatOnly", signal="2HDM", chan=chan, BDT = dobdt, zcr = dozcr, plots = doplots, LTT = doLTT)
                #configfiles_stat += add_config(str(m), "StatOnly", signal="RSGc2", chan=chan, BDT = dobdt, zcr = dozcr, plots = doplots, LTT = doLTT)
             

    for fi in configfiles_stat:
        print fi
    for fi in configfiles:
        print fi

    #sys.exit()

    ########## define tasks

    tasks = ["-w"]             # create workspace (do always)
    try:
        tasks += ["-k", WSlink]    # link workspace instead of creating one (requires -w)
    except NameError:
        pass

    if dolimits:
        tasks += ["-l", "0,{MassPoint}"]        
        #tasks += ["-l", "1,{MassPoint}"]       

    if dotables or dopostfit or dofcc:
        tasks += ["--fcc", "4,10@{MassPoint}"]

    if dotables:
        tasks += ["-t", "1@{MassPoint}"]

    if dopostfit:
         tasks += ["-p", "1@{MassPoint}"]
         
    if dopulls:
        tasks += ["--fcc", "2,7@{MassPoint}"]
        # Then run comparePulls script

    if doranking:
        tasks += ["-r", "{{MassPoint}}"]
        tasks += ["-n", "{MassPoint}"]        

#     tasks += ["-s", 0]         # significance
#    tasks += ["--fcc", "2,8@{MassPoint}"]  # fitCrossChecks
#     tasks += ["-u", 0]         # breakdown of muhat errors stat/syst/theory
#     # requires fitCrossChecks:
#     tasks += ["-m", "7,2"]     # reduced diag plots (quick)
#     tasks += ["-b", "2"]       # unfolded b-tag plots (quick)
#     tasks += ["-p", "2,0"]       # post-fit plots (CPU intense)
#     tasks += ["-t", "0,2"]     # yield tables (CPU intense)

    ########## submit jobs

    # in case you want to run something locally
    Batch.run_local_batch(configfiles, outversion, tasks)    
    Batch.run_local_batch(configfiles_stat, outversion, tasks)
    sys.exit()
    #Batch.run_local_batch(configfiles_stat, outversion, ["-w", "-l", 1])

    # adjust to recover failed ranking subjobs
    redoSubJob = -1

    if redoSubJob < 0:
        print "Submitting stat only jobs..."
        Batch.run_lxplus_batch(configfiles_stat, outversion, ["-w", "-l", 1,"-s",1], '2nd')
        print "Submitting systs jobs..."
        Batch.run_lxplus_batch(configfiles, outversion, tasks, '2nd')
    print "Submitting rankings..."
    Batch.run_lxplus_batch(configfiles, outversion, ["-w", "-r", "125"], '2nd', jobs=20, subJob=redoSubJob)
    #Batch.run_lxplus_batch(configfiles, outversion, ["-w", "-k", WSlink, "-r", "125"], '2nd', jobs=20, subJob=redoSubJob)

    ########## non-default stuff. Warning: don't submit rankings / toys / NLL scans with the same outversion and nJobs!
    #print "Submitting toys..."
    #Batch.run_lxplus_batch(configfiles, outversion, ["-w", "-k", WSlink, "--fcc", "6"], '2nd', jobs=50, subJob=redoSubJob)
    #print "Submitting NLL scans..."
    #Batch.run_lxplus_batch(configfiles, outversion, ["-w", "-k", WSlink, "--fcc", "7,2,doNLLscan"], '2nd', jobs=50, subJob=redoSubJob)
