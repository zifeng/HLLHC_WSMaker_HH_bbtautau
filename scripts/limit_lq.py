import sys
import ROOT as R
from array import array
from collections import OrderedDict as odict

chan = "lephad"
#chan = "hadhad"
#chan = "tautau"

if chan == "lephad":
    inver = "LQ200917" # lephad
elif chan == "hadhad":
    inver = ""
elif chan == "tautau":
    inver = ""

def br_to_btau(m_lq, beta, lamb=0.3):
    m_b = 4.7
    m_tau=1.777
    return (m_lq**2 - m_b**2 - m_tau**2)*R.TMath.Sqrt(m_lq**4 + m_b**4 + m_tau**4 - 2*(m_lq**2*m_b**2 + m_lq**2*m_tau**2 + m_b**2*m_tau**2))*3.0*lamb**2*beta/(48*R.TMath.Pi()*m_lq**3)

def reweight_beta1(mass, br):
    weight = br #(br_to_btau(mass, beta=1.0)/br_to_btau(mass, beta=br))
    return 1/weight**2

def xsect(scale=False, chan="lephad", decay=False, stat=True, br = None, xstype = "old", scale1000 = False, rw = False):

    result = {}

    if xstype == "new":
        result = {
            200 : 64.5085,
            205 : 57.2279,
            210 : 50.9226,
            215 : 45.3761,
            220 : 40.5941,
            225 : 36.3818,
            230 : 32.6679,
            235 : 29.3155,
            240 : 26.4761,
            245 : 23.8853,
            250 : 21.5949,
            255 : 19.5614,
            260 : 17.6836,
            265 : 16.112,
            270 : 14.6459,
            275 : 13.3231,
            280 : 12.1575,
            285 : 11.0925,
            290 : 10.1363,
            295 : 9.29002,
            300 : 8.51615,
            305 : 7.81428,
            310 : 7.17876,
            315 : 6.60266,
            320 : 6.08444,
            325 : 5.60471,
            330 : 5.17188,
            335 : 4.77871,
            340 : 4.41629,
            345 : 4.08881,
            350 : 3.78661,
            355 : 3.50911,
            360 : 3.25619,
            365 : 3.02472,
            370 : 2.8077,
            375 : 2.61162,
            380 : 2.43031,
            385 : 2.26365,
            390 : 2.10786,
            395 : 1.9665,
            400 : 1.83537,
            405 : 1.70927,
            410 : 1.60378,
            415 : 1.49798,
            420 : 1.39688,
            425 : 1.31169,
            430 : 1.22589,
            435 : 1.14553,
            440 : 1.07484,
            445 : 1.01019,
            450 : 0.948333,
            455 : 0.890847,
            460 : 0.836762,
            465 : 0.787221,
            470 : 0.740549,
            475 : 0.697075,
            480 : 0.655954,
            485 : 0.618562,
            490 : 0.582467,
            495 : 0.549524,
            500 : 0.51848,
            505 : 0.489324,
            510 : 0.462439,
            515 : 0.436832,
            520 : 0.412828,
            525 : 0.390303,
            530 : 0.368755,
            535 : 0.348705,
            540 : 0.330157,
            545 : 0.312672,
            550 : 0.296128,
            555 : 0.280734,
            560 : 0.266138,
            565 : 0.251557,
            570 : 0.238537,
            575 : 0.226118,
            580 : 0.214557,
            585 : 0.203566,
            590 : 0.193079,
            595 : 0.183604,
            600 : 0.174599,
            605 : 0.166131,
            610 : 0.158242,
            615 : 0.150275,
            620 : 0.142787,
            625 : 0.136372,
            630 : 0.129886,
            635 : 0.123402,
            640 : 0.11795,
            645 : 0.112008,
            650 : 0.107045,
            655 : 0.102081,
            660 : 0.09725,
            665 : 0.0927515,
            670 : 0.0885084,
            675 : 0.0844877,
            680 : 0.0806192,
            685 : 0.0769099,
            690 : 0.0734901,
            695 : 0.0701805,
            700 : 0.0670476,
            705 : 0.0641426,
            710 : 0.0612942,
            715 : 0.0585678,
            720 : 0.0560753,
            725 : 0.0536438,
            730 : 0.0513219,
            735 : 0.0491001,
            740 : 0.0470801,
            745 : 0.045061,
            750 : 0.0431418,
            755 : 0.0413447,
            760 : 0.0396264,
            765 : 0.0379036,
            770 : 0.0363856,
            775 : 0.0348796,
            780 : 0.0334669,
            785 : 0.0320548,
            790 : 0.0307373,
            795 : 0.0295348,
            800 : 0.0283338,
            805 : 0.0272206,
            810 : 0.0261233,
            815 : 0.0251107,
            820 : 0.0241099,
            825 : 0.0230866,
            830 : 0.0221834,
            835 : 0.0213766,
            840 : 0.0204715,
            845 : 0.0197653,
            850 : 0.0189612,
            855 : 0.0182516,
            860 : 0.0175509,
            865 : 0.0168336,
            870 : 0.0162314,
            875 : 0.015625,
            880 : 0.0150143,
            885 : 0.0144112,
            890 : 0.0138979,
            895 : 0.0133962,
            900 : 0.0128895,
            905 : 0.0123843,
            910 : 0.0119837,
            915 : 0.0114713,
            920 : 0.0110688,
            925 : 0.0106631,
            930 : 0.0102629,
            935 : 0.0098874,
            940 : 0.00952142,
            945 : 0.00916636,
            950 : 0.00883465,
            955 : 0.00851073,
            960 : 0.00820884,
            965 : 0.00791403,
            970 : 0.00763112,
            975 : 0.00735655,
            980 : 0.00710317,
            985 : 0.00684867,
            990 : 0.00660695,
            995 : 0.00637546,
            1000 : 0.00615134,
            1005 : 0.00593765,
            1010 : 0.00572452,
            1015 : 0.00553094,
            1020 : 0.00533968,
            1025 : 0.00514619,
            1030 : 0.00497235,
            1035 : 0.00479906,
            1040 : 0.00463806,
            1045 : 0.00447537,
            1050 : 0.00432261,
            1055 : 0.00417983,
            1060 : 0.00403886,
            1065 : 0.0038962,
            1070 : 0.00376343,
            1075 : 0.00364174,
            1080 : 0.00352093,
            1085 : 0.00339813,
            1090 : 0.00328695,
            1095 : 0.00317628,
            1100 : 0.00307413,
            1105 : 0.00297377,
            1110 : 0.00287148,
            1115 : 0.00278078,
            1120 : 0.00268873,
            1125 : 0.00260821,
            1130 : 0.00251529,
            1135 : 0.00243484,
            1140 : 0.00236295,
            1145 : 0.00228192,
            1150 : 0.00221047,
            1155 : 0.00213907,
            1160 : 0.00206845,
            1165 : 0.0020063,
            1170 : 0.00194569,
            1175 : 0.0018741,
            1180 : 0.00182266,
            1185 : 0.00176211,
            1190 : 0.00170006,
            1195 : 0.00164968,
            1200 : 0.00159844,
            1205 : 0.0015472,
            1210 : 0.00149657,
            1215 : 0.00145544,
            1220 : 0.00140288,
            1225 : 0.00136155,
            1230 : 0.00131271,
            1235 : 0.0012717,
            1240 : 0.00123066,
            1245 : 0.00119994,
            1250 : 0.0011583,
            1255 : 0.00112694,
            1260 : 0.00108716,
            1265 : 0.00105517,
            1270 : 0.00102241,
            1275 : 0.000991293,
            1280 : 0.000961012,
            1285 : 0.000932394,
            1290 : 0.000903404,
            1295 : 0.000876957,
            1300 : 0.000850345,
            1305 : 0.00082443,
            1310 : 0.00079983,
            1315 : 0.000775222,
            1320 : 0.000751372,
            1325 : 0.000728912,
            1330 : 0.000706867,
            1335 : 0.000685372,
            1340 : 0.000664649,
            1345 : 0.000644804,
            1350 : 0.000625155,
            1355 : 0.000606802,
            1360 : 0.000588512,
            1365 : 0.000570506,
            1370 : 0.000553379,
            1375 : 0.000536646,
            1380 : 0.000521404,
            1385 : 0.000505008,
            1390 : 0.000490353,
            1395 : 0.000476164,
            1400 : 0.000461944,
            1405 : 0.000448172,
            1410 : 0.000435082,
            1415 : 0.000422967,
            1420 : 0.000410381,
            1425 : 0.000398106,
            1430 : 0.000386792,
            1435 : 0.000375724,
            1440 : 0.000364616,
            1445 : 0.000353965,
            1450 : 0.000343923,
            1455 : 0.000333885,
            1460 : 0.000324344,
            1465 : 0.0003153,
            1470 : 0.00030583,
            1475 : 0.000296811,
            1480 : 0.000288149,
            1485 : 0.000279711,
            1490 : 0.000271724,
            1495 : 0.000264275,
            1500 : 0.000256248
            }

    elif chan == "lephad" or chan == "tautau":

        #  XS below are for generating hh -> allall             305148    0.586E-02  1.0      1.0       LQ3btaubtau1000  Py8EG_A14NNPDF23LO_LQ3LQ3_1000_btaubtau

        xsectstr = """
        308043	5.8464E+01	1.0	1.0    LQ3btaubtau200	 aMcAtNloPy8EG_A14N30NLO_LQ3_up_beta1_M200
        308044	7.8640E+00	1.0	1.0    LQ3btaubtau300	 aMcAtNloPy8EG_A14N30NLO_LQ3_up_beta1_M300
        308045	1.7025E+00	1.0	1.0    LQ3btaubtau400	 aMcAtNloPy8EG_A14N30NLO_LQ3_up_beta1_M400
        308046	4.8304E-01	1.0	1.0    LQ3btaubtau500	 aMcAtNloPy8EG_A14N30NLO_LQ3_up_beta1_M500
        308047	1.6310E-01	1.0	1.0    LQ3btaubtau600	 aMcAtNloPy8EG_A14N30NLO_LQ3_up_beta1_M600
        308048	6.1812E-02	1.0	1.0    LQ3btaubtau700	 aMcAtNloPy8EG_A14N30NLO_LQ3_up_beta1_M700
        308049	2.5678E-02	1.0	1.0    LQ3btaubtau800	 aMcAtNloPy8EG_A14N30NLO_LQ3_up_beta1_M800
        308050	1.1420E-02	1.0	1.0    LQ3btaubtau900	 aMcAtNloPy8EG_A14N30NLO_LQ3_up_beta1_M900
        308051	5.3397E-03	1.0	1.0    LQ3btaubtau1000	 aMcAtNloPy8EG_A14N30NLO_LQ3_up_beta1_M1000
        308052	2.6091E-03	1.0	1.0    LQ3btaubtau1100	 aMcAtNloPy8EG_A14N30NLO_LQ3_up_beta1_M1100
        308053	1.3129E-03	1.0	1.0    LQ3btaubtau1200	 aMcAtNloPy8EG_A14N30NLO_LQ3_up_beta1_M1200
        308054	6.7894E-04	1.0	1.0    LQ3btaubtau1300	 aMcAtNloPy8EG_A14N30NLO_LQ3_up_beta1_M1300
        308055	3.5952E-04	1.0	1.0    LQ3btaubtau1400	 aMcAtNloPy8EG_A14N30NLO_LQ3_up_beta1_M1400
        308056	1.9645E-04	1.0	1.0    LQ3btaubtau1500	 aMcAtNloPy8EG_A14N30NLO_LQ3_up_beta1_M1500
        """
             
    else:
        sys.exit("Unknown channel %s" % chan)

    BR = 1 

    scaling = {}
    if scale:
        f = open("scripts/HHbbtautau/xsratios8_13.txt")
        for l in f:
            tok = l.split()
            m = float(tok[0])
            r = float(tok[1])
            scaling[m] = r

    if xstype == "old":
        for l in xsectstr.split('\n'):
            tok = l.split()
            if not tok: continue        
            m = int(tok[-1].split("_")[5].replace("M", "").replace("hh", ""))
            xs = float(tok[1]) * BR

            result[m] = xs    

    for m, xs in result.iteritems():
        if not stat and (m in [1300,1400,1500] or (scale1000 and m == 1100) or (scale1000 and not rw and m in [900, 1000, 1100])): xs *= 1000

        if br is not None:
            xs *= reweight_beta1(m, br)

        if scale:
            xs /= scaling[m]

        result[m] = xs
        
    return result

def LQXS(xstype = "old", br = None, decay = False):
    res = xsect(chan="lephad", decay = decay, br = br, xstype = xstype)

    m = array('f', [])
    e = array('f', [])

    for k, v in sorted(res.iteritems()):
        if k < 400 or k > 1000: continue
        m.append(k)
        e.append(v)

    #m = array('f', [200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500])
    #e = array('f', [5.8464E+01, 7.8640E+00, 1.7025E+00, 4.8304E-01, 1.6310E-01, 6.1812E-02, 2.5678E-02, 1.1420E-02, 5.3397E-03, 2.6091E-03, 1.3129E-03, 
    #                6.7894E-04, 3.5952E-04, 1.9645E-04])

    return m, e
    
def plot(name, masses, same=False, color=R.kRed, legend="exp (LQ)", chan="lephad", decay=True, dump=True, xstype = "new", br = None, scale1000 = False, rw = False):

    m = array('f', [])
    o = array('f', []);e = array('f', [])
    m2 = array('f', []);m1 = array('f', [])
    p1 = array('f', []);p2 = array('f', [])

    stat_only = "StatOnly" in name
    xs = xsect(scale=False, chan=chan, decay=decay, stat = stat_only, br = br, xstype = xstype, scale1000 = scale1000, rw = rw)

    for mass in masses:
        if 'afs' in name:
            f = R.TFile.Open(name.format(mass) + "/" + str(mass) + ".root")
        else:
            f = R.TFile.Open("root-files/" + name.format(mass) + "/" + str(mass) + ".root")

        if f:
            info = f.Get("limit")
        else:
            info = None
            
        if info:
            vo  = info.GetBinContent(1) * xs[mass] 
            ve  = info.GetBinContent(2) * xs[mass] 
            vm1 = ve - info.GetBinContent(5) * xs[mass]
            vp1 = info.GetBinContent(4) * xs[mass] - ve
            vm2 = ve - info.GetBinContent(6) * xs[mass]
            vp2 = info.GetBinContent(3) * xs[mass] - ve
        else:
            vo = ve = vm1 = vp1 = vm2 = vp2 = 0

        m.append(mass)
        o.append(vo)
        e.append(ve)
        m1.append(vm1)
        m2.append(vm2)
        p1.append(vp1)
        p2.append(vp2)

        if f:
            f.Close()

    if dump:
        print "#"*100
        print legend
        print "#"*100        

        print m
        print e
    
    z = array('f', [0]*len(m))

    ge = R.TGraphAsymmErrors(len(m), m, e, z, z, z, z)
    if same:
        ge.SetLineColor(color);ge.SetMarkerColor(color)    
    else:
        ge.SetLineColor(R.kBlack);ge.SetMarkerColor(R.kBlack)

    if not same or same == 2:
        ge.SetLineStyle(2)

    go = R.TGraphAsymmErrors(len(m), m, o, z, z, z, z)
    if same == 2:
        go.SetLineColor(color);go.SetMarkerColor(color)
    else:
        go.SetLineColor(R.kBlack);go.SetMarkerColor(R.kBlack)
    
    g1 = R.TGraphAsymmErrors(len(m), m, e, z, z, m1, p1)
    g1.SetFillColor(R.kGreen)

    g2 = R.TGraphAsymmErrors(len(m), m, e, z, z, m2, p2)
    g2.SetFillColor(R.kYellow)

    mlq, elq = LQXS(xstype, decay)
    glq = R.TGraph(len(mlq), mlq, elq)
    glq.SetLineColor(R.kMagenta+3);glq.SetMarkerColor(R.kMagenta+3)   

    g = R.TMultiGraph("", "Limits for " + chan + (";m_{LQ} [GeV]; 95% CL Limit on #sigma_{LQLQ} [pb]" if decay else
                                                  ";m_{LQ} [GeV]; 95% CL Limit on #sigma_{LQLQ} [pb]"))


    if not same:
        g.Add(g2, "E3")
        g.Add(g1, "E3")
        g.Add(go, "PL*")

    if same == 2:
        g.Add(go, "PL*")
    g.Add(ge, "PL*")
    
    if same:
        if same == 2:
            leg.AddEntry(go, legend.replace("exp", "obs").replace("Expected", "Observed"), "l") 
        leg.AddEntry(ge, legend, "l")
    else:
        leg.AddEntry(go, legend.replace("exp", "obs").replace("Expected", "Observed"), "l") 
        leg.AddEntry(ge, legend, "l")

        g.Add(glq, "L")
        leg.AddEntry(glq, "LQLQ cross section", "l")

    R.gPad.SetLogy(True)
    g.Draw("P" if same else "AP")

    if not same:
        g.GetYaxis().SetRangeUser(1.1e-4, 1e2)
    
    c._hists.append(g)
    c._hists.append(ge)

    return m, e

def ratio2(m, exps, name="ratio.eps", bottom=False, title = "Ratio", legends = [], ymin = 0.2, ymax = 1.1):

    gm = R.TMultiGraph("", title + ";m_{X} [GeV]; Ratio of expected limits")
    c = R.TCanvas()
    c._hists = []

    if bottom:
        leg = R.TLegend(0.58,0.20,0.89,0.49)
    else:
        leg = R.TLegend(0.58,0.60,0.89,0.79)
    leg.SetBorderSize(0)
    leg.SetFillColor(0)
    
    nom = exps[0]
    for i,e in enumerate(exps[1:]):
        ratio = array('f', [r[0]/r[1] if r[1] != 0 else 0 for r in zip(e,nom)])
        g = R.TGraph(len(m), m, ratio)
        g.SetLineColor(i+1)
        g.SetMarkerColor(i+1)
        gm.Add(g, "PL*")        
        c._hists.append(g)        
        leg.AddEntry(g, legends[i] if legends else "new/old", "l")

        gm.SetMinimum(ymin)
        gm.SetMaximum(ymax)
    gm.Draw("AP")
    leg.Draw()
    l = R.TLine(250, 1, 1500, 1)
    l.SetLineColor(1)
    l.SetLineStyle(2)
    l.Draw()

    c.Print(name)


def ratio(masses, inputs, name="ratio.eps", bottom=False):

    m = array('f', masses)
    legs = inputs.keys()
    exps = inputs.values()

    gm = R.TMultiGraph("", "Limits for different setups ;m_{LQ} [GeV]; Ratio of expected limits for X/sT")
    c = R.TCanvas()
    c._hists = []

    if bottom:
        leg = R.TLegend(0.58,0.20,0.89,0.49)
    else:
        leg = R.TLegend(0.58,0.60,0.89,0.79)
    leg.SetBorderSize(0)
    leg.SetFillColor(0)

    nom = exps[0]

    for i,e in enumerate(exps[1:]):
        ratio = array('f', [r[0]/r[1] if r[1] != 0 else 0 for r in zip(e,nom)])
        g = R.TGraph(len(m), m, ratio)
        g.SetLineColor(i+1)
        g.SetMarkerColor(i+1)
        gm.Add(g, "PL*")        
        c._hists.append(g)
        leg.AddEntry(g, legs[i+1], "l")

    gm.SetMinimum(0.2)
    gm.SetMaximum(1.1)    
    gm.Draw("AP")
    leg.Draw()
    l = R.TLine(250, 1, 1000, 1)
    l.SetLineColor(1)
    l.SetLineStyle(2)
    l.Draw()

    c.Print(name)

    
if __name__ == '__main__':

    R.gROOT.SetBatch(True)

    c = R.TCanvas()
    c._hists = []
    leg = R.TLegend(0.58,0.60,0.89,0.89)
    leg.SetBorderSize(0)
    leg.SetFillColor(0)

    outver = sys.argv[1] if len(sys.argv) > 1 else "150916"

    if chan == "tautau":
        masses = [200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500]
        plot("LQComb070618.090318_HH_13TeV_090318_Systs_tautau_LQ_BDT_{0}_obs", masses, chan=chan, legend="exp (LQ, tautau, Systs, 36.1 fb-1)")
        plot("LQ260218.270218b1_HH_13TeV_270218b1_Systs_lephad_LQ_BDT_{0}_obs", masses, chan=chan, same=True, color=R.kBlue, legend="exp (LQ, lephad, Systs, 36.1 fb-1)")
        plot("LQComb070618.090318_HH_13TeV_090318_Systs_hadhad_LQ_BDT_{0}_obs", masses, chan=chan, same=True, color=R.kRed, legend="exp (LQ, hadhad, Systs, 36.1 fb-1)")        
        leg.Draw() 
        c.Print("limit.LQ."+chan+".eps") 
        leg.Clear()


        masses = [200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500]
        plot("LQComb070618.090318_HH_13TeV_090318_StatOnly_tautau_LQ_BDT_{0}_obs", masses, chan=chan, legend="exp (LQ, tautau, Stats, 36.1 fb-1)")
        plot("LQ260218.270218b1_HH_13TeV_270218b1_StatOnly_lephad_LQ_BDT_{0}_obs", masses, chan=chan, same=True, color=R.kBlue, legend="exp (LQ, lephad, Stats, 36.1 fb-1)")
        plot("LQComb070618.090318_HH_13TeV_090318_StatOnly_hadhad_LQ_BDT_{0}_obs", masses, chan=chan, same=True, color=R.kRed, legend="exp (LQ, hadhad, Stats, 36.1 fb-1)")                
        leg.Draw() 
        c.Print("limit.LQ."+chan+".stat.eps") 
        leg.Clear()


        sys.exit()        

    masses = [400, 500, 600, 700, 800, 900, 1000]
    plot("LQCombBeta05.090418b_HH_13TeV_090418b_Systs_tautau_LQDown10_BDT_{}_obs", masses, chan="tautau", legend="exp (LQ, tautau, Systs, 36.1 fb-1)")
    plot("LQ280318.170518_HH_13TeV_170518_Systs_lephad_LQDown10_BDT_{}_obs", masses, chan="lephad", same = True, color=R.kBlue, legend="exp (LQ, lephad, Systs, 36.1 fb-1)")
    plot("LQCombBeta05.170518b_HH_13TeV_170518b_Systs_hadhad_LQDown10_BDT_{}_obs", masses, chan="hadhad", same = True, color=R.kRed, legend="exp (LQ, hadhad, Systs, 36.1 fb-1)")        

    leg.Draw() 
    c.Print("limit.LQ."+chan+".Down.eps") 
    leg.Clear() 

    sys.exit()

    plot("LQ280318.050418_HH_13TeV_050418_Systs_lephad_LQ_BDT_{0}_obs", masses, chan=chan, legend="exp (LQ, lephad n>10, Systs, 36.1 fb-1)")
    plot("LQ280318.050418n20_HH_13TeV_050418n20_Systs_lephad_LQ_BDT_{0}_obs", masses, chan=chan, same=2, color=R.kBlue, legend="exp (LQ, lephad n>20, Systs, 36.1 fb-1)")
    plot("LQ280318.050418n30_HH_13TeV_050418n30_Systs_lephad_LQ_BDT_{0}_obs", masses, chan=chan, same=2, color=R.kRed, legend="exp (LQ, lephad n>30, Systs, 36.1 fb-1)")
    plot("LQ280318.050418n40_HH_13TeV_050418n40_Systs_lephad_LQ_BDT_{0}_obs", masses, chan=chan, same=2, color=R.kMagenta, legend="exp (LQ, lephad n>40, Systs, 36.1 fb-1)")    

    leg.Draw() 
    c.Print("limit.LQ."+chan+".Comp.eps") 
    leg.Clear() 

    sys.exit()

    plot("LQ260218.270218b1_HH_13TeV_270218b1_Systs_lephad_LQ_BDT_{0}_obs", masses, chan=chan, legend="exp (LQ, lephad, Systs, 36.1 fb-1)")
    plot("LQ260218.270218b1_HH_13TeV_270218b1_StatOnly_lephad_LQ_BDT_{0}_obs", masses, chan=chan, same=True, color=R.kBlue, legend="exp (LQ, lephad, Stats, 36.1 fb-1)")
    plot("LQ260218.270218_HH_13TeV_270218_Systs_lephad_LQ_BDT_{0}_obs", masses, chan=chan, same=True, color=R.kRed, legend="exp (LQ, lephad, Systs (2tag only), 36.1 fb-1)")        

       
    leg.Draw() 
    c.Print("limit.LQ."+chan+".eps") 
    leg.Clear()

    masses = [200, 225, 250, 275, 300, 325, 350, 400, 450, 500, 550, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500]
    massesbr = [400, 500, 600, 700, 800, 900, 1100]
    massesbrscaled = [400, 500, 600, 700, 800, 900, 1000, 1100]

    for b in [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]:
        plot("LQ280318.290318_HH_13TeV_290318_Systs_lephad_LQ_BDT_{0}_obs", masses, chan=chan, xstype = "new", br = b, legend="exp (LQ, lephad, BR = {} scaled, Systs, 36.1 fb-1)".format(b))
        if b == 1.0:
            plot("LQ280318.290318_HH_13TeV_290318_StatOnly_lephad_LQ_BDT_{0}_obs", masses, chan=chan, same=True, xstype = "new", color=R.kBlue, legend="exp (LQ, lephad, Stats, 36.1 fb-1)")
        plot("LQ280318.290318_HH_13TeV_290318_Systs_lephad_LQUp" + "{:02d}".format(int(b*10)) + "_BDT_{0}_obs", massesbr, chan=chan, same=True, xstype = "new", color=R.kRed,
             legend="exp (LQ, lephad, BR = {} reweighted, Stats, 36.1 fb-1)".format(b), scale1000 = True if b in [0.1, 0.2] else False, rw = True)

        plot("LQ280318.030418b_HH_13TeV_030418b_Systs_lephad_LQ_BDT_BR" + "{:02d}".format(int(b*10)) +  "_{}_obs", massesbrscaled, chan=chan, xstype = "new", same=True, color=R.kMagenta+1,
             legend="exp (LQ, lephad, BR = {} scaled in WS, Systs, 36.1 fb-1)".format(b), scale1000 = True if b in [0.1, 0.2] else False)

       
        leg.Draw() 
        c.Print("limit.LQ."+chan+".BR{:02d}.eps".format(int(b*10))) 
        leg.Clear()
        c.Clear()

    sys.exit()    


    masses = [200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500]
    #masses2 = [300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 1500]
    #masses3 = [200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400]

    m, e = plot("LQ200917.dm74_HH_13TeV_dm74_FloatOnly_lephad_LQ3_BDT_{0}_obs", masses, chan=chan, legend="exp (LQ, lephad, stat no mmc corr, 36.1 fb-1)")    
    _, ecorr = plot("LQ270917.dM7Corr_HH_13TeV_dM7Corr_FloatOnly_lephad_LQ3_BDT_{0}_obs", masses, same=True, chan=chan, color=R.kMagenta, legend="exp (LQ, lephad, stat with mmc corr, 36.1 fb-1)")    
    #_, edpt = plot("LQ130917dpt2.dpt21_HH_13TeV_dpt21_FloatOnly_lephad_LQ3_BDT_{0}_obs", masses, same=True, chan=chan, color=R.kBlue, legend="exp (LQ, lephad, dpT 21, 36.1 fb-1)")    
    #e3 = plot("HH130717v7.7varsdn_HH_13TeV_7varsdn_FloatOnly_lephad_LQ3_BDT_{0}_obs", masses2, same=True, chan=chan, color=R.kRed, legend="exp (LQ, lephad, dn, 13.2 fb-1)")
    #e3 = plot("HH130717v7.LQvars7_HH_13TeV_LQvars7_FloatOnly_lephad_LQ3_{0}_obs", masses, same=True, chan=chan, color=R.kRed, legend="exp (LQ, lephad, 7 vars, 13.2 fb-1)")
    #e4 = plot("LQ101016.101016ave_HH_13TeV_101016ave_StatOnly_lephad_LQ3_{0}_exp", masses, same=True, chan=chan, color=R.kBlue, legend="exp (LQ, lephad, MLQ ave, 13.2 fb-1)")
    #e5 = plot("LQ101016.101016minonetag_HH_13TeV_101016minonetag_StatOnly_lephad_LQ3_{0}_exp", masses, same=True, chan=chan, color=R.kCyan+3, legend="exp (LQ, lephad, MLQ min + 1 b-tag, 13.2 fb-1)")            
    leg.Draw() 
    c.Print("limit.LQ."+chan+".dm7corr.eps") 

    ratio2(m, [e, ecorr], title = "Effect of adding 1 tag", legends = ["corr/no corr"], ymin = 0.9, ymax = 1.1, name = "ratio.LQ."+chan+".dm7corr.eps")
                   

    c.Clear()
    leg.Clear()
    
    sys.exit()

    m, estat = plot("LQ200917.dm74_HH_13TeV_dm74_FloatOnly_lephad_LQ3_BDT_{0}_obs", masses, chan=chan, legend="exp (LQ, lephad, dM pair 7 vars (stat), 36.1 fb-1)")    
    _, esyst = plot("LQ200917.dm74_HH_13TeV_dm74_Systs_lephad_LQ3_BDT_{0}_obs", masses, same=True, chan=chan, color=R.kMagenta, legend="exp (LQ, lephad, dm pair 7 vars (systs), 36.1 fb-1)")    
    #_, edpt = plot("LQ130917dpt2.dpt21_HH_13TeV_dpt21_FloatOnly_lephad_LQ3_BDT_{0}_obs", masses, same=True, chan=chan, color=R.kBlue, legend="exp (LQ, lephad, dpT 21, 36.1 fb-1)")    
    #e3 = plot("HH130717v7.7varsdn_HH_13TeV_7varsdn_FloatOnly_lephad_LQ3_BDT_{0}_obs", masses2, same=True, chan=chan, color=R.kRed, legend="exp (LQ, lephad, dn, 13.2 fb-1)")
    #e3 = plot("HH130717v7.LQvars7_HH_13TeV_LQvars7_FloatOnly_lephad_LQ3_{0}_obs", masses, same=True, chan=chan, color=R.kRed, legend="exp (LQ, lephad, 7 vars, 13.2 fb-1)")
    #e4 = plot("LQ101016.101016ave_HH_13TeV_101016ave_StatOnly_lephad_LQ3_{0}_exp", masses, same=True, chan=chan, color=R.kBlue, legend="exp (LQ, lephad, MLQ ave, 13.2 fb-1)")
    #e5 = plot("LQ101016.101016minonetag_HH_13TeV_101016minonetag_StatOnly_lephad_LQ3_{0}_exp", masses, same=True, chan=chan, color=R.kCyan+3, legend="exp (LQ, lephad, MLQ min + 1 b-tag, 13.2 fb-1)")            
    leg.Draw() 
    c.Print("limit.LQ."+chan+".dm7statsyst.eps") 

    ratio2(m, [estat, esyst], title = "Effect of systematics", legends = ["syst/stat"], ymin = 0.9, ymax = 2.0, name = "ratio.LQ."+chan+".dm7statsyst.eps")
                   

    c.Clear()
    leg.Clear()

    m, e2tag = plot("LQ200917.dm74_HH_13TeV_dm74_FloatOnly_lephad_LQ3_BDT_{0}_obs", masses, chan=chan, legend="exp (LQ, lephad, 2 tag (stat), 36.1 fb-1)")    
    _, e1tag = plot("LQ200917.dm7onetag2_HH_13TeV_dm7onetag2_FloatOnly_lephad_LQ3_BDT_{0}_obs", masses, same=True, chan=chan, color=R.kMagenta, legend="exp (LQ, lephad, 2 tag + 1 tag (stat), 36.1 fb-1)")    
    #_, edpt = plot("LQ130917dpt2.dpt21_HH_13TeV_dpt21_FloatOnly_lephad_LQ3_BDT_{0}_obs", masses, same=True, chan=chan, color=R.kBlue, legend="exp (LQ, lephad, dpT 21, 36.1 fb-1)")    
    #e3 = plot("HH130717v7.7varsdn_HH_13TeV_7varsdn_FloatOnly_lephad_LQ3_BDT_{0}_obs", masses2, same=True, chan=chan, color=R.kRed, legend="exp (LQ, lephad, dn, 13.2 fb-1)")
    #e3 = plot("HH130717v7.LQvars7_HH_13TeV_LQvars7_FloatOnly_lephad_LQ3_{0}_obs", masses, same=True, chan=chan, color=R.kRed, legend="exp (LQ, lephad, 7 vars, 13.2 fb-1)")
    #e4 = plot("LQ101016.101016ave_HH_13TeV_101016ave_StatOnly_lephad_LQ3_{0}_exp", masses, same=True, chan=chan, color=R.kBlue, legend="exp (LQ, lephad, MLQ ave, 13.2 fb-1)")
    #e5 = plot("LQ101016.101016minonetag_HH_13TeV_101016minonetag_StatOnly_lephad_LQ3_{0}_exp", masses, same=True, chan=chan, color=R.kCyan+3, legend="exp (LQ, lephad, MLQ min + 1 b-tag, 13.2 fb-1)")            
    leg.Draw() 
    c.Print("limit.LQ."+chan+".dm7onetag.eps") 

    ratio2(m, [e2tag, e1tag], title = "Effect of adding 1 tag", legends = ["2 + 1 tag/2 tag"], ymin = 0.2, ymax = 1.1, name = "ratio.LQ."+chan+".dm7onetag.eps")
                   

    c.Clear()
    leg.Clear()

    m, eCBst = plot("LQ200917.cutbased2_HH_13TeV_cutbased2_FloatOnly_lephad_LQ3_{0}_obs", masses, chan=chan, legend="exp (LQ, lephad, cut-based (stat), 36.1 fb-1)")    
    _, eCBsy = plot("LQ200917.cutbased2_HH_13TeV_cutbased2_Systs_lephad_LQ3_{0}_obs", masses, same=True, chan=chan, color=R.kMagenta, legend="exp (LQ, lephad, cut-based (syst), 36.1 fb-1)")    
    #_, edpt = plot("LQ130917dpt2.dpt21_HH_13TeV_dpt21_FloatOnly_lephad_LQ3_BDT_{0}_obs", masses, same=True, chan=chan, color=R.kBlue, legend="exp (LQ, lephad, dpT 21, 36.1 fb-1)")    
    #e3 = plot("HH130717v7.7varsdn_HH_13TeV_7varsdn_FloatOnly_lephad_LQ3_BDT_{0}_obs", masses2, same=True, chan=chan, color=R.kRed, legend="exp (LQ, lephad, dn, 13.2 fb-1)")
    #e3 = plot("HH130717v7.LQvars7_HH_13TeV_LQvars7_FloatOnly_lephad_LQ3_{0}_obs", masses, same=True, chan=chan, color=R.kRed, legend="exp (LQ, lephad, 7 vars, 13.2 fb-1)")
    #e4 = plot("LQ101016.101016ave_HH_13TeV_101016ave_StatOnly_lephad_LQ3_{0}_exp", masses, same=True, chan=chan, color=R.kBlue, legend="exp (LQ, lephad, MLQ ave, 13.2 fb-1)")
    #e5 = plot("LQ101016.101016minonetag_HH_13TeV_101016minonetag_StatOnly_lephad_LQ3_{0}_exp", masses, same=True, chan=chan, color=R.kCyan+3, legend="exp (LQ, lephad, MLQ min + 1 b-tag, 13.2 fb-1)")            
    leg.Draw() 
    c.Print("limit.LQ."+chan+".dm7cutbasedsyst.eps") 

    ratio2(m, [eCBst, eCBsy], title = "Effect of systematics on cut-based", legends = ["stat / syst"], ymin = 0.9, ymax = 2.0, name = "ratio.LQ."+chan+".dm7cutbasedsyst.eps")
                   

    c.Clear()
    leg.Clear()

    #e1 = plot("LQ101016.101016st_HH_13TeV_101016st_Systs_lephad_LQ3_{0}_exp", masses, chan=chan, legend="exp (LQ, lephad, sT, 13.2 fb-1)")
    #e2 = plot("LQ101016.101016_HH_13TeV_101016_Systs_lephad_LQ3_{0}_exp", masses, same=True, chan=chan, color=R.kMagenta, legend="exp (LQ, lephad, MLQ max, 13.2 fb-1)")
    #e3 = plot("LQ101016.101016min_HH_13TeV_101016min_Systs_lephad_LQ3_{0}_exp", masses, same=True, chan=chan, color=R.kRed, legend="exp (LQ, lephad, MLQ min, 13.2 fb-1)")
    #e4 = plot("LQ101016.101016ave_HH_13TeV_101016ave_Systs_lephad_LQ3_{0}_exp", masses, same=True, chan=chan, color=R.kBlue, legend="exp (LQ, lephad, MLQ ave, 13.2 fb-1)")
    #e5 = plot("LQ101016.101016minonetag_HH_13TeV_101016minonetag_Systs_lephad_LQ3_{0}_exp", masses, same=True, chan=chan, color=R.kCyan+3, legend="exp (LQ, lephad, MLQ min + 1 b-tag, 13.2 fb-1)")            
    #leg.Draw() 
    #c.Print("limit.LQ."+chan+".syst.eps") 

    #c.Clear()
    #leg.Clear()

    #ratio(masses, odict([("sT" , e1),
    #                     ("MLQ max" , e2),
    #                     ("MLQ min" , e3), 
    #                     ("MLQ ave" , e4),                         
    #                     ("MLQ min + 1 b-tag" , e5)]), "ratio.LQ.syst.eps")
    
