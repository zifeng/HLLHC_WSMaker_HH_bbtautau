#inver=26092019
#inver=26092019_InjectionSMmu10
#inver=26092019_InjectionX251mu44
#inver=26092019_InjectionX500mu021
#inver=26092019_InjectionX1000mu006
#inver=HHv13HadHadNew2
#inver=25052021PostProc
inver=HadHad17062021
outver=$1


##For plots and tables you might need to adjust scripys/analysisPlottingConfig.py, especially for plots it is all a bit hard coded (e.g. signal to plot, lables and so on)

#prefit tables
#python WSMakerCore/scripts/runFitCrossCheck.py $inver.${outver}_HH_13TeV_${outver}_Systs_hadhad_SM_BDT_0 4 0
#python WSMakerCore/scripts/makeTables.py -f $inver.${outver}_HH_13TeV_${outver}_Systs_hadhad_SM_BDT_0 -m 0 -t 0 $inver.${outver}_HH_13TeV_${outver}_Systs_hadhad_SM_BDT_0 # <fit result> -m <mass> -t <type> <wsname>

#prefit plots
#python WSMakerCore/scripts/runFitCrossCheck.py $inver.${outver}_HH_13TeV_${outver}_Systs_hadhad_SMggFVBF_BDT_0 4 0
#python WSMakerCore/scripts/doPlotFromWS.py -f $inver.${outver}_HH_13TeV_${outver}_Systs_hadhad_SMggFVBF_BDT_0 -m 0 -p 0 $inver.${outver}_HH_13TeV_${outver}_Systs_hadhad_SMggFVBF_BDT_0

#python WSMakerCore/scripts/runFitCrossCheck.py $inver.${outver}_HH_13TeV_${outver}_Systs_hadhad_2HDM_BDT_300 4 300
#python WSMakerCore/scripts/doPlotFromWS.py -f $inver.${outver}_HH_13TeV_${outver}_Systs_hadhad_2HDM_BDT_300 -m 300 -p 0 $inver.${outver}_HH_13TeV_${outver}_Systs_hadhad_2HDM_BDT_300


#postfit tables
#python WSMakerCore/scripts/runFitCrossCheck.py $inver.${outver}_HH_13TeV_${outver}_Systs_hadhad_SM_BDT_0 4 0
#python WSMakerCore/scripts/makeTables.py -f $inver.${outver}_HH_13TeV_${outver}_Systs_hadhad_SM_BDT_0 -m 0 -t 1 $inver.${outver}_HH_13TeV_${outver}_Systs_hadhad_SM_BDT_0 # <fit result> -m <mass> -t <type> <wsname>


#python WSMakerCore/scripts/runFitCrossCheck.py $inver.${outver}_HH_13TeV_${outver}_Systs_hadhad_2HDM_BDT_1000 4 1000
#python WSMakerCore/scripts/makeTables.py -f $inver.${outver}_HH_13TeV_${outver}_Systs_hadhad_2HDM_BDT_1000 -m 1000 -t 1 $inver.${outver}_HH_13TeV_${outver}_Systs_hadhad_2HDM_BDT_1000

#postfit plots
#python WSMakerCore/scripts/runFitCrossCheck.py $inver.${outver}_HH_13TeV_${outver}_Systs_hadhad_SM_BDT_0 4 0
#python WSMakerCore/scripts/doPlotFromWS.py -f $inver.${outver}_HH_13TeV_${outve r}_Systs_hadhad_SM_BDT_0 -m 0 -p 1 $inver.${outver}_HH_13TeV_${outver}_Systs_hadhad_SM_BDT_0


#python WSMakerCore/scripts/runFitCrossCheck.py $inver.${outver}_HH_13TeV_${outver}_Systs_hadhad_2HDM_BDT_1000 4 1000
#python WSMakerCore/scripts/doPlotFromWS.py -f $inver.${outver}_HH_13TeV_${outver}_Systs_hadhad_2HDM_BDT_500 -m 500 -p 1 $inver.${outver}_HH_13TeV_${outver}_Systs_hadhad_2HDM_BDT_500

#pulls
#python WSMakerCore/scripts/runFitCrossCheck.py $inver.${outver}_HH_13TeV_${outver}_Systs_hadhad_SMggFVBF_BDT_0 2,8 0
#python WSMakerCore/scripts/comparePulls.py -w $inver.${outver}_HH_13TeV_${outver}_Systs_hadhad_SMggFVBF_BDT_0 -a 2 -n

#python WSMakerCore/scripts/runFitCrossCheck.py $inver.${outver}_HH_13TeV_${outver}_Systs_hadhad_2HDM_BDT_1600 2,8 1600
#python WSMakerCore/scripts/comparePulls.py -w $inver.${outver}_HH_13TeV_${outver}_Systs_hadhad_2HDM_BDT_300 -a 2 -n 

#rankings
#python WSMakerCore/scripts/runNPranking.py $inver.${outver}_HH_13TeV_${outver}_Systs_hadhad_SMggFVBF_BDT_0 --mass 0 --num_slice 0
#python WSMakerCore/scripts/makeNPrankPlots.py $inver.${outver}_HH_13TeV_${outver}_Systs_hadhad_SMggFVBF_BDT_0 -m 0 --poi 0.005 --theta 1.5

#python WSMakerCore/scripts/runNPranking.py $inver.${outver}_HH_13TeV_${outver}_Systs_hadhad_2HDM_BDT_1600 --mass 1600 --num_slice 0
#python WSMakerCore/scripts/makeNPrankPlots.py $inver.${outver}_HH_13TeV_${outver}_Systs_hadhad_2HDM_BDT_300 -m 300 --poi 0.5 --theta 1.5


#sys breakdown table from data
#python WSMakerCore/scripts/runMuHat.py $inver.${outver}_HH_13TeV_${outver}_Systs_hadhad_SMggFVBF_BDT_0 --mass 0 --data obsData --exp 0 --slices -1 --mode 9 

#python WSMakerCore/scripts/runMuHat.py $inver.${outver}_HH_13TeV_${outver}_Systs_hadhad_2HDM_BDT_1000 --mass 1000 --data obsData --exp 0 --slices -1 --mode 9

#sys breakdown table from Asimov at limit
#python WSMakerCore/scripts/runMuHat.py $inver.${outver}_HH_13TeV_${outver}_Systs_hadhad_SMggFVBF_BDT_0 --mass 0 --data asimovDataAtLimit --exp 2 --slices -1 --mode 9

#NPs correlations
#python WSMakerCore/scripts/runFitCrossCheck.py $inver.${outver}_HH_13TeV_${outver}_Systs_hadhad_SM_BDT_0 2,7 0
#python WSMakerCore/scripts/makeReducedDiagPlots.py -p 2,7 $inver.${outver}_HH_13TeV_${outver}_Systs_hadhad_SM_BDT_0

#get significance
#python WSMakerCore/scripts/getSig.py $inver.${outver}_HH_13TeV_${outver}_Systs_hadhad_SM_BDT_0 0 0

#python WSMakerCore/scripts/getSig.py $inver.${outver}_HH_13TeV_${outver}_Systs_hadhad_2HDM_BDT_1000 0 1000
