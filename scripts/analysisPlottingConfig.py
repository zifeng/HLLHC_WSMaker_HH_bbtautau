import re
import ROOT
import plottingConfig as cfg
from optparse import OptionParser
import logging

_cache = []

# Stolen from MonoH, will need to be updated for bbtautau

class Config(cfg.PlottingConfig):

    def __init__ (self, options):
        self.options = options
        super(Config, self).__init__()

        self.loggingLvl = logging.DEBUG
        logging.basicConfig(format='%(levelname)s in %(module)s: %(message)s', level=self.loggingLvl)

        sigma = 500 # at mu=1 (arbitrary for AZh)
        sigma_units = 'fb'

        # for child classes to use

        # Set True for both to remove pickle files and recompute
        self.find_optimal_yrange = True #no automatic range for ratio  
        self.force_recompute_yields = True
        self.force_recompute_plotobjs = True
        self.draw_error_band_on_b_only = True
        self._mu = 1
        self._muhat = 1    
        self.verbose = False
        self.formats = [ 'eps','png','pdf','root']
        self.blind = True
        #self.thresh_drop_legend = 0.01
        self.thresh_drop_legend = 0.0
        self.restrict_to = []
        self.excludes = []
        self.additionalPlots = []              
        self.add_sig_to_ratio_plot = False
        self.use_exp_sig = True
        self.force_mu = True
        #self.sig = 0
        #self.sig_to_use= None
        # self.transferResults_fitName = "HiggsNorm"
        # self.get_binning_hist_removal = ["_meas2l2q2v2q"]
        self.file_tags = ["Y", "L", "J", "T", "TType", "Flv", "Sgn", "isMVA", "dist", "Spc", "D", "nAddTag", "BMax", "BMin", "Fat", "incFat", "incJet", "incAddTag"]
        self.dataname='obsData'

        self.atlas_rounding = True
        self.sig_names = ["Ghhbbtautau", "Hhhbbtautau", "hhttbb", "hhttbbRW"]

        # TODO: Add steering for type
        #self.force_mu = (True, 1/1000.)  # H or table or SM postfit input vars
        #self.force_mu = (True, 1000./1000.) # G
        self.force_mu = (True, 5./1000.) #1000) # Non-res
        #self.force_mu = (True, 200./1000.)
        #self.force_mu=(True, 1.)

        # TODO: Add steering for type  (STACK for BDT; OVERPRINT for input vars)      
        #self.signal = ["H (best fit)", self._OVERPRINT, ROOT.kRed + 1, 1] # last = mult factor
        #self.signal = ["H (best fit)", self._STACK, ROOT.kRed + 1, 1] # last = mult factor
        #self.signal = ["G", self._OVERPRINT, ROOT.kRed + 1, 1] # last = mult factor
        #self.signal = ["G", self._STACK, ROOT.kRed + 1, 1] # last = mult factor
        self.signal = ["non-res", self._OVERPRINT, ROOT.kRed + 1, 1] # last = mult factor    
        #self.signal = ["non-res", self._STACK, ROOT.kRed + 1, 1] # last = mult factor 
                     

        # TODO: Add steering for type (STACK for BDT; OVERPRINT for input vars)
        #self.expected_signal = ["H (#mu=1)" , self._OVERPRINT, ROOT.kRed +1, 1]
        #self.expected_signal = ["X" , self._OVERPRINT, ROOT.kRed +1, 1]        
        #self.expected_signal = ["X" , self._STACK, ROOT.kRed +1, 1]
          
        #self.expected_signal = ["G_{kk}" , self._OVERPRINT, ROOT.kRed +1, 1] 
        #self.expected_signal = ["G_{kk}" , self._STACK, ROOT.kRed +1, 1]          
        self.expected_signal = ["SM HH" , self._OVERPRINT, ROOT.kRed +1, 1]        
        #self.expected_signal = ["NR HH" , self._STACK, ROOT.kRed +1, 1]        

        # Define sets of backgrounds for merging later
        self.setBkgTop = {"ttbar", "stop"}
        self.setBkgOthers = {"Zhf", "Zlf", "Zttlf", "W", "Wtt", "Wjets", "diboson", "DY", "DYtt", "ttW", "ttZ"}
        self.setBkgSingleHiggs = {"ttH", "qqZHbb", "ggZHbb", "WHbb", "qqZHtautau", "ggZHtautau", "WHtautau", "VBFHtautau", "ggFHtautau"}

        listOfRest = lambda s, x: list(s - {x})

        self.bkg_tuple = {
            # Unmerged samples 

            'Ztthf': ("Z #rightarrow #tau#tau +(bb,bc,cc)", 23, ROOT.kGreen -9, []),            
            #'VH': ("Zh #rightarrow #tau#taubb", 20, ROOT.kRed - 6, []),
            #'Fake': ("jet #rightarrow #tau Fake", 40, ROOT.kPink + 1, []),
            'Fake': ("jet #rightarrow #tau_{had} fakes", 40, ROOT.kAzure -9, []),
            
            # Merge ttbar and single-top
            'ttbar': ("Top-quark", 42, ROOT.kOrange -2, listOfRest(self.setBkgTop, 'ttbar')),
            'stop': ("Top-quark", 42, ROOT.kOrange -2, listOfRest(self.setBkgTop, 'stop')),

            # merge VH and ttH, ZHbb, ZHtautau, WHbb, WHtautau,ggFHtautau, VBFHtautau 
            'ttH': ("SM Higgs", 20, ROOT.kYellow -9, listOfRest(self.setBkgSingleHiggs, 'ttH')),
            'ggZHbb': ("SM Higgs", 20, ROOT.kYellow -9, listOfRest(self.setBkgSingleHiggs, 'ggZHbb')),
            'qqZHbb': ("SM Higgs", 20, ROOT.kYellow -9, listOfRest(self.setBkgSingleHiggs, 'qqZHbb')),
            'WHbb': ("SM Higgs", 20, ROOT.kYellow -9, listOfRest(self.setBkgSingleHiggs, 'WHbb')),
            'ggZHtautau': ("SM Higgs", 20, ROOT.kYellow -9, listOfRest(self.setBkgSingleHiggs, 'ggZHtautau')),
            'qqZHtautau': ("SM Higgs", 20, ROOT.kYellow -9, listOfRest(self.setBkgSingleHiggs, 'qqZHtautau')),
            'WHtautau': ("SM Higgs", 20, ROOT.kYellow -9, listOfRest(self.setBkgSingleHiggs, 'WHtautau')),
            'VBFHtautau': ("SM Higgs", 20, ROOT.kYellow -9, listOfRest(self.setBkgSingleHiggs, 'VBFHtautau')),
            'ggFHtautau': ("SM Higgs", 20, ROOT.kYellow -9, listOfRest(self.setBkgSingleHiggs, 'ggFHtautau')),

                
            # Merging others (lephad)
            'Zhf': ("Other", 21, ROOT.kBlue -2, listOfRest(self.setBkgOthers, 'Zhf')),
            'Zlf': ("Other", 21, ROOT.kBlue -2, listOfRest(self.setBkgOthers, 'Zlf')),
            'Zttlf': ("Other", 21, ROOT.kBlue -2, listOfRest(self.setBkgOthers, 'Zttlf')),
            'W': ("Other", 21, ROOT.kBlue -2, listOfRest(self.setBkgOthers, 'W')),
            'Wtt': ("Other", 21, ROOT.kBlue -2, listOfRest(self.setBkgOthers, 'Wtt')),
            'diboson': ("Other", 21, ROOT.kBlue -2, listOfRest(self.setBkgOthers, 'diboson')),
            'ttZ': ("Other", 21, ROOT.kBlue -2, listOfRest(self.setBkgOthers, 'ttZ')),
            'ttW': ("Other", 21, ROOT.kBlue -2, listOfRest(self.setBkgOthers, 'ttW')),

            # Merging others (hadhad)
            #'ttbarFake': ("Top-quark fakes", 44, ROOT.kOrange + 3, ["stopFake"]),
            #'stopFake': ("Top-quark fakes", 44, ROOT.kOrange + 3, ["ttbarFake"]),
            'ttbarFake': ("jet #rightarrow #tau_{had} fakes (t#bar{t})", 22, ROOT.kRed -7, ["stopFake"]),
            'stopFake': ("jet #rightarrow #tau_{had} fakes (t#bar{t})", 22, ROOT.kRed -7, ["ttbarFake"]),

#             # Unmerged (2lepton)
#             'Zhf': ("Other", 22, ROOT.kAzure - 9, []),
# 
#             # Merging others (2lepton)
#             'Zlf': ("Other", 22, ROOT.kGray + 1,  ["Zttlf", "W", "Wtt", "diboson", "WW", "Diboson"]),
#             'Zttlf': ("Other", 22, ROOT.kGray + 1, ["Zlf", "W", "Wtt", "diboson", "WW", "Diboson"]),            
#             'W': ("Other", 22, ROOT.kGray + 1, ["Zlf", "Zttlf", "Wtt", "diboson", "WW", "Diboson"]),
#             'Wtt': ("Other", 22, ROOT.kGray + 1, ["Zlf", "Zttlf", "W", "diboson", "WW", "Diboson"]),                          
#             'diboson': ("Other", 22, ROOT.kGray + 1, ["Zlf", "Zttlf", "W", "Wtt", "WW", "Diboson"]),
#             'WW': ("Other", 22, ROOT.kGray + 1,  ["Zlf", "Zttlf", "W", "Wtt", "diboson", "Diboson"]),
#             'Diboson': ("Other", 22, ROOT.kGray + 1,  ["Zlf", "Zttlf", "W", "Wtt", "diboson", "WW"]),

            }

          

        # Order for tables
        self.priorities = {
            "S/sqrt(S+B)" : 201,
            "S/B" : 200,

            "SignalExpected" : 190,
            "Signal" : 180,

            "hhttbb" : 170,
            "hhttbbRW" : 160,            

            "data" : 150,

            "Bkg" : 141,
            "MC" : 140,

            "Top quark" : 1,
            "ttbar" : 2,
            "ttbarFake" : 3,      
            "stop" : 5,
            
            "QCDFake" : 10,                      
            "Fake" : 11,
            
            "Ztthf" : 20,
            "Zhf": 21,

            "SM Higgs" : 30,
            "VH" : 31,            
            "VH125" : 32,
            "WHbb" : 33, 
            "qqZHbb" : 34, 
            "ggZHbb" : 35, 
            "WHtautau" : 36, 
            "qqZHtautau" : 37, 
            "ggZHtautau" : 38, 
            "ttH" : 39,
            "VBFHtautau" : 40, 
            "ggFHtautau" : 41,

            "Other" : 50,
            "Other (W + Z + DY + VV)" : 51,
            "Other (W + Zlf + VV)" : 52,
            
            "diboson" : 53,
            "ttW" : 54,
            "ttZ" : 55,
            "Wjets" : 56,
            "W" : 57,
            "Wtt": 58,
            "DY" : 59,
            "DYtt" : 60,
            "Zlf": 61,
            "Zttlf" : 62,            
            }

        for m in [260] + range(300, 1001, 100):
            self.priorities["Ghhbbtautau%sc10" % m] = 60
            self.priorities["Ghhbbtautau%sc20" % m] = 60            
            self.priorities["Hhhbbtautau%s" % m] = 60
        

        self.ATLAS_suffix = "Internal"
        #self.ATLAS_suffix = "Work in progress"
        # self.ATLAS_suffix = "Simulation"
        #self.ATLAS_suffix = "Preliminary"
        #self.ATLAS_suffix = ""

        # for yields
        self.make_slides = False
        self.window = None
        # for reduced diag plots only
        self.exclude_str = 'HiggsNorm'

        self.cov_classification = {
        "BTag": [False, ["SysFT_EFF_Eigen", "SysFT_EFF_extrapolation"], []],
        "Top": [False, ["ACC_TTBAR", "norm_ttbar", "XS_ttbar"], []],
        "ZHF": [False, ["ACC_Zhf", "norm_Zhf"], []],
        "Stop": [False, ["Stop"], []],
        "ModelBoson": [False, ["XS_V", "ACC_W", "ACC_Zlf", "XS_Diboson", "ACC_Diboson", "ttH", "ZH", "WH", "VBFH", "ggFH", "ggFHH", "ggFSMHH", "BR_Hbb", "BR_Htautau"], []],
        "Norm": [False, ["Norm","Ratio"], []],
        "norm": [False, ["norm"], []],
        "Lepton": [False, ["SysMUON","SysEL","SysEG"], []],
        "Tau": [False, ["SysTAU"], []],
        "Jet": [False, ["SysJET","FATJET"], []],
        "MET": [False, ["SysMET"], []],
        "Others": [False, ["SysMET", "LUMI", "PRW"], []],
        "FakeRate": [False, ["FakeRate"], []],
        "ttbarFakeSF": [False, ["TTBAR_FAKESF"], []],
        "Fakes": [False, ["FF_OSSS","FF_OTHER_SUBTRACTION","FF_TRUE_TTBAR_SUBTRACTION","FF_Stat_HadHadSR","TF_STAT"], []], 
        "LepHadFakes" : [False, ["SysFFStat", "SysFFSubtraction", "SysttReweighting", "SysFFVar", "Subtraction_bkg"], []], 
        "Shifted": [True, [], ["blablabla"]]
        }

        self.cov_special = {
        "noMCStat": [[], ["gamma"]],
        "JES": [["SigX", "norm_", "Jet"], []],
        "BTag": [["SigX", "norm_", "BTag"], []],
        "Mbb": [["SigX", "norm_", "Mbb"], []],
        "Modelling": [["SigX", "norm_", "Norm", "Ratio", "PtBi"], []],
        "SF": [["SigX", "norm_"], []],
        "Norm": [["3JNorm", "norm_", "Norm", "Ratio"], []]
        }

        self.syst_to_study = ["JetEResol", "Mbb_Whf", "V_Whf", "METScale", "TChanP",
        "ttbarHigh", "BJetReso", "ZblZbb", "BTagB1", "norm_Wbb", "WblWbbRatio"]

        self.suspicious_syst = ["norm_"]
        # for yield ratios only

        self.allNPsYMax = 9
        self.allNPsYMin = -3

    def do_rebinning (self, prop):
        if (not "BDTVars" in prop["D"] and "BDT" in prop["D"] or "BDT" in prop["dist"]):
            return False
        else:
            return True

        return True

    def is_signal(self, compname):
        """ Check if a component is signal. If yes, return mass """
        # Spyros: Add ggA to list of signal names - has to be first in list otherwise we get problems

        signames = self.sig_names
        has_mass = False
        mass = ""
        # remove c10/c20 from compname
        compname = re.sub('c10', '', compname)
        compname = re.sub('c20', '', compname)        

        for sg in signames:
            if sg in compname:
                has_mass = True
                if sg == "hhttbb" or sg == "hhttbbRW":
                    mass = 0
                    break
                pos = compname.find(sg) + len(sg)
                #try:
                mass = int(re.sub("[^0-9]", "", compname[pos:pos + compname[pos:].find('_')]))                
#                 except ValueError:
#                     mass = 0
#                     break
        return has_mass, mass

    def determine_additional_plots_from_properties (self, prop):
        #if prop["dist"] == "MEff" :      return True
        return False

    def blind_data (self, setup):
        def do_blinding (title):
            return ("T2" in title and not "BDTVars" in title and not "SS" in title and not "HighMT" in title and ("mHH" in title or "mMMC" in title  or "BDT" in title)), [0, 0]

        

        do_blinding, blind_range = do_blinding(setup.title)

        #blind_range[0] = -1
        #blind_range[1] = 1
        #setup.data.blind(blind_range[0], blind_range[1])

        if setup.sig == 0:
            return

        hsig = setup.sig.h
        ntot = hsig.Integral()
        print "signal integral {0}".format(ntot)

        for i in range(hsig.GetNbinsX(),0, -1):
            print "bin {0}".format(i)
            print "integral {0}".format(hsig.Integral(i, hsig.GetNbinsX()))
            if hsig.Integral(i, hsig.GetNbinsX())/ntot > 0.85:
                blind_range[0] = hsig.GetBinCenter(i)
                blind_range[1] = setup.data.h.GetXaxis().GetXmax()  
                break            
        setup.data.blind(blind_range[0], blind_range[1])
        
        #if do_blinding and "BDT" in setup.title:
            # Blind 90% of signal from RHS for BDT-based

         #   hsig = setup.sig.h
          #  ntot = hsig.Integral()

         #   for i in range(hsig.GetNbinsX(), 0, -1):
          #      if hsig.Integral(i, hsig.GetNbinsX())/ntot > 0.9:
           #         blind_range[0] = hsig.GetBinLowEdge(i)
            #        blind_range[1] = setup.data.h.GetXaxis().GetXmax()
             #       break                
            #setup.data.blind(blind_range[0], blind_range[1])
                
        #elif do_blinding:
            # Blind all for cut-based
         #   if blind_range[0] == 0 and blind_range[1] == 0:
          #      blind_range[0] = setup.data.h.GetXaxis().GetXmin()
           #     blind_range[1] = setup.data.h.GetXaxis().GetXmax()
            #setup.data.blind(blind_range[0], blind_range[1])
            
       
        
            
    def add_histogram_preprocess (self, hist, sm):
        return hist

    def get_run_info (self):
        lumi = {}
        if self._year == "4023":
            lumi["2011"] = ["4.7", 7]
            lumi["2012"] = ["20.3", 8]
        if self._year == "2011":
            lumi["2011"] = ["4.7", 7]
        if self._year == "2012":
            lumi["2012"] = ["20.3", 8]
        if self._year == "2015":
            lumi["2015"] = ["139", 13]
            #lumi["2015"] = ["3000", 14]
        if self._year == "2016":
            lumi["2016"] = ["36.1", 13]
        return lumi

    def get_title_height (self):
        return 3.5 if self._year == "4023" else 2

    def draw_category_ids (self, props, l, pos, nf):

        merged = False
        plural_jets = False
        nf += 0.25*nf # a bit more vertical spacing

        signalControl = props.get("D", "")

        pos_next = pos[1] - 0.1*nf # a bit more spacing

        #chan = "#tau_{lep}#tau_{had}" if props.get("Spc", "") == "TauLH" else "#tau_{had}#tau_{had}"
        if props.get("Spc", "") == "TauLH" and props.get("L","") == "TT0":
            chan = "#tau_{lep}#tau_{had} SLT"
        elif props.get("Spc", "") == "TauLH" and props.get("L","") == "TT1":
            chan = "#tau_{lep}#tau_{had} LTT"
        else: chan = "#tau_{had}#tau_{had}"
        btag = "{} b-tags".format(props.get("T", ""))        
        region = "{} {}".format(chan, btag)

        l.SetTextFont(42)
        l.DrawLatex(pos[0], pos_next, region)


        if not signalControl in ["", "SR", "SRmHH", "NonResSR", "SRRSGBDT", "SRRSGc1BDT", "SRRSGc2BDT",
                                 "SR2HDMBDT", "SRSMBDT", "SRSMRW", 
                                 "BDTVarsPreselection", "SRSM", "ControlTau", "Preselection"] or props.get("L", "1") == "2":
            if "TopCR" in signalControl or "HighMT" in signalControl:
                #signalControl = "Top validation region"
                signalControl = "Top VR"

            if "RSGc1BDT" in signalControl:
                #signalControl = "Top validation region"
                signalControl = "Top VR"
             #   signalControl = "Z+HF validation region"
             #   signalControl = "Z+HF VR"
 

            #if "RSGc2BDT" in signalControl:               :
                #signalControl = "Top validation region"
            #   signalControl = "Z+HF validation region"

            if "SS" in signalControl:
                #signalControl = "SS validation region"
                signalControl = "SS VR"

            if "ControlTop" in signalControl:
                #signalControl = "Top validation region"
                signalControl = "Top VR"

            if "ControlZJet" in signalControl:
                #signalControl = "Z+HF validation region"
                signalControl = "Z+HF VR"                

            # ZCR i.e. 2mu
            if props.get("L", "1") == "2":
                signalControl = "Z control region"

            pos_next -= nf
            l.DrawLatex(pos[0], pos_next, signalControl)
            pos_next -= 0.8*nf
            #l.DrawLatex(pos[0], pos_next, "(G_{kk} 500 GeV, k/#bar{M}_{Pl}=1)")
            #l.DrawLatex(pos[0], pos_next, "(NR HH)")

        pos_next -= nf
        return (pos[0], pos_next)

    def force_mu_value (self):
        return self.force_mu

    def get_year_str (self):
        return "" #self._year if int(self._year) < 2100 else ""

    def fine_tune_additional_signal_mult_factor (self, dist, prop, current_mult_factor):
        if dist == "mBB"  : current_mult_factor /= 4

        # do current_mult_factor rounded
        current_mult_factor = int(current_mult_factor)

        return current_mult_factor

    def get_xbound_from_properties (self, prop):
        if prop["dist"] == "pTB1": return (40, 400)
        elif  prop["dist"] == "Tau0Pt" and prop["L"] == "0": return (40, 200)
        else: return None


    def get_legend_pos_from_properties (self, prop):
        result = None
        if prop["L"] == '0' and prop["dist"] == "VpT":
            result = [0.155, 0.13, 0.375, 0.65]
        if prop["dist"] == "dPhiVBB":
            result = [0.16, 0.16, 0.38, 0.68]
        return result

    def get_yscale_factor_from_properties (self, prop, logy):
        # if prop["dist"] == "MV1cB1" or prop["dist"] == "MV1cB2" or prop["dist"] == "MV1cBTag":
        #     if not logy: return 1.5
        return 1.0

    def set_histogram_properties_everything (self, prop, hist):
        if "MV1c" in prop["dist"]:
            hist.GetXaxis().SetNdivisions(-1)
            hist.GetXaxis().SetLabelSize(0.07)
        if prop["dist"] == "MET" or prop["dist"] == "pTV":
            # max_value = hist.GetBinContent(hist.GetMaximumBin())
            max_value = hist.GetMaximum()
            # min_value = hist.GetBinContent(hist.GetMinimumBin())
            # min_value = hist.GetMinimum()
            min_value = hist.GetYaxis().GetXmin()
            x_value = hist.GetXaxis().GetBinLowEdge(hist.GetXaxis().FindBin(500))
            l = ROOT.TLine(x_value, min_value, x_value, max_value)
            l.SetLineStyle(2)
            l.SetLineWidth(4)
            l.SetNDC(False)
            # l.Draw()
            l.DrawLine(x_value, min_value, x_value, max_value)

    def get_xTitle (self, prop, data_hist):
        """ get title of X-axis from properties """
        if not prop:
            return ""

        varname = prop["dist"]

        labels = {
            "mHH": "m_{HH} [GeV]",
            "SRmHH": "m_{HH} [GeV]",            
            "mMMC": "m_{#tau#tau} [GeV]",            
            "TauPt" :  "p_{T}^{#tau_1} [GeV]",
            "Tau0Pt" :  "p_{T}^{#tau}",            
            "SRRSGBDT" : "RSG BDT (%s GeV)" % prop["dist"],
            #"SRRSGc1BDT" : "RSG c=1 BDT (%s GeV)" % prop["dist"],
            #"SRRSGc2BDT" : "RSG c=2 BDT (%s GeV)" % prop["dist"],                        
            #"SR2HDMBDT" : "2HDM BDT (%s GeV)" % prop["dist"],
            #"SRSMBDT" : "SM BDT",
            #"SRSM" : "SM BDT",        
            "SRRSGc1BDT" : "BDT score",
            "SRRSGc2BDT" : "BDT score", 
            "RSGc1BDT" : "BDT score",
            "RSGc2BDT" : "BDT score",                        
            "SR2HDMBDT" : "BDT score",
            "SRSMBDT" : "BDT score",
            "SRSM" : "BDT score",    
            "SRSMRW" : "BDT score",         
            "SSSRRSGc1BDT" : "BDT score",
            "SSSRRSGc2BDT" : "BDT score",                        
            "SSSR2HDMBDT" : "BDT score",
            "SSSRSMBDT" : "BDT score",
            "SSSRSM" : "BDT score",    
            "SSSRSMRW" : "BDT score",   
            "BDTScoreHighMTCRRSGc1BDT" : "BDT score",
            "BDTScoreHighMTCRRSGc2BDT" : "BDT score",                        
            "BDTScoreHighMTCR2HDMBDT" : "BDT score",
            "BDTScoreHighMTCRSM" : "BDT score",    
            "BDTScoreHighMTCRSMRW" : "BDT score",  
            "BDTScoreControlTopOrigRSGc1BDT" : "BDT score",
            "BDTScoreControlTopOrigRSGc2BDT" : "BDT score",                     
            "BDTScoreControlTopOrig2HDMBDT" : "BDT score",
            "BDTScoreControlTopOrigSM" : "BDT score",    
            "BDTScoreControlTopOrigSMRW" : "BDT score",  
            "BDTScoreControlZJetRSGc1BDT" : "BDT score",
            "BDTScoreControlZJetRSGc2BDT" : "BDT score",                     
            "BDTScoreControlZJet2HDMBDT" : "BDT score",
            "BDTScoreControlZJetSM" : "BDT score",    
            "BDTScoreControlZJetSMRW" : "BDT score",  
            "mLL" : "m_{ll} [GeV]",

            "MET": "E_{T}^{miss} [GeV]",
            "MtW": "M_{T}^{W} [GeV]", 
            "dPhiHBB": "#Delta#phi(h,h)", 
            "Mhh": "m_{HH} [GeV]", 
            "mbb": "m_{bb} [GeV]", 
            "mBB": "m_{bb} [GeV]", 
            "pTBB":"p_{T}^{bb} [GeV]", 
            "METCent": "MET #phi centrality", 
            "pTB2": "p_{T}^{b_{2}} [GeV]", 
            "dPtLepTau": "#Delta p_{T}(l,#tau)", 
            "dRbb": "#DeltaR(b,b)", 
            "dRBB": "#DeltaR(b,b)", 
            "DRTauTau": "#DeltaR(#tau,#tau)", 
            "mMMC": "m_{#tau#tau}^{MMC} [GeV]", 
            }    

        if prop["D"] in labels:
            return labels.get(prop["D"])

        return labels.get(varname, varname)

    def get_yTitle_tag (self, prop, data_hist):
        #print "AAAAAAAAAAAAA"
        extra_unit = ""
        if prop["dist"] == "mHH" :  extra_unit = " GeV"
        if prop["dist"] == "mMMC" :  extra_unit = " GeV"
        if prop["dist"] == "TauPt" :  extra_unit = " GeV"
        if prop["dist"] == "mLL" :  extra_unit = " GeV"
        if prop["dist"] == "mBB" : extra_unit = " GeV"

        if prop["D"] == "SRmHH": extra_unit = " GeV" 

        if prop["dist"] in ["MET", "MtW", "Mhh", "mbb", "pTBB", "pTB2"]: extra_unit = " GeV"

        # NOTE: JWH - ED board requests
#         if not self.do_rebinning(prop):
#             extra_number = str(data_hist.GetBinWidth(1))
#             if not extra_number.find('.') == -1: extra_number = extra_number[:extra_number.find('.')]
#             extra_unit = " " + extra_number + " / " + extra_unit
#         else:


        if prop["dist"] == "Tau1Pt" and prop["L"] == "0":
            extra_unit = " / bin"
        else:
            #extra_number = str(round(data_hist.GetBinWidth(1),2))
            extra_number = str("%.2g" %round(data_hist.GetBinWidth(1),2))
            extra_unit = " / " + extra_number + extra_unit
        #extra_unit = " / " +  extra_unit


        
        return extra_unit

    def set_y_range (self, hist, nlegend_items, miny, total_miny, maxy, log_scale, prop):
        bottom_padding = 1.0/16.0
        content_faction = 0.8 if nlegend_items <= 8 else 3.0/7.0
        #content_faction = 1            

        if prop["dist"] == "mHH":
            content_faction = 0.65
        elif prop["dist"] == "mMMC":
            content_faction = 0.6
        elif not "BDTVars" in prop["D"] and "BDT" in prop["D"] or "BDT" in prop["dist"]:
            content_faction = 0.7        
        else:
            content_faction = 0.6

        if not log_scale:
            if miny < 1e-6: miny = 0
            plot_scale = (maxy - miny)
            bottom = miny - bottom_padding*plot_scale
            top = bottom + 1.3*plot_scale/content_faction

            hist.GetYaxis().SetLimits(bottom, top)

            print "setting plot y-range to {0} - {1}".format(bottom, top)
            return
        else:
            log_miny = ROOT.TMath.Log10(miny)
            log_maxy = ROOT.TMath.Log10(maxy)
            plot_scale = (log_maxy - log_miny)
            # 0.25 is just fine tuning
            # bottom = log_miny - 0.25*bottom_padding*plot_scale
            bottom = log_miny
            top = bottom + 1.2*plot_scale/content_faction
            #top = bottom + 1.7*plot_scale/content_faction

            hist.GetYaxis().SetLimits(ROOT.TMath.Power(10, bottom),
                                      ROOT.TMath.Power(10,top))

            print "setting log scale plot y-range to {0} - {1}".format(ROOT.TMath.Power(10, bottom), ROOT.TMath.Power(10, top))
            return

    def auto_compute_ratio_yscale_from_properties (self, prop):
        return (prop["dist"] == "mva" or prop["dist"] == "mvaVZ")

    def scale_all_yvals(self, prop):
        return prop["dist"] == "mva", 0.05

    def set_histogram_properties_dataMC_ratio (self, prop, hist):
        if "MV1c" in prop["dist"]:
            #self.relabel_mv1c_axis(hdash)
            hist.GetXaxis().SetLabelSize(0.2)
            ranges = ["100", "80", "70", "60", "50", "0"]
            nbins = hist.GetXaxis().GetNbins()
            if nbins is 4 or 5:
                print "write tex x-axis labels..."
                for ibin in range(1, nbins+2):
                    binlabel = ranges[ibin - nbins + 4]
                    pos_x = 0.11 + (ibin-1)*(0.907-0.11)/nbins
                    if binlabel == "0" : pos_x += 0.01
                    if binlabel == "100" : pos_x -= 0.01
                    pos_y = 0.26
                    l = ROOT.TLatex()
                    l.SetNDC()
                    l.SetTextFont(42)
                    l.SetTextSize(0.15)
                    l.SetTextColor(1)
                    l.SetTextAlign(11)
                    l.DrawLatex(pos_x, pos_y, binlabel)

    def determine_year_from_title (self, title):
        if "2015" in title:
            return "2015"
        elif "2012" in title:
            return "2012"
        elif "2011" in title:
            return "2011"
        elif "both" in title:
            return "4023"

    def combineComps(self, comps):        
        # return

        # To apply only for tables
        if not hasattr(self, "tables") or not self.tables: return

        # Loop over regions
        for ttname in comps:

            # Define components to be compbined

            if "L1" or "L0" in ttname:
                toComb = {
                    ROOT.RooAddition("Top quark", "Top quark", ROOT.RooArgList()) : list(self.setBkgTop),
                    #ROOT.RooAddition("Z #rightarrow #tau#tau", "Z #rightarrow #tau#tau", ROOT.RooArgList()) : ["Zttlf", "Ztthf", "DYtt"],
                    ROOT.RooAddition("Other", "Other (W + Z + DY + VV)", ROOT.RooArgList()) : list(self.setBkgOthers),
                    ROOT.RooAddition("SM Higgs", "SM Higgs", ROOT.RooArgList()) : list(self.setBkgSingleHiggs)
                    }
            elif "L2" in ttname:
                toComb = {
                    ROOT.RooAddition("Top quark", "Top quark", ROOT.RooArgList()) : list(self.setBkgTop),
                    ROOT.RooAddition("Other", "Other (W + Zlf + VV)", ROOT.RooArgList()) : list(self.setBkgOthers),
                    ROOT.RooAddition("SM Higgs", "SM Higgs", ROOT.RooArgList()) : list(self.setBkgSingleHiggs)
                    }
            else:
                continue

            toRemove = []

            # Loop over components
            compMap = comps[ttname][3]
            for outcomp, incomps in toComb.iteritems():
                for name, c in comps[ttname][3].iteritems():

                    # Add
                    if any([(("_" + ic + "_") in name) for ic in incomps]):
                        outcomp.list().add(c)
                        toRemove.append(name)

                # Register combination in comps
                comps[ttname][3][outcomp.GetTitle()] = outcomp

            # Delete original components: causes problem
            #for name in toRemove:
            #    if name in comps[ttname][3]:
            #        _cache.append(comps[ttname][3].pop(name))
                    
        return
    
    def add_additional_signal_info_to_legend (self, legend, signal):

        if signal.mass == 0: return

        t = "G" if "RSG" in signal.h.GetName() else "X"
        c = "2" if "2" in signal.h.GetName() else "1" if "RSG" in signal.h.GetName() else None
        #c = "1"
       

        #if signal.mode == self._STACK:
        #    legend.AddEntry(ROOT.NULL, "m_{%s}=" % t + str(signal.mass) + " GeV", "")
        #    if t == "G": legend.AddEntry(ROOT.NULL, "k/#bar{M}_{Pl}=%s" % c, "")
            
       # else:
        #    legend.AddEntry(ROOT.NULL, "m_{%s}=" % t + str(signal.mass) + " GeV", "")
         #   if t == "G": legend.AddEntry(ROOT.NULL, "k/#bar{M}_{Pl}=%s" % c, "")            

        #if signal.mode == self._STACK:
        if t == "G":
            legend.AddEntry(ROOT.NULL, "m_{%s}=" % t + str(signal.mass) + " GeV"+ ", k/#bar{M}_{Pl}=%s" % c , "")
        else:
            legend.AddEntry(ROOT.NULL, "m_{%s}=" % t + str(signal.mass) + " GeV", "")
        #    if t == "G": legend.AddEntry(ROOT.NULL, "k/#bar{M}_{Pl}=%s" % c, "")
            
       # else:
        #    legend.AddEntry(ROOT.NULL, "m_{%s}=" % t + str(signal.mass) + " GeV", "")
         #   if t == "G": legend.AddEntry(ROOT.NULL, "k/#bar{M}_{Pl}=%s" % c, "")            
