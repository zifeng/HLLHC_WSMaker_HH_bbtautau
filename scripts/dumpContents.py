import ROOT as R

keep = []

def main(fname, nbins = 2):
    f = R.TFile(fname)

    can = [k for k in f.GetListOfKeys() if type(k.ReadObj())][0].ReadObj()
    pad = can.FindObject("p1")
    stack = pad.FindObject("stack")
    data = pad.FindObject("h_obsData")
    print data.GetN()
    print data.GetY()[data.GetN()-1]
    print data.GetY()[data.GetN()-2]
    print data.GetY()[data.GetN()-1] + data.GetY()[data.GetN()-2]
    res = {}

    for h in stack.GetHists():
        name = h.GetName().split("_")[2]
        ilast = h.GetNbinsX()

        print ">>>", name
        args = R.RooArgList(name + "list")        
        for i in xrange(0, nbins):
            v = R.RooRealVar(name + "var" + str(i), name + "var" + str(i), h.GetBinContent(ilast - i))
            v.setError(h.GetBinError(ilast - i))
            keep.append(v)
            args.add(v)
            print h.GetBinContent(ilast - i), v.getVal()

        res[name] = R.RooAddition(name, name, args)
        #print "{:10s} & {0:.2f} $\pm$ {1:.2f}".format(name, res[name].getVal(), res[name].getError())

if __name__ == "__main__":
    #fname = "plots/LQ270218Plots.270218bPlots_HH_13TeV_270218bPlots_Systs_lephad_LQ_BDT_1500/postfit/Region_BMin0_incJet1_J2_T2_isMVA0_L1_LTT0_Y2015_dist1500_DSRLQBDT_SpcTauLH_GlobalFit_conditionnal_mu0.root"
    fname = "plots/HH060218Direct.270218b_HH_13TeV_270218b_Systs_lephad_2HDM_BDT_300/postfit/Region_BMin0_incJet1_J2_T2_isMVA0_L1_LTT0_Y2015_dist300_DSR2HDMBDT_SpcTauLH_GlobalFit_conditionnal_mu0.root"
    main(fname)
