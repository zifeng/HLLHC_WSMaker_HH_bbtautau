import ROOT as R
from array import array
import commands

def extract(name, masses, inmass):

    vals = array("f", [])
    errs = array("f", [])
    mass = array("f", [])    
    
    #pat = "logs/output_" + name + "_getLimit_{}.log"
    pat = "output/" + name+ "/logs/limit.log"
    for m in masses:
        e, o = commands.getstatusoutput("grep -m1 SigXsecOverSM " + pat.format(m, m))
        if not e:
            tok = o.split()
            mu = float(tok[2])
            #print mu, name
            emu = float(tok[4])            

            if "RSG" in name and m in [260, 300]:
                mu *= 1000
                emu *= 1000

            vals.append(mu)
            errs.append(emu)
            mass.append(m)

    return mass, vals, errs


def plot(name, masses, inmass = 300, inmu = 1, ymin = None, ymax = None):

    mass, vals, errs = extract(name, masses, inmass)

    print mass
    print vals
    print errs

    zero = array("f", [0]*len(mass))        

    c = R.TCanvas()
    g = R.TGraphErrors(len(mass), mass, vals, zero, errs)

    g.GetXaxis().SetLimits(200, 1660)
    #g.GetYaxis().SetRangeUser(-2e-3, 2e-3)

    g.SetMarkerStyle(10)


    if ymin is not None:
        g.SetMinimum(ymin)

    if ymax is not None:
        g.SetMaximum(ymax)

    f = R.TF1("Fit", "gaus", 200, 1600)
    f.SetParameter(1, inmass)
    g.Fit(f, "RN")
    f.SetLineColor(2)


#     t = R.TLatex() 
#     t.SetTextSize(0.025)
#     t.DrawLatex(800, 0, "mean = %s, width = %s" % (round(f.GetParameter(1),2), round(f.GetParameter(2),2)))


    g.SetTitle("Injection test at %s GeV -> (%s, %s); mass [GeV]; #mu_{hat}" % (inmass, round(f.GetParameter(1),2), round(f.GetParameter(2),2)))
    g.Draw("ap")

    f.Draw("same")
    
    l = R.TLine(200, 0, 1600, 0)
    l.SetLineColor(1)
    l.SetLineStyle(2)
    l.Draw()

    l2 = R.TLine(200, inmu, 1600, inmu)
    l2.SetLineColor(2)
    l2.SetLineStyle(2)
    l2.Draw()

    c.Modify()
    c.Update()
    
    l3 = R.TLine(inmass,  g.GetHistogram().GetMinimum(), inmass, g.GetHistogram().GetMaximum())
    l3.SetLineColor(2)
    l3.SetLineStyle(2)
    l3.Draw()
    

    print mass
    print vals

    if "RSG" in name:
        c.Print("muhat_inj_RSG_{0}.eps".format(inmass))
    else:
        c.Print("muhat_inj_2HDM_{0}.eps".format(inmass))        

if __name__ == "__main__":

    R.gROOT.SetBatch(True)
    masses = [251, 260, 280, 300, 325, 350, 400, 450, 500, 550, 600, 700, 800, 900, 1000, 1100, 1200, 1400, 1600]

#     plot("HH240517.240517Inj300_HH_13TeV_240517Inj300_Systs_lephad_2HDM_BDT_{}", masses, inmass = 300, inmu = 0.00881197, ymin = -0.025, ymax = 0.025)
#     plot("HH240517.240517Inj400_HH_13TeV_240517Inj400_Systs_lephad_2HDM_BDT_{}", masses, inmass = 400, inmu = 0.00269636, ymin = -0.01, ymax = 0.01)
#     plot("HH240517.240517Inj500_HH_13TeV_240517Inj500_Systs_lephad_2HDM_BDT_{}", masses, inmass = 500, inmu = 0.00188319, ymin = -0.006, ymax = 0.006)
# 
#     plot("HH240517.240517Inj300_HH_13TeV_240517Inj300_Systs_lephad_RSG_BDT_{}", masses, inmass = 300, inmu = 5.48578, ymin = -2, ymax = 6)
#     plot("HH240517.240517Inj400_HH_13TeV_240517Inj400_Systs_lephad_RSG_BDT_{}", masses, inmass = 400, inmu = 0.791713, ymin = -2, ymax = 6)
#     plot("HH240517.240517Inj500_HH_13TeV_240517Inj500_Systs_lephad_RSG_BDT_{}", masses, inmass = 500, inmu = 0.434995, ymin = -2, ymax = 3)

    #plot("HH240517.080617Inj300_HH_13TeV_080617Inj300_Systs_lephad_RSG_BDT_{}", masses, inmass = 300, inmu = 5.48578, ymin = -2, ymax = 6)
    #plot("HH240517.080617Inj400_HH_13TeV_080617Inj400_Systs_lephad_RSG_BDT_{}", masses, inmass = 400, inmu = 0.791713, ymin = -2, ymax = 6)    
    
#    plot("HH190517.190517dInj300_HH_13TeV_190517dInj300_Systs_lephad_2HDM_BDT_{}", masses, inmass = 300, inmu = 0.0164443, ymin = -0.025, ymax = 0.025)
#    plot("HH190517.190517bInj400_HH_13TeV_190517bInj400_Systs_lephad_2HDM_BDT_{}", masses, inmass = 400, inmu = 0.00269314, ymin = -0.01, ymax = 0.01)
#    plot("HH190517.190517bInj500_HH_13TeV_190517bInj500_Systs_lephad_2HDM_BDT_{}", masses, inmass = 500, inmu = 0.00187001, ymin = -0.006, ymax = 0.006)


    plot("HHv15HadHad29042021AllFixesUnblindingPostProc.InjX1100AtLimit_HH_13TeV_InjX1100AtLimit_Systs_hadhad_2HDM_BDT_{}", masses, inmass = 1100, inmu= 0.02, ymin= -0.5, ymax= 0.5)
