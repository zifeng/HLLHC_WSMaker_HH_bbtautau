import limit as l

_xslh = l.xsect(G = 0, chan = "lephad")
_xshh = l.xsect(G = 0, chan = "hadhad")

_hMSSM = {
    260 :  2.6742310387,
    275 :  2.9190982626,
    300 :  2.6859151204,
    325 :  2.4535980702,
    350 :  2.2189669757,
    400 :  0.5593503107,
    450 :  0.2054836430,
    500 :  0.0893764994,
    550 :  0.0428614977,
    600 :  0.0220075085,
    700 :  0.0067277170,
    800 :  0.0023742485,
    900 :  0.0009316554,
    1000:  0.0003971562,
}


def ratio(m, chan = "lephad"):
    xs = _xslh if chan == "lephad" else _xshh
    return _hMSSM[m]/xs[m]

if __name__ == "__main__":
    print ratio(300)
