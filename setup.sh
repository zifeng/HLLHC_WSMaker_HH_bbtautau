export ANALYSISTYPE="HHbbtautau"
export ANALYSISDIR=$(pwd)
export IS_BLINDED=0

echo "Setting ANALYSISTYPE to : "$ANALYSISTYPE
source WSMakerCore/setup.sh $ANALYSISDIR/WSMakerCore $IS_BLINDED
export NCORE=4